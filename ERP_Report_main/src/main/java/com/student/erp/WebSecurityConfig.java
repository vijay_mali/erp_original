package com.student.erp;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;

import com.student.erp.service.CustomAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsService userDetailsService;
    
    @Autowired
    private CustomAccessDeniedHandler authFailureHandler;
    
   /* @Autowired
    DataSource dataSource;*/

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/resources/**", "/registration","/images/**","/login**").permitAll()
                    .anyRequest().authenticated()
                    .and()
                    .formLogin().failureUrl("/login")
                    .loginPage("/login")
                    .permitAll()
                    .and()
                .logout()
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login?logout")
                .permitAll();
        http.csrf().disable();
        http.addFilterBefore(customUsernamePasswordAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class).exceptionHandling().accessDeniedPage("/login?error");
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return super.userDetailsService();
    }
    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }
    
    @Bean("CustomAuthenticationFilter")
    public CustomAuthenticationFilter customUsernamePasswordAuthenticationFilter()
        throws Exception {
    	CustomAuthenticationFilter customUsernamePasswordAuthenticationFilter = new CustomAuthenticationFilter();
        customUsernamePasswordAuthenticationFilter
            .setAuthenticationManager(authenticationManagerBean());
        customUsernamePasswordAuthenticationFilter
            .setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/login","POST"));
        return customUsernamePasswordAuthenticationFilter;
    }
    
    @Component
    public class CustomAccessDeniedHandler implements AccessDeniedHandler{

    	public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exc)
            throws IOException, ServletException {

            if (!response.isCommitted()) {

               request.getRequestDispatcher("/login?error").forward(request, response);
            }
        }

		
    }

    
    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource)
    {
    	System.out.println("** JDBC INITIALIZED");
        return new JdbcTemplate(dataSource);
    }
    
    @Bean
    public PlatformTransactionManager transactionManager(DataSource dataSource)
    {
        return new DataSourceTransactionManager(dataSource);
    }
}