package com.student.erp;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.student.erp.master.entities.SchoolCode;
import com.student.erp.master.service.SchoolCodeService;

@Component
public class TenantInterceptor implements HandlerInterceptor{

	@Autowired
	SchoolCodeService service;
	
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse arg1, Object arg2) throws Exception {
		
		
		if(request.getParameter("scode") != null){
			System.out.println("Trying to Creat session "+request.getParameter("scode"));
			
			SchoolCode codes=service.findbySchoolCode(request.getParameter("scode"));
			if(codes.getDb_name() != null)
				{
				
				System.out.println(" SESSION CREATED");
				request.getSession(true);
				}
			else
			{
				System.out.println(" CANT CREATE SESSION");
			}
		}
		return true;
	}
	
	

}
