package com.student.erp.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="remarks")
public class Remarks {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private long id;
	

	
	@Column(name="student_id")
	private String student_id;


	@Column(name="label")
	private String label;
	
	@Column(name="value")
	private String value;

	
	@Column(name="active")
	private int active;

	
	@Column(name="session")
	private String session;

	@Column(name="examtype")
	private String examtype;

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getStudent_id() {
		return student_id;
	}


	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}


	public String getLabel() {
		return label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public int getActive() {
		return active;
	}


	public void setActive(int active) {
		this.active = active;
	}


	public String getSession() {
		return session;
	}


	public void setSession(String session) {
		this.session = session;
	}


	public String getExamtype() {
		return examtype;
	}


	public void setExamtype(String examtype) {
		this.examtype = examtype;
	}
	
	

	

	
}

