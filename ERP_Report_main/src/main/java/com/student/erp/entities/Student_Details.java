package com.student.erp.entities;



import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="student_details")
public class Student_Details implements Serializable{

		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		@Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
		@Column(name="student_id")
		private long student_id;
		
		@Column(name="admno")
		private String admno;

		@Column(name = "admission_date", columnDefinition="DATE")
		@Temporal(TemporalType.TIMESTAMP)
		private Date admission_date;

		@Column(name="student_name")
		private String student_name;
		
		@Column(name="batch")
		private String batch;
		
		@Column(name="blood_group")
		private String blood_group;
		
		@Column(name="gender")
		private String gender; 
		
		@Column(name="nationality")
		private String nationality;
		
		@Column(name="language")
		private String language;
		
		@Column(name="category")
		private String category;
		
		@Column(name="religion")
		private String religion;
		
		@Column(name="address")
		private String address;
		
		@Column(name="city")
		private String city;
		
		@Column(name="state")
		private String state;

		
		
		@Column(name="country")
		private String country;
		
		@Column(name="phone")
		private String phone;
		
		@Column(name="mobile")
		private String mobile;
		
		@Column(name="email")
		private String email;
		
		@Column(name="biometric_id")
		private String biometric_id;
		
		@Column(name="is_sms_enabled")
		private String is_sms_enabled;
		
		
		@Column(name="is_email_enabled")
		private String is_email_enabled;
		
		
		@Column(name="photo")
		private String photo;
		
		@Column(name="immediate_contact")
		private String immediate_contact;
		
		@Column(name="session")
		private String session;
		
		@Column(name="roll_no")
		private String roll_no;
		
		@Column(name="aio_no")
		private String aio_no;
		
		@Column(name="dob")
		private String dob;
		
		@Column(name="active")
		private int active;

		public long getStudent_id() {
			return student_id;
		}

		public void setStudent_id(long student_id) {
			this.student_id = student_id;
		}

		public String getAdmno() {
			return admno;
		}

		public void setAdmno(String admno) {
			this.admno = admno;
		}

		public Date getAdmission_date() {
			return admission_date;
		}

		public void setAdmission_date(Date admission_date) {
			this.admission_date = admission_date;
		}

		public String getStudent_name() {
			return student_name;
		}

		public void setStudent_name(String student_name) {
			this.student_name = student_name;
		}

		public String getBatch() {
			return batch;
		}

		public void setBatch(String batch) {
			this.batch = batch;
		}

		public String getBlood_group() {
			return blood_group;
		}

		public void setBlood_group(String blood_group) {
			this.blood_group = blood_group;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

		public String getNationality() {
			return nationality;
		}

		public void setNationality(String nationality) {
			this.nationality = nationality;
		}

		public String getLanguage() {
			return language;
		}

		public void setLanguage(String language) {
			this.language = language;
		}

		public String getCategory() {
			return category;
		}

		public void setCategory(String category) {
			this.category = category;
		}

		public String getReligion() {
			return religion;
		}

		public void setReligion(String religion) {
			this.religion = religion;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public String getMobile() {
			return mobile;
		}

		public void setMobile(String mobile) {
			this.mobile = mobile;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getBiometric_id() {
			return biometric_id;
		}

		public void setBiometric_id(String biometric_id) {
			this.biometric_id = biometric_id;
		}

		public String getIs_sms_enabled() {
			return is_sms_enabled;
		}

		public void setIs_sms_enabled(String is_sms_enabled) {
			this.is_sms_enabled = is_sms_enabled;
		}

		public String getIs_email_enabled() {
			return is_email_enabled;
		}

		public void setIs_email_enabled(String is_email_enabled) {
			this.is_email_enabled = is_email_enabled;
		}

		public String getPhoto() {
			return photo;
		}

		public void setPhoto(String photo) {
			this.photo = photo;
		}

		public String getImmediate_contact() {
			return immediate_contact;
		}

		public void setImmediate_contact(String immediate_contact) {
			this.immediate_contact = immediate_contact;
		}

		public String getSession() {
			return session;
		}

		public void setSession(String session) {
			this.session = session;
		}

		public int getActive() {
			return active;
		}

		public void setActive(int active) {
			this.active = active;
		}

		public String getRoll_no() {
			return roll_no;
		}

		public void setRoll_no(String roll_no) {
			this.roll_no = roll_no;
		}

		public String getAio_no() {
			return aio_no;
		}

		public void setAio_no(String aio_no) {
			this.aio_no = aio_no;
		}

		public String getDob() {
			return dob;
		}

		public void setDob(String dob) {
			this.dob = dob;
		}
		
		
		
}
