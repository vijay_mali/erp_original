package com.student.erp.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="marks")
public class Marks {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private long id;
	
	@Column(name="admno")
	private String admnno;

	@Column(name="exam")
	private String exam;
	
	@Column(name="batch")
	private String batch;

	@Column(name="subject")
	private String subject;

	@Column(name="marks",precision=2)
	private float marks;

	@Column(name="grade")
	private String grade;
	
	@Column(name="active")
	private int active;

	
	@Column(name="session")
	private String session;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getAdmnno() {
		return admnno;
	}


	public void setAdmnno(String admnno) {
		this.admnno = admnno;
	}


	public String getExam() {
		return exam;
	}


	public void setExam(String exam) {
		this.exam = exam;
	}


	public String getBatch() {
		return batch;
	}


	public void setBatch(String batch) {
		this.batch = batch;
	}


	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public float getMarks() {
		return marks;
	}


	public void setMarks(float marks) {
		this.marks = marks;
	}


	public String getGrade() {
		return grade;
	}


	public void setGrade(String grade) {
		this.grade = grade;
	}


	public int getActive() {
		return active;
	}


	public void setActive(int active) {
		this.active = active;
	}


	public String getSession() {
		return session;
	}


	public void setSession(String session) {
		this.session = session;
	}


	
}

