package com.student.erp.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="max_min_marks")
public class Max_Min_Marks {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private long id;
	
	@Column(name="exam_name")
	private String exam_name;

	@Column(name="subject_name")
	private String subject_name;
	
	@Column(name="batch")
	private String batch;

	
	@Column(name="min",precision=2)
	private float min;

	@Column(name="max",precision=2)
	private float max;


	@Column(name="exam_date")
	private String exam_date;

	@Column(name="starttime")
	private String starttime;

	@Column(name="endtime")
	private String endtime;

	
	@Column(name="session")
	private String session;
	
	
	@Column(name="active")
	private int active;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getExam_name() {
		return exam_name;
	}


	public void setExam_name(String exam_name) {
		this.exam_name = exam_name;
	}


	public String getSubject_name() {
		return subject_name;
	}


	public void setSubject_name(String subject_name) {
		this.subject_name = subject_name;
	}


	public String getBatch() {
		return batch;
	}


	public void setBatch(String batch) {
		this.batch = batch;
	}


	public float getMin() {
		return min;
	}


	public void setMin(float min) {
		this.min = min;
	}


	public float getMax() {
		return max;
	}


	public void setMax(float max) {
		this.max = max;
	}


	public String getSession() {
		return session;
	}


	public void setSession(String session) {
		this.session = session;
	}


	public int getActive() {
		return active;
	}


	public void setActive(int active) {
		this.active = active;
	}


	public String getExam_date() {
		return exam_date;
	}


	public void setExam_date(String exam_date) {
		this.exam_date = exam_date;
	}


	public String getStarttime() {
		return starttime;
	}


	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}


	public String getEndtime() {
		return endtime;
	}


	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}

	
	
}

