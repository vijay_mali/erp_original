package com.student.erp.entities;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="temp_attendence")
public class Temp_Attendance {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private long id;
	
	@Column(name="admno")
	private String admno;


	@Column(name="name")
	private String name;
	
	@Column(name="tot_present")
	private String tot_present;
	
	@Column(name="working_days")
	private String working_days;
	
	@Column(name="exam_name")
	private String exam;
	
	@Column(name="batch")
	private String batch;
	
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAdmno() {
		return admno;
	}

	public void setAdmno(String admno) {
		this.admno = admno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTot_present() {
		return tot_present;
	}

	public void setTot_present(String tot_present) {
		this.tot_present = tot_present;
	}

	public String getWorking_days() {
		return working_days;
	}

	public void setWorking_days(String working_days) {
		this.working_days = working_days;
	}

	public String getExam_name() {
		return exam;
	}

	public void setExam_name(String exam) {
		this.exam = exam;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	} 
	
	
	

}
