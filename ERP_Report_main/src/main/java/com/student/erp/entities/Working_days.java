package com.student.erp.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="working_days")
public class Working_days {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private long id;
	

	
	@Column(name="batch")
	private String batch;


	@Column(name="working_days")
	private int working_days;

	
	@Column(name="active")
	private int active;

	
	@Column(name="session")
	private String session;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getBatch() {
		return batch;
	}


	public void setBatch(String batch) {
		this.batch = batch;
	}


	public int getWorking_days() {
		return working_days;
	}


	public void setWorking_days(int working_days) {
		this.working_days = working_days;
	}


	public int getActive() {
		return active;
	}


	public void setActive(int active) {
		this.active = active;
	}


	public String getSession() {
		return session;
	}


	public void setSession(String session) {
		this.session = session;
	}

	
}

