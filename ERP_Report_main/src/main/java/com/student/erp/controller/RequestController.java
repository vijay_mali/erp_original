package com.student.erp.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RequestController {
	
	
	
	@RequestMapping(value = "errors", method = RequestMethod.GET)
    public ModelAndView renderErrorPage(HttpServletRequest httpRequest) {
         
        ModelAndView errorPage = new ModelAndView("login");
        String errorMsg = "";
        int httpErrorCode = getErrorCode(httpRequest);
 
        switch (httpErrorCode) {
            case 401: {
                errorMsg = "Invalid Credentials.";
                break;
            }
           
        }
        errorPage.addObject("error", errorMsg);
        return errorPage;
    }
     
    private int getErrorCode(HttpServletRequest httpRequest) {
        return (Integer) httpRequest
          .getAttribute("javax.servlet.error.status_code");
    }
	
	
	@RequestMapping(value="/", method={})
	public ModelAndView getHome()
	{
		ModelAndView homeView=new ModelAndView();
		homeView.setViewName("home");
		return homeView;
	}
	
	@RequestMapping(value="/home", method={})
	public ModelAndView getHomeDefault()
	{
		ModelAndView homeView=new ModelAndView();
		homeView.setViewName("home");
		return homeView;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }
	
	
	@RequestMapping(value="/edit", method={})
	public ModelAndView getEdit()
	{
		ModelAndView homeView=new ModelAndView();
		homeView.setViewName("edit");
		return homeView;
	}
	
	@RequestMapping(value="/print", method={})
	public ModelAndView getPrint()
	{
		ModelAndView homeView=new ModelAndView();
		homeView.setViewName("print");
		return homeView;
	}

}
