package com.student.erp.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itextpdf.text.DocumentException;
import com.student.erp.PaginationUtils;
import com.student.erp.entities.Auto_Attendence;
import com.student.erp.entities.EditReportWrapper;
import com.student.erp.entities.Examination;
import com.student.erp.entities.Remarks;
import com.student.erp.entities.Student_Details;
import com.student.erp.entities.Temp_Attendance;
import com.student.erp.entities.Working_days;
import com.student.erp.service.AutoAttendenceService;
import com.student.erp.service.ExaminationService;
import com.student.erp.service.HolidayService;
import com.student.erp.service.MarksService;
import com.student.erp.service.MaxMinService;
import com.student.erp.service.RemarksService;
import com.student.erp.service.StudentDetailsService;
import com.student.erp.service.SubjectService;
import com.student.erp.service.WorkingDaysService;
import com.student.erp.utils.PDFUtilities;

@RestController
@RequestMapping("/erp/api")
public class ERPRestController {

	@Autowired
	private StudentDetailsService studentDetailsService;
	
	@Autowired
	private WorkingDaysService workingDaysService;
	
	@Autowired
	private ExaminationService examinationService;
	
	@Autowired
	private SubjectService subjectService;
	
	@Autowired
	private MarksService marksService;
	
	
	@Value("${app.logopath}")
	private String logoPath;
	

	@Autowired
	private MaxMinService maxMinService;
	
	@Autowired
	private HolidayService holidayService;
	
	@Autowired
	private RemarksService remarksService;
	

	@Autowired
	private AutoAttendenceService autoAttendenceService;
	
	private List<String> examSequenceList= new ArrayList<String>();
	
	@PostConstruct
	public void init()
	{
		System.out.println("INITIALIZED...");
		examSequenceList.add("First Periodic");
		examSequenceList.add("First Terminal");
		examSequenceList.add("Second Periodic");
		examSequenceList.add("Second Terminal");
	}
	
	@RequestMapping(value="/get", method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<Page<Student_Details>> getAllData(@RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "5") int size) throws InterruptedException, ExecutionException 
	{
		Page<Student_Details> studentDetails=studentDetailsService.findAll(PaginationUtils.generatePageRequest(page, size));
		return new ResponseEntity<Page<Student_Details>>(studentDetails, HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/getEdit", method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<List<EditReportWrapper>> getAllDataEdit(@RequestParam("attendance") String attendance,@RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "5") int size) throws InterruptedException, ExecutionException 
	{
		List<Student_Details> studentDetails=studentDetailsService.findAllList();
		
		List<EditReportWrapper> edList= new ArrayList<EditReportWrapper>();
		
		/*for(Student_Details st : studentDetails)
		{
			EditReportWrapper editReportWrapper= new EditReportWrapper();
			editReportWrapper.setId(String.valueOf(st.getStudent_id()));
			editReportWrapper.setName(st.getStudent_name());
			editReportWrapper.setAdmno(st.getAdmno());
			
			String attendence = "AB";
			if(attendance.equalsIgnoreCase("manualattendance"))
			{
				Temp_Attendance temp_Attendance= studentDetailsService.getManualAttendance(st.getAdmno());
				if(temp_Attendance != null)
					attendance= temp_Attendance.getTot_present();
			}
			else
			{
				attendence=studentDetailsService.getAttendence(st.getAdmno(), st.getSession());
			}
			editReportWrapper.setAttendence(attendence);
			editReportWrapper.setResult("");
			edList.add(editReportWrapper);
			
		}*/
		
		
		if(attendance.equalsIgnoreCase("manualattendance")){
			List<Map<String, Object>> listDetails = marksService.getStudentMarksManualAll();
			
			for(Map<String, Object> obj : listDetails)
			{
				System.out.println("MAPSS:: "+obj);
				
				EditReportWrapper editReportWrapper= new EditReportWrapper();
				editReportWrapper.setId(String.valueOf(obj.get("id").toString()));
				editReportWrapper.setName(obj.get("name").toString());
				editReportWrapper.setAdmno(obj.get("admno").toString());

				
				String attendence = "AB";
				
				
					attendence=obj.get("tot_present").toString();
				editReportWrapper.setAttendence(attendence);
				editReportWrapper.setWorking_days(obj.get("working_days").toString());
				editReportWrapper.setResult("");
				edList.add(editReportWrapper);
			}
		}
		return new ResponseEntity<List<EditReportWrapper>>(edList, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value="/getEditAuto", method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<List<EditReportWrapper>> getAllDataEditAuto(@RequestParam("attendance") String attendance,@RequestParam(value="startdate", required=false) String startDate,@RequestParam(value="enddate", required=false) String endDate,@RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "5") int size) throws InterruptedException, ExecutionException, ParseException 
	{
		
		List<EditReportWrapper> edList= new ArrayList<EditReportWrapper>();
	
		
		
		if(attendance.equalsIgnoreCase("manualattendance")){
			List<Map<String, Object>> listDetails = marksService.getStudentMarksManualAll();
			
			for(Map<String, Object> obj : listDetails)
			{
				EditReportWrapper editReportWrapper= new EditReportWrapper();
				editReportWrapper.setId(String.valueOf(obj.get("id").toString()));
				editReportWrapper.setName(obj.get("name").toString());
				editReportWrapper.setAdmno(obj.get("admno").toString());
			
				
				String attendence = "AB";
				
				
					attendence=obj.get("tot_present").toString();
				
				editReportWrapper.setAttendence(attendence);
				editReportWrapper.setResult("");
				edList.add(editReportWrapper);
			}
		}
		else if(attendance.equalsIgnoreCase("autoattendance")){
			if(startDate != null && endDate !=null)
			{
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date startdate= sdf.parse(startDate);
				Date enddate= sdf.parse(endDate);
				System.out.println(startdate+" -- "+enddate);
				System.out.println("DIFF "+(enddate.getTime() - startdate.getTime()));
				System.out.println("DIFF1"+( (enddate.getTime() - startdate.getTime()) / (1000 * 60 * 60 * 24)));
				System.out.println((int)( (enddate.getTime() - startdate.getTime()) / (1000 * 60 * 60 * 24)));
				
				int working_days=(int)( (enddate.getTime() - startdate.getTime()) / (1000 * 60 * 60 * 24));
				long getHolidayCount=holidayService.getCountByDate(startDate, endDate);
				
				List<Student_Details> studentDetails=studentDetailsService.findAllList();
				
				for(Student_Details st : studentDetails)
				{
					EditReportWrapper editReportWrapper= new EditReportWrapper();
					editReportWrapper.setId(String.valueOf(st.getStudent_id()));
					editReportWrapper.setName(st.getStudent_name());
					editReportWrapper.setAdmno(st.getAdmno());
					
					
					String attendence=studentDetailsService.getAttendence(st.getAdmno(), st.getSession());
					
						System.out.println(attendence+" HOLIDAYCOUNT - "+getHolidayCount);
						attendence=String.valueOf(working_days-Long.valueOf(attendence)-getHolidayCount);
					editReportWrapper.setAttendence(attendence);
					editReportWrapper.setResult("");
					editReportWrapper.setWorking_days(String.valueOf(working_days-getHolidayCount));
					edList.add(editReportWrapper);
					
				}
				
			}
			/*List<Object[]> listDetails = marksService.getStudentMarksManualAll();
			
			for(Object[] obj : listDetails)
			{
				EditReportWrapper editReportWrapper= new EditReportWrapper();
				editReportWrapper.setId(String.valueOf(obj[0].toString()));
				editReportWrapper.setName(obj[1].toString());
				editReportWrapper.setAdmno(obj[2].toString());
				
				String attendence = "AB";
				
				
					attendence=obj[3].toString();
				
				editReportWrapper.setAttendence(attendence);
				editReportWrapper.setResult("");
				edList.add(editReportWrapper);
			}*/
		}
			
		return new ResponseEntity<List<EditReportWrapper>>(edList, HttpStatus.OK);
	}
	
	
	
	
	
	
	
	@RequestMapping(value="/searchEdit", method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<List<EditReportWrapper>> getSearchDataEdit(@RequestParam("batch") String batch,@RequestParam("attendance") String attendance,@RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "5") int size) throws InterruptedException, ExecutionException 
	{
		List<Student_Details> studentDetails=studentDetailsService.searchByBatch(batch);
		
		
		
		List<EditReportWrapper> edList= new ArrayList<EditReportWrapper>();
		
		if(attendance.equalsIgnoreCase("manualattendance")){
			List<Map<String, Object>> listDetails = marksService.getStudentMarksManualByBatch(batch);
			
			for(Map<String, Object> obj : listDetails)
			{
				EditReportWrapper editReportWrapper= new EditReportWrapper();
				editReportWrapper.setId(String.valueOf(obj.get("id").toString()));
				editReportWrapper.setName(obj.get("name").toString());
				editReportWrapper.setAdmno(obj.get("admno").toString());
				
				
				String attendence = "AB";
				
				
					attendence=obj.get("tot_present").toString();
				
				editReportWrapper.setAttendence(attendence);
				editReportWrapper.setResult("");
				edList.add(editReportWrapper);
			}
		}
		
		return new ResponseEntity<List<EditReportWrapper>>(edList, HttpStatus.OK);
	}
	
	@RequestMapping(value="/searchEditByExamBranch", method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<List<EditReportWrapper>> searchEditByExamBranch(@RequestParam("batch") String batch,@RequestParam("exam") String exam,@RequestParam("attendance") String attendance,@RequestParam(defaultValue = "0") int page,
			@RequestParam(value="startdate", required=false) String startDate,@RequestParam(value="enddate", required=false) String endDate,@RequestParam(defaultValue = "5") int size) throws InterruptedException, ExecutionException, ParseException 
	{
		
		List<EditReportWrapper> edList= new ArrayList<EditReportWrapper>();

		if(attendance.equalsIgnoreCase("manualattendance")){
		
			List<Student_Details> studentDetails=studentDetailsService.searchByBatch(batch);
			
			List<String> marksAdmList=marksService.findListByBatchExam(batch,exam);
			System.out.println(">>."+marksAdmList);
			for(Student_Details st : studentDetails)
			{
				if(marksAdmList.contains(st.getAdmno())){
					EditReportWrapper editReportWrapper= new EditReportWrapper();
					editReportWrapper.setId(String.valueOf(st.getStudent_id()));
					editReportWrapper.setName(st.getStudent_name());
					editReportWrapper.setAdmno(st.getAdmno());
					
					String student_attendence = "AB";
					if(attendance.equalsIgnoreCase("manualattendance"))
					{
						//Temp_Attendance temp_Attendance= studentDetailsService.getManualAttendance(st.getAdmno());
						//Temp_Attendance temp_Attendance_exam= studentDetailsService.getManualAttendanceByExam(st.getAdmno(),exam);
						List<Temp_Attendance> temp_Attendance1= studentDetailsService.getManualAttendanceWokringDays(batch,exam,st.getAdmno());
						
						//if(temp_Attendance1 != null)
						
							
						int i=0;
						for(Temp_Attendance tmp : temp_Attendance1)
						{
							if(i==0)
							{
							
								student_attendence= tmp.getTot_present();
								System.out.println(">> Attendence: "+student_attendence);
								editReportWrapper.setWorking_days(tmp.getWorking_days());
								
							}
							i++;
						}
					}
					/*else
					{
						attendence=studentDetailsService.getAttendence(st.getAdmno(), st.getSession());
					}*/
					if(exam.equalsIgnoreCase("Final Exam"))
					{
						List<Remarks> remarksList=remarksService.searchByStudent(st.getAdmno());
						boolean resultFound=false, remarksFound=false;
						System.out.println(st.getAdmno()+" LIST: "+remarksList);
						for(Remarks remarks : remarksList)
						{
							System.out.println(remarks.getLabel()+" - "+remarks.getValue()+"FOUND REMARKS FOR: "+st.getAdmno());
							if(remarks.getLabel().equalsIgnoreCase("overall_remark"))
							{	
								remarksFound=true;
								editReportWrapper.setRemarks(remarks.getValue());
							}
							else if(remarks.getLabel().equalsIgnoreCase("Result"))
							{	
								resultFound=true;
								editReportWrapper.setResult(remarks.getValue());
							}
						}
						if(!remarksFound)
						{
							editReportWrapper.setRemarks("");
						}
						if(!resultFound)
						{
							editReportWrapper.setResult("");
						}
					}
					editReportWrapper.setAttendence(student_attendence);
					edList.add(editReportWrapper);
				}
				
			}
			
			
		}
		else if(attendance.equalsIgnoreCase("autoattendance")){
			
			if(startDate != null && endDate !=null)
			{
				
				List<String> marksAdmList=marksService.findListByBatchExam(batch,exam);
				
				
				List<Student_Details> studentDetails=studentDetailsService.searchByBatch(batch);
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date startdate= sdf.parse(startDate);
				Date enddate= sdf.parse(endDate);
				System.out.println(startdate+" -- "+enddate);
				System.out.println("DIFF "+(enddate.getTime() - startdate.getTime()));
				System.out.println("DIFF1"+( (enddate.getTime() - startdate.getTime()) / (1000 * 60 * 60 * 24)));
				System.out.println((int)( (enddate.getTime() - startdate.getTime()) / (1000 * 60 * 60 * 24)));
				
				int working_days=(int)( (enddate.getTime() - startdate.getTime()) / (1000 * 60 * 60 * 24));
				long getHolidayCount=holidayService.getCountByDate(startDate, endDate);
				
				
				
				for(Student_Details st : studentDetails)
				{
					if(marksAdmList.contains(st.getAdmno())){
							EditReportWrapper editReportWrapper= new EditReportWrapper();
							editReportWrapper.setId(String.valueOf(st.getStudent_id()));
							editReportWrapper.setName(st.getStudent_name());
							editReportWrapper.setAdmno(st.getAdmno());
							
							String attendence=studentDetailsService.getAttendence(st.getAdmno(), st.getSession());
							
								System.out.println(attendence+" HOLIDAYCOUNT - "+getHolidayCount);
								attendence=String.valueOf(working_days-Long.valueOf(attendence)-getHolidayCount);
							editReportWrapper.setAttendence(attendence);
							boolean resultFound=false, remarksFound=false;
							if(exam.equalsIgnoreCase("Final Exam"))
							{
								List<Remarks> remarksList=remarksService.searchByStudent(st.getAdmno());
								
								System.out.println(st.getAdmno()+" LIST: "+remarksList);
								for(Remarks remarks : remarksList)
								{
									System.out.println(remarks.getLabel()+" - "+remarks.getValue()+"FOUND REMARKS FOR: "+st.getAdmno());
									if(remarks.getLabel().equalsIgnoreCase("overall_remark"))
									{	
										remarksFound=true;
										editReportWrapper.setRemarks(remarks.getValue());
									}
									else if(remarks.getLabel().equalsIgnoreCase("Result"))
									{	
										resultFound=true;
										editReportWrapper.setResult(remarks.getValue());
									}
								}
								
						}
							if(!remarksFound)
							{
								editReportWrapper.setRemarks("");
							}
							if(!resultFound)
							{
								editReportWrapper.setResult("");
							}
							editReportWrapper.setWorking_days(String.valueOf(working_days-getHolidayCount));
							edList.add(editReportWrapper);
		
					}
				}
			}
			
		}
		return new ResponseEntity<List<EditReportWrapper>>(edList, HttpStatus.OK);
	}
	
	
	
	
	
	@RequestMapping(value="/searchPrintEditByExamBranch", method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<List<EditReportWrapper>> searchPrintEditByExamBranch(@RequestParam("batch") String batch,@RequestParam("attendance") String attendance,@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size) throws InterruptedException, ExecutionException, ParseException 
	{
		
		List<EditReportWrapper> edList= new ArrayList<EditReportWrapper>();
	//	Auto_Attendence autoAttendance=autoAttendenceService.findByBatch(batch);
		
		
		if(attendance.equalsIgnoreCase("manualattendance")){
			
			List<Student_Details> studentDetails=studentDetailsService.searchByBatch(batch);
			
			List<String> marksAdmList=marksService.findListByBatch(batch);
			
			for(Student_Details st : studentDetails)
			{
				if(marksAdmList.contains(st.getAdmno())){
					EditReportWrapper editReportWrapper= new EditReportWrapper();
					editReportWrapper.setId(String.valueOf(st.getStudent_id()));
					editReportWrapper.setName(st.getStudent_name());
					editReportWrapper.setAdmno(st.getAdmno());
					
					String attendence = "AB";
					if(attendance.equalsIgnoreCase("manualattendance"))
					{
					//	Temp_Attendance temp_Attendance= studentDetailsService.getManualAttendance(st.getAdmno());
						List<Temp_Attendance> temp_Attendance1= studentDetailsService.getManualAttendanceWokringDaysByAdmno(batch,st.getAdmno());
						
						//if(temp_Attendance1 != null)
							
						int i=0;
						for(Temp_Attendance tmp : temp_Attendance1)
						{
							if(i==0)
							{
							
								attendence= tmp.getTot_present();
								editReportWrapper.setWorking_days(tmp.getWorking_days());
							}
							i++;
						}
					}
					/*else
					{
						attendence=studentDetailsService.getAttendence(st.getAdmno(), st.getSession());
					}*/
					editReportWrapper.setAttendence(attendence);
					edList.add(editReportWrapper);
				}
				
			}
			
			
		}
		else if(attendance.equalsIgnoreCase("autoattendance")){
		
		List<Auto_Attendence> autoAttendanceList=autoAttendenceService.findByBatchDesc(batch);
		if(autoAttendanceList.size()>0){
		
			String startDate=autoAttendanceList.get(0).getStart_date();
			String endDate=autoAttendanceList.get(0).getEnd_date();
			
			if(startDate != null && endDate !=null)
			{
				
				List<String> marksAdmList=marksService.findListByBatch(batch);
				
				
				List<Student_Details> studentDetails=studentDetailsService.searchByBatch(batch);
				
				
				
				
				for(Student_Details st : studentDetails)
				{
					if(marksAdmList.contains(st.getAdmno())){
							EditReportWrapper editReportWrapper= new EditReportWrapper();
							
							editReportWrapper.setId(String.valueOf(st.getStudent_id()));
							editReportWrapper.setName(st.getStudent_name());
							editReportWrapper.setAdmno(st.getAdmno());
							
							editReportWrapper.setStartDate(startDate);
							editReportWrapper.setEndDate(endDate);
							String attendence=studentDetailsService.getAttendence(st.getAdmno(), st.getSession());
							
								/*System.out.println(attendence+" HOLIDAYCOUNT - "+getHolidayCount);
								attendence=String.valueOf(working_days-Long.valueOf(attendence)-getHolidayCount);*/
							editReportWrapper.setAttendence(attendence);
							boolean resultFound=false, remarksFound=false;
							
							if(!remarksFound)
							{
								editReportWrapper.setRemarks("");
							}
							if(!resultFound)
							{
								editReportWrapper.setResult("");
							}
							//editReportWrapper.setWorking_days(String.valueOf(working_days-getHolidayCount));
							edList.add(editReportWrapper);
						
					}
			
			else
			{
				EditReportWrapper editReportWrapper= new EditReportWrapper();
				editReportWrapper.setStartDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
				editReportWrapper.setEndDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
				editReportWrapper.setWorking_days("0");
				edList.add(editReportWrapper);
			}
			
				}
			}
		}
	}
		return new ResponseEntity<List<EditReportWrapper>>(edList, HttpStatus.OK);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(value="/getBatch", method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<List<String>> getBatchList() throws InterruptedException, ExecutionException 
	{
		List batchList=studentDetailsService.getBatchList();
		return new ResponseEntity<List<String>>(batchList, HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/getExam", method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<List<String>> getExamList() throws InterruptedException, ExecutionException 
	{
		List examList=marksService.listExam();
		return new ResponseEntity<List<String>>(examList, HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/getDays", method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<String> getDays(@RequestParam("batch") String batch,@RequestParam("attendance") String attendance,@RequestParam(value="startdate", required=false) String startDate,@RequestParam(value="enddate", required=false) String endDate) throws InterruptedException, ExecutionException 
	{
		
		if(attendance.equalsIgnoreCase("manualattendance")){
			Working_days workingDays=workingDaysService.searchByBatch(batch);
			if(workingDays == null)
			{
				return new ResponseEntity<String>("0", HttpStatus.OK);
			}else
			{
				return new ResponseEntity<String>(String.valueOf(workingDays.getWorking_days()), HttpStatus.OK);
			}
		}
		else
		{
			long getHolidayCount=holidayService.getCountByDate(startDate, endDate);
			Working_days workingDays=workingDaysService.searchByBatch(batch);
			if(workingDays == null)
			{
				return new ResponseEntity<String>("0", HttpStatus.OK);
			}else
			{
				return new ResponseEntity<String>(String.valueOf(workingDays.getWorking_days()), HttpStatus.OK);
			}
			
		}
		
		
	}
	@RequestMapping(value="/print", method={RequestMethod.GET,RequestMethod.POST},produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> getPrint(@RequestParam("batch") String batch,@RequestParam("attendence") String attendence) throws InterruptedException, ExecutionException, MalformedURLException, DocumentException, IOException, ParseException 
	{
		
		
		if(attendence.equalsIgnoreCase("manualattendance"))
		{
			//
			
			HttpHeaders headers = new HttpHeaders();
	        headers.add("Content-Disposition", "inline; filename="+new Date().getTime()+".pdf");
	        return ResponseEntity
	                .ok()
	                .headers(headers)
	                .contentType(MediaType.APPLICATION_PDF)
	                .body(new InputStreamResource(getPrintManual(batch, "","", attendence)));
		}
		else{
		List<Student_Details> studentDetails=studentDetailsService.searchByBatch(batch);
		List<Examination> examinationDetails=examinationService.findAllListByBbatch(batch);
		List<String> dynamicExamListString = new ArrayList<>();
 		if(examinationDetails!=null && !examinationDetails.isEmpty())
		{
			for(Examination examDetails:examinationDetails)
			{
				dynamicExamListString.add(examDetails.getExam_name());
			}
		}
 		List<String> exam_list= new ArrayList<>();
		List<String> exam_list_without_max=new ArrayList<>();
		
		
		
		//List<String> exam_list=examinationService.getExaminationType(batch);
		System.out.println("EXAM: "+exam_list);
		
		Map<String, Map<String, Map<String,String>>> subjectMap=new HashMap<String, Map<String, Map<String,String>>>();
		
		//Auto_Attendence autoAttendance=autoAttendenceService.findByBatch(batch);
		List<Auto_Attendence> autoAttendanceList=autoAttendenceService.findByBatchDesc(batch);
		System.out.println(">>."+autoAttendanceList);
		int latest_exam_details=0;
		String latest_exam_name=null;

			List<String> examList=new ArrayList<String>();
		if(autoAttendanceList.size()>0)
		{
			int examNo=examSequenceList.size();
			for(Auto_Attendence  auto_attendence : autoAttendanceList)
			{
				if(latest_exam_details ==0)
				{
					System.out.println(">>>."+auto_attendence.getExam());
					latest_exam_name=auto_attendence.getExam();
					latest_exam_details++;
				}
				
				
				if((!latest_exam_name.equalsIgnoreCase("Final Exam")))
				{
					examNo=examSequenceList.indexOf(latest_exam_name)+1;
					for(int examindex=0; examindex<examNo; examindex++)
					{
						examList.add(examSequenceList.get(examindex));
					}
					
				}
				else
				{
					examList=examSequenceList;
				}
			}
		
			String examName=latest_exam_name;
			if(dynamicExamListString!=null && !dynamicExamListString.isEmpty())
			{
				System.out.println("inside dynamic list");
				exam_list=examinationService.getExaminationTypeWithDynamicExamList(batch,dynamicExamListString);
				exam_list_without_max=examinationService.getExaminationTypeWithDynamicExamListWithoutmaxMarks(batch,dynamicExamListString);

				
			}
			else
			{
				exam_list=examinationService.getExaminationType(batch);
			}


			
			
			PDFUtilities pdf= new PDFUtilities();
			
			
			List<String> subject_list=marksService.searchByBatch(batch,examList);

			List<String> marksAdmList=marksService.findListByBatch(batch);
			double classAverage=0.0;
			double totalAverage=0.0;
			HashMap<String, Double> examAverage= new HashMap<>();
			List<String> exam_skills_list=marksService.searchSkillsByBatch(batch);
			List<String> exam_annual_list=marksService.searchAnnulaExamg2ByBatch(batch);
			
			HashMap<String, Double> examClassAverage= new HashMap<>();
			List<Student_Details> studentDetailsTotal=studentDetailsService.searchByBatch(batch);
			for(Student_Details student : studentDetailsTotal)
			{
				
				examAverage=	pdf.getClassAverageOfStudent(remarksService,holidayService,marksService,maxMinService,workingDaysService,studentDetailsService,student,subject_list,exam_skills_list,exam_annual_list, exam_list,subjectMap,attendence,"","","","S.D.A HIGHER SECONDARY SCHOOL", "NUZVID - 521201", "(Under the management of METAS - SURAT)", "Ph : 08656 - 232302",exam_list_without_max);
				System.out.println(" examAverage is"+examAverage);
				Set<String> keyset=examAverage.keySet();
				for(String key:keyset)
				{
					if(examClassAverage.containsKey(key))
					{
						examClassAverage.put(key, examClassAverage.get(key)+examAverage.get(key));
					}
					else
					{
						examClassAverage.put(key, examAverage.get(key));

					}
				}

			}
			System.out.println("examClassAverage"+examClassAverage);
			Set<String> keyset=examClassAverage.keySet();
			DecimalFormat df2 = new DecimalFormat(".##");

			for(String key:keyset)
			{
				if(examClassAverage.containsKey(key))
				{
					examClassAverage.put(key,Double.parseDouble(df2.format(examClassAverage.get(key)/studentDetailsTotal.size())));
				}
			}
			
			

			
		for(Student_Details st :  studentDetails)
		{
			System.out.println("ENTERED INTO LOOP "+st.getStudent_id());
			List<Map<String, Object>>  subjectMarksList=marksService.getSubjectMarksByBatch(batch,st.getAdmno(),examList);
			System.out.println("ENTERED INTO LOOP1 "+subjectMarksList);
			Map<String, Map<String,String>> subject_exam_marks= new HashMap<String, Map<String,String>>();
			for(Map<String, Object> obj: subjectMarksList)
			{
				Map<String, String> exam_marks= new HashMap<String, String>();
				
			
					exam_marks.put(obj.get("exam").toString(), obj.get("marks").toString());
					exam_marks.put("grade", obj.get("grade").toString());
				
				subject_exam_marks.put(obj.get("subject").toString(),exam_marks);
			}
			subjectMap.put(String.valueOf(st.getStudent_id()), subject_exam_marks);
		}
		
		
		System.out.println("COMPLETE MAP: "+subjectMap);
		
		
			
			
		exam_list.add("Total of Classwork");
		//exam_list.add("Final Exam");
		exam_list.add("50% of Total Classwork");
		exam_list.add("50% of Final Exam");
		exam_list.add("Total");
		exam_list.add("Grade");
		exam_list.add("Remarks");
		
		
//		List<String> exam_skills_list=marksService.searchSkillsByBatch(batch);
//		List<String> exam_annual_list=marksService.searchAnnulaExamg2ByBatch(batch);
//		
//		System.out.println(exam_list);
		System.out.println(exam_annual_list);
		
	
		pdf.openDoc("S.D.A HIGHER SECONDARY SCHOOL", "NUZVID - 521201", "(Under the management of METAS - SURAT)", "Ph : 08656 - 232302",this.logoPath);
		
		ByteArrayInputStream bis = null;
		System.out.println("MAP: "+subjectMap);
		
		
		
		//List<String> marksAdmList=new ArrayList<String>();
		
	
		
		System.out.println(examSequenceList+"--"+examName+"--"+examNo);
		
		if((!examName.equalsIgnoreCase("Final Exam")))
		{
			examNo=examSequenceList.indexOf(examName)+1;
		}
		
			for(int examindex=0; examindex<examNo; examindex++)
			{
				marksAdmList.addAll(marksService.findListByBatchExam(batch,examSequenceList.get(examindex)));
			}
		
		
		for(Student_Details student : studentDetails)
		{
			if(marksAdmList.contains(student.getAdmno())){
				pdf.createHeader(remarksService,holidayService,marksService,maxMinService,workingDaysService,studentDetailsService,student,subject_list,exam_skills_list,exam_annual_list, exam_list,subjectMap,attendence,examName,examList,autoAttendanceList,"","S.D.A HIGHER SECONDARY SCHOOL", "NUZVID - 521201", "(Under the management of METAS - SURAT)", "Ph : 08656 - 232302",exam_list,exam_list_without_max,examClassAverage);

			//	pdf.createHeader(remarksService,holidayService,marksService,maxMinService,workingDaysService,studentDetailsService,student,subject_list,exam_skills_list,exam_annual_list, exam_list,subjectMap,attendence,examName,examList,autoAttendanceList,"","S.D.A HIGHER SECONDARY SCHOOL", "NUZVID - 521201", "(Under the management of METAS - SURAT)", "Ph : 08656 - 232302");
			}
		}
		
		
		bis=pdf.closeDoc();
		System.out.println("PROCESS DONE:");
		
		
		System.out.println("SENDING EXAM LIST:"+examList);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename="+new Date().getTime()+".pdf");
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
			}
		}
		  HttpHeaders headers = new HttpHeaders();
	        headers.add("Content-Disposition", "inline; filename="+new Date().getTime()+".pdf");
		 return ResponseEntity
	                .ok()
	                .headers(headers)
	                .contentType(MediaType.APPLICATION_PDF)
	                .body(new InputStreamResource(null));
		//return new ResponseEntity<String>("success", HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value="/printStudent", method={RequestMethod.GET,RequestMethod.POST},produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> getPrintStudent(@RequestParam("batch") String batch,@RequestParam("admno") String admno,@RequestParam("attendence") String attendence) throws InterruptedException, ExecutionException, MalformedURLException, DocumentException, IOException, ParseException 
	{
		
		if(attendence.equalsIgnoreCase("manualattendance"))
		{
			//
			System.out.println("manual attendance");
			HttpHeaders headers = new HttpHeaders();
	        headers.add("Content-Disposition", "inline; filename="+new Date().getTime()+".pdf");
	        return ResponseEntity
	                .ok()
	                .headers(headers)
	                .contentType(MediaType.APPLICATION_PDF)
	                .body(new InputStreamResource(getPrintManual(batch, "",admno, attendence)));
		}
		else{
		List<Student_Details> studentDetails=studentDetailsService.searchByBatch(batch);
		List<Examination> examinationDetails=examinationService.findAllListByBbatch(batch);
		List<String> dynamicExamListString = new ArrayList<>();
 		if(examinationDetails!=null && !examinationDetails.isEmpty())
		{
			for(Examination examDetails:examinationDetails)
			{
				dynamicExamListString.add(examDetails.getExam_name());
			}
		}
 		System.out.println("studentDetails"+studentDetails);
 		List<String> exam_list= new ArrayList<>();
		List<String> exam_list_without_max=new ArrayList<>();
		
		if(dynamicExamListString!=null && !dynamicExamListString.isEmpty())
		{
			System.out.println("inside dynamic list");
			exam_list=examinationService.getExaminationTypeWithDynamicExamList(batch,dynamicExamListString);
			exam_list_without_max=examinationService.getExaminationTypeWithDynamicExamListWithoutmaxMarks(batch,dynamicExamListString);

			
		}
		else
		{
			exam_list=examinationService.getExaminationType(batch);
		}
		
		//List<String> exam_list=examinationService.getExaminationType(batch);
		System.out.println("EXAM: "+exam_list);
		
		Student_Details studentInfo=studentDetailsService.findByAdmNo(admno);
 		System.out.println("studentInfo"+studentInfo);

		Map<String, Map<String, Map<String,String>>> subjectMap=new HashMap<String, Map<String, Map<String,String>>>();
		List<Auto_Attendence> autoAttendanceList=autoAttendenceService.findByBatchDesc(batch);
		int latest_exam_details=0;
		String latest_exam_name=null;

			List<String> examList=new ArrayList<String>();
		if(autoAttendanceList.size()>0)
		{
			//int examNo=0;
			
			int examNo= exam_list!=null?exam_list.size():0;
			for(Auto_Attendence  auto_attendence : autoAttendanceList)
			{
				if(latest_exam_details ==0)
				{
					latest_exam_name=auto_attendence.getExam();
					latest_exam_details++;
				}
				
				
				if((!latest_exam_name.equalsIgnoreCase("Final Exam")))
				{
					examNo=examSequenceList.indexOf(latest_exam_name)+1;
					for(int examindex=0; examindex<examNo; examindex++)
					{
						examList.add(examSequenceList.get(examindex));
					}
					
				}
				else
				{
					examList=examSequenceList;
				}
			}
		
			String examName=latest_exam_name;
			
		PDFUtilities pdf= new PDFUtilities();
			
			
			List<String> subject_list=marksService.searchByBatch(batch,examList);

			List<String> marksAdmList=marksService.findListByBatch(batch);
			double classAverage=0.0;
			double totalAverage=0.0;
			HashMap<String, Double> examAverage= new HashMap<>();
			List<String> exam_skills_list=marksService.searchSkillsByBatch(batch);
			List<String> exam_annual_list=marksService.searchAnnulaExamg2ByBatch(batch);
			
			HashMap<String, Double> examClassAverage= new HashMap<>();
			List<Student_Details> studentDetailsTotal=studentDetailsService.searchByBatch(batch);
			for(Student_Details student : studentDetailsTotal)
			{
				
				examAverage=	pdf.getClassAverageOfStudent(remarksService,holidayService,marksService,maxMinService,workingDaysService,studentDetailsService,student,subject_list,exam_skills_list,exam_annual_list, exam_list,subjectMap,attendence,"","","","S.D.A HIGHER SECONDARY SCHOOL", "NUZVID - 521201", "(Under the management of METAS - SURAT)", "Ph : 08656 - 232302",exam_list_without_max);
				System.out.println(" examAverage is"+examAverage);
				Set<String> keyset=examAverage.keySet();
				for(String key:keyset)
				{
					if(examClassAverage.containsKey(key))
					{
						examClassAverage.put(key, examClassAverage.get(key)+examAverage.get(key));
					}
					else
					{
						examClassAverage.put(key, examAverage.get(key));

					}
				}

			}
			System.out.println("examClassAverage"+examClassAverage);
			Set<String> keyset=examClassAverage.keySet();
			DecimalFormat df2 = new DecimalFormat(".##");

			for(String key:keyset)
			{
				if(examClassAverage.containsKey(key))
				{
					examClassAverage.put(key,Double.parseDouble(df2.format(examClassAverage.get(key)/studentDetailsTotal.size())));
				}
			}
			
		for(Student_Details st :  studentDetails)
		{
			List<Map<String, Object>> subjectMarksList=marksService.getSubjectMarksByBatch(batch,st.getAdmno(),exam_list);
			Map<String, Map<String,String>> subject_exam_marks= new HashMap<String, Map<String,String>>();
			System.out.println("subkect marjs"+subject_exam_marks);
			for(Map<String, Object> obj : subjectMarksList)
			{
				Map<String, String> exam_marks= new HashMap<String, String>();
				
				exam_marks.put(obj.get("exam").toString(), obj.get("marks").toString());
				exam_marks.put("grade", obj.get("grade").toString());
			
				subject_exam_marks.put(obj.get("subject").toString(),exam_marks);
			
					
			}
			subjectMap.put(String.valueOf(st.getStudent_id()), subject_exam_marks);
		}
		
		
//		System.out.println("COMPLETE MAP: "+subjectMap);
		
		
			
			
		//List<String> subject_list=marksService.searchByBatch(batch,examList);
		exam_list.add("Total of Classwork");
		//exam_list.add("Final Exam");
		exam_list.add("50% of Total Classwork");
		exam_list.add("50% of Final Exam");
		exam_list.add("Total");
		exam_list.add("Grade");
		exam_list.add("Remarks");
		
		
//		List<String> exam_skills_list=marksService.searchSkillsByBatch(batch);
//		List<String> exam_annual_list=marksService.searchAnnulaExamg2ByBatch(batch);
		
//		System.out.println(exam_list);
		System.out.println(exam_annual_list);
		
		//PDFUtilities pdf= new PDFUtilities();
		pdf.openDoc("S.D.A HIGHER SECONDARY SCHOOL", "NUZVID - 521201", "(Under the management of METAS - SURAT)", "Ph : 08656 - 232302",this.logoPath);
		
		ByteArrayInputStream bis = null;
		System.out.println("MAP: "+subjectMap);
		
		
		
		//List<String> marksAdmList=new ArrayList<String>();
		
	
		
		System.out.println(examSequenceList+"--"+examName+"--"+examNo);
		
//		if((!examName.equalsIgnoreCase("Final Exam")))
//		{
//			examNo=examSequenceList.indexOf(examName)+1;
//		}
//		
//			for(int examindex=0; examindex<examNo; examindex++)
//			{
//				marksAdmList.addAll(marksService.findListByBatchExam(batch,examSequenceList.get(examindex)));
//			}
//		
		//List<String> marksAdmList=marksService.findListByBatch(batch);

		
			if(marksAdmList.contains(studentInfo.getAdmno())){
				pdf.createHeader(remarksService,holidayService,marksService,maxMinService,workingDaysService,studentDetailsService,studentInfo,subject_list,exam_skills_list,exam_annual_list, exam_list,subjectMap,attendence,examName,examList,autoAttendanceList,"","S.D.A HIGHER SECONDARY SCHOOL", "NUZVID - 521201", "(Under the management of METAS - SURAT)", "Ph : 08656 - 232302",exam_list,exam_list_without_max,examClassAverage);
			}
		
			
		
		bis=pdf.closeDoc();
		System.out.println("PROCESS DONE:");
		
		
		System.out.println("SENDING EXAM LIST:"+examList);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename="+new Date().getTime()+".pdf");
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
		}
		}
		 HttpHeaders headers = new HttpHeaders();
	        headers.add("Content-Disposition", "inline; filename="+new Date().getTime()+".pdf");

	        return ResponseEntity
	                .ok()
	                .headers(headers)
	                .contentType(MediaType.APPLICATION_PDF)
	                .body(new InputStreamResource(null));
		//return new ResponseEntity<String>("success", HttpStatus.OK);
	}
	

	
	@RequestMapping(value="/saveEdit", method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<String> saveAllDataEdit(@RequestBody EditReportWrapper editReportWrapper) 
	{
		
		System.out.println("DATA: "+editReportWrapper.getId());
		System.out.println("DATA: "+editReportWrapper.getRemarks());
		System.out.println("DATA: "+editReportWrapper.getResult());
		
		Student_Details findStudent= studentDetailsService.findOne(Long.valueOf(editReportWrapper.getId()));
		findStudent.setStudent_name(editReportWrapper.getName());
		// findStudent.setAdmno(editReportWrapper.getAdmno());
		studentDetailsService.saveStudent(findStudent);
		
		if(editReportWrapper.getAttendenceType().equalsIgnoreCase("manualattendance"))
		{
			System.out.println("SAVING MANUAL");
			studentDetailsService.saveTempAttendance(editReportWrapper.getAdmno(),(editReportWrapper.getAttendence().equalsIgnoreCase("AB") ? "0" : editReportWrapper.getAttendence()));
		}
		if(editReportWrapper.getResult() != null &&  editReportWrapper.getExamType().equalsIgnoreCase("Final Exam"))
			{
			
				remarksService.updateRemarksData(editReportWrapper.getAdmno(),"overall_remark",editReportWrapper.getRemarks());
				remarksService.updateResultData(editReportWrapper.getAdmno(),"Result",editReportWrapper.getResult(),"Final Exam");
			}
		
		
		return new ResponseEntity<String>("{\"data\" : \"Success\"}", HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/updateWorkingDays", method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<String> updateWorkingDays(@RequestParam("batch") String batch,@RequestParam("exam") String exam,@RequestParam("workingdays") String workingdays) 
	{
		
		int tmp_attendence= workingDaysService.updateTempWorkingDays(batch, exam, workingdays);
		
		return new ResponseEntity<String>("{\"data\" : \"Success\"}", HttpStatus.OK);
	}
	
	@RequestMapping(value="/saveAutoAttendence", method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<String> saveAutoAttendence(@RequestParam("batch") String batch,@RequestParam("exam") String exam,@RequestParam("start_date") String start_date,@RequestParam("end_date") String end_date) 
	{
		
		Auto_Attendence autoAttendance=autoAttendenceService.findByBatchExam(batch,exam);
		if(autoAttendance == null)
		{
			System.out.println("INSERTING RECORDS");
			Auto_Attendence autoAttendanceData=new Auto_Attendence();
			autoAttendanceData.setBatch(batch);
			autoAttendanceData.setExam(exam);
			autoAttendanceData.setStart_date(start_date);
			autoAttendanceData.setEnd_date(end_date);
			autoAttendenceService.save(autoAttendanceData);
		}
		else
		{
			System.out.println("UPDATING RECORDS");
			autoAttendenceService.update(batch, exam,start_date,end_date);
		}
		
		return new ResponseEntity<String>("{\"data\" : \"Success\"}", HttpStatus.OK);
	}
	
	
	
	
	
	
	public ByteArrayInputStream getPrintManual(String batch,String exam,String admno,String attendence) throws InterruptedException, ExecutionException, MalformedURLException, DocumentException, IOException, ParseException 
	{
		//System.out.println("getting print manual");
		
		List<Student_Details> studentDetails = new ArrayList<>();
		if(admno.length()>0)
			studentDetails=studentDetailsService.searchByBatchAndAdmno(batch,admno);
		else
			studentDetails=studentDetailsService.searchByBatch(batch);
		List<Examination> examinationDetails=examinationService.findAllListByBbatch(batch);
		List<String> dynamicExamListString = new ArrayList<>();
 		if(examinationDetails!=null && !examinationDetails.isEmpty())
		{
			for(Examination examDetails:examinationDetails)
			{
				dynamicExamListString.add(examDetails.getExam_name());
			}
		}
		System.out.println("all examlist "+dynamicExamListString);
		List<String> exam_list= new ArrayList<>();
		List<String> exam_list_without_max=new ArrayList<>();
		if(dynamicExamListString!=null && !dynamicExamListString.isEmpty())
		{
			System.out.println("inside dynamic list");
			exam_list=examinationService.getExaminationTypeWithDynamicExamList(batch,dynamicExamListString);
			exam_list_without_max=examinationService.getExaminationTypeWithDynamicExamListWithoutmaxMarks(batch,dynamicExamListString);

			
		}
		else
		{
			exam_list=examinationService.getExaminationType(batch);
		}
		
		//examinationService.getExaminationType(batch);

		System.out.println("EXAM list dynamixx: "+exam_list);
		
		Map<String, Map<String, Map<String,String>>> subjectMap=new HashMap<String, Map<String, Map<String,String>>>();
		
		for(Student_Details st :  studentDetails)
		{
			//System.out.println("dynamicExamListString"+dynamicExamListString);
			System.out.println("admission number"+st.getAdmno());
			List<Map<String, Object>>  subjectMarksList=marksService.getSubjectMarksByBatch(batch,st.getAdmno(),dynamicExamListString);
			System.out.println("subjectMarksList"+subjectMarksList);
			Map<String, Map<String,String>> subject_exam_marks= new HashMap<String, Map<String,String>>();
			
			
			for(Map<String, Object> obj : subjectMarksList)
			{
				Map<String, String> exam_marks= new HashMap<String, String>();
				
				exam_marks.put(obj.get("exam").toString(), obj.get("marks").toString());
				exam_marks.put("grade", obj.get("grade").toString());
			
				subject_exam_marks.put(obj.get("subject").toString(),exam_marks);

			}
			subjectMap.put(String.valueOf(st.getStudent_id()), subject_exam_marks);
		}
		
		
		System.out.println("COMPLETE MAP: "+subjectMap);
		System.out.println("exmaslist till now"+exam_list);
		
		List<String> subject_list=marksService.searchByBatch(batch,dynamicExamListString);
		
		exam_list.add("Total of Classwork");
		//exam_list.add("Final Exam");
		exam_list.add("50% of Total Classwork");
		exam_list.add("50% of Final Exam");
		exam_list.add("Total");
		exam_list.add("Grade");
		exam_list.add("Remarks");
		
		//exam_list_without_max.add("Total of Classwork");
		//exam_list.add("Final Exam");
		//exam_list_without_max.add("50% of Total Classwork");
	//	exam_list_without_max.add("50% of Final Exam");
		//exam_list_without_max.add("Total");
		//exam_list_without_max.add("Grade");
	//	exam_list_without_max.add("Remarks");
		
		
		
		
		
		List<String> exam_skills_list=marksService.searchSkillsByBatch(batch);
		List<String> exam_annual_list=marksService.searchAnnulaExamg2ByBatch(batch);
		
//		System.out.println(exam_list);
		System.out.println("annual exam list"+exam_annual_list);
		
		PDFUtilities pdf= new PDFUtilities();
		pdf.openDoc("S.D.A HIGHER SECONDARY SCHOOL", "NUZVID - 521201", "(Under the management of METAS - SURAT)", "Ph : 08656 - 232302",logoPath);
		
		ByteArrayInputStream bis = null;
		System.out.println("MAP: "+subjectMap);
		
		List<String> marksAdmList=marksService.findListByBatch(batch);
		double classAverage=0.0;
		double totalAverage=0.0;
		HashMap<String, Double> examAverage= new HashMap<>();
		HashMap<String, Double> examClassAverage= new HashMap<>();
		List<Student_Details> studentDetailsTotal=studentDetailsService.searchByBatch(batch);
		for(Student_Details student : studentDetailsTotal)
		{
			
			examAverage=	pdf.getClassAverageOfStudent(remarksService,holidayService,marksService,maxMinService,workingDaysService,studentDetailsService,student,subject_list,exam_skills_list,exam_annual_list, exam_list,subjectMap,attendence,"","","","S.D.A HIGHER SECONDARY SCHOOL", "NUZVID - 521201", "(Under the management of METAS - SURAT)", "Ph : 08656 - 232302",exam_list_without_max);
			System.out.println(" examAverage is"+examAverage);
			Set<String> keyset=examAverage.keySet();
			for(String key:keyset)
			{
				if(examClassAverage.containsKey(key))
				{
					examClassAverage.put(key, examClassAverage.get(key)+examAverage.get(key));
				}
				else
				{
					examClassAverage.put(key, examAverage.get(key));

				}
			}

		}
		System.out.println("examClassAverage"+examClassAverage);
		Set<String> keyset=examClassAverage.keySet();
		DecimalFormat df2 = new DecimalFormat(".##");

		for(String key:keyset)
		{
			if(examClassAverage.containsKey(key))
			{
				examClassAverage.put(key,Double.parseDouble(df2.format(examClassAverage.get(key)/studentDetailsTotal.size())));
			}
		}
		System.out.println("final class average "+examClassAverage);
		for(Student_Details student : studentDetails)
		{
			//
			
			if(marksAdmList.contains(student.getAdmno())){
				pdf.createHeaderManual(remarksService,holidayService,marksService,maxMinService,workingDaysService,studentDetailsService,student,subject_list,exam_skills_list,exam_annual_list, exam_list,subjectMap,attendence,"","","","S.D.A HIGHER SECONDARY SCHOOL", "NUZVID - 521201", "(Under the management of METAS - SURAT)", "Ph : 08656 - 232302",exam_list_without_max,examClassAverage);
			}
		}
		bis=pdf.closeDoc();
		System.out.println("PROCESS DONE:");
		
		

        return bis;
		//return new ResponseEntity<String>("success", HttpStatus.OK);
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
