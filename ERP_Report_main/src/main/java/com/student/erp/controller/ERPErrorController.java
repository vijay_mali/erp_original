package com.student.erp.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ERPErrorController implements ErrorController{
	private final static String ERROR_PATH = "/error";
	
	 @RequestMapping(value = ERROR_PATH, produces = "text/html")
	    public String errorHtml(HttpServletRequest request) {
		 return "redirect:/login?error";
	    }
	 

	public String getErrorPath() {
		// TODO Auto-generated method stub
		return ERROR_PATH;
	}
	
	

}
