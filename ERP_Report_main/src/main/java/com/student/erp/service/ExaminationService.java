package com.student.erp.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.student.erp.entities.Examination;

public interface ExaminationService {

	
	//public Page<Examination> findAll(Pageable page);
	//public List<Examination> findAllList();
	
	public List<Examination> findAllListByBbatch(String batch);
	
	public List<String> getExaminationType(String batch);

	public List<String> getExaminationTypeWithDynamicExamList(String batch, List<String> dynamicExamListString);

	public List<String> getExaminationTypeWithDynamicExamListWithoutmaxMarks(String batch,
			List<String> dynamicExamListString);
	
	
}
