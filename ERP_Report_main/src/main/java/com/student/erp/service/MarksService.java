package com.student.erp.service;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.student.erp.entities.BatchSubject;
import com.student.erp.entities.Marks;

public interface MarksService {
	
	public Page<Marks> findAll(Pageable page);
	
	public List<String> searchByBatch(String batch,List<String> examList);
	
	public List<String> searchAnnulaExamg2ByBatch(String batch);
	

	public List<String> searchSkillsByBatch(String batch);
	
	public List<Map<String, Object>> getSubjectMarksByBatch(String batch,String admno, List<String> examList);
	
	public List<Map<String, Object>> getFullSubjectDetails(String admno,String batch,String subject);
	
	public List<String> listExam();
	
	public List<String> findListByBatchExam(String batch,String exam);
	
	public List<String> findListByBatch(String batch);
	
	public List<Map<String, Object>> getStudentMarksManualAll();
	
	public List<Map<String, Object>> getStudentMarksManualByBatch(String batch);
	
	
	
	public BatchSubject getSubjectByBatch(String batch);
	
	public List<Marks> findSkillsSubjectMark(String batch,String admno,String exam, String subject);
	
	public List<Map<String, Object>> getStudentRankInClass(String batch,String exam);
	
	
	public List<Object> getAllStudentsTotalMarks(String batch, String exam);

	public List<Map<String, Object>> getStudentRankInClassForFullExam(String batch, List<String> exam_list_without_max);
}

