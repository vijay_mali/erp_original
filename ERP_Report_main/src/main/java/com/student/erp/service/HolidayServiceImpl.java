package com.student.erp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.student.erp.repositories.users.HolidayRepo;

@Service("holidayService")
public class HolidayServiceImpl implements HolidayService{
	
	@Autowired
	private HolidayRepo holidayRepo;
	
	public Long getCountByDate(String startDate, String endDate)
	{
		
		return holidayRepo.getCountByDate(startDate, endDate);
	}

}
