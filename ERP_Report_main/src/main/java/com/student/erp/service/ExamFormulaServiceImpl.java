package com.student.erp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.student.erp.entities.Exam_Formula;
import com.student.erp.repositories.users.ExamFormulaRepo;

@Service("examFormulaService")
public class ExamFormulaServiceImpl implements ExamFormulaService{

	
	@Autowired
	private ExamFormulaRepo examFormulaRepo;
	
	
	public Page<Exam_Formula> findAll(Pageable page) {
		return examFormulaRepo.findAll(page);
	}
	
	

}
