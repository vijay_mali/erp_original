package com.student.erp.service;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.student.erp.MasterDBConfigProperties;
import com.student.erp.master.entities.SchoolCode;
import com.student.erp.master.service.SchoolCodeService;
import com.student.erp.repositories.users.UserRepository;

public class CustomAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	
	 @Autowired
	 private UserRepository userRepository;
	 
	 private JdbcTemplate jdbcTemplate;
	 
	 @Autowired
	 MasterDBConfigProperties masterDbProperties;
	 
	 @Value("${app.db.url}")
	 private String DB_CONNECTION_STRING;
	 
	 @Autowired
	 private SchoolCodeService schoolCodeService;
	 
    @Override
    protected String obtainUsername(HttpServletRequest request) {
        String username = super.obtainUsername(request);
        String extraparam = request.getParameter("scode");
        System.out.println("User name in authentication filter "+ username + " extra param "+ extraparam);
        return username+"|"+extraparam;
    }
    
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
    	
    			 System.out.println("in attempt authentication");
    	         String username = obtainUsername(request);
    	         String password = obtainPassword(request);
    	 
    	         if (username == null) {
    	             username = "";
    	         }
    	 
    	         if (password == null) {
    	             password = "";
    	         }
    	 
    	         username = username.trim();
    	         
    	         System.out.println("user name in attempt auth " + username);
    	         String[] parts = username.split("\\|");
    	     		String email = parts[0]; 
    	     		String scode = parts[1];
    	         
    	     		String dbName="";
    	     		SchoolCode schoolCode=schoolCodeService.findbySchoolCode(scode);
    	     		if(schoolCode!=null)
    	     		{
        	     		dbName=schoolCode.getDb_name();

    	     		}
    	     		/*DriverManagerDataSource dataSource = new DriverManagerDataSource();
    	            dataSource.setDriverClassName(masterDbProperties.getDriverClassName());
    	            dataSource.setUrl(DB_CONNECTION_STRING + dbName);
    	            dataSource.setUsername(masterDbProperties.getUsername());
    	            dataSource.setPassword(masterDbProperties.getPassword());*/
    	            
    	     		request.getSession().setAttribute("USER_DB", dbName);
    	            
    	        /* User user = userRepository.findByName(email,scode);
    	         if(user != null)
    	            {
    	            	System.out.println("************** SETTING TENNTss"+user.getName());
    	            	request.getSession().setAttribute("CURRENT_TENANT",user.getName());
    	            }
    	         else
    	         {
    	        	 System.out.println("user is null");
    	         }
    	        */
    	         UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
    
    	         setDetails(request, authRequest);
    	 
    	        // return super.attemptAuthentication(request, response);
    	         return this.getAuthenticationManager().authenticate(authRequest);
    	     }

}