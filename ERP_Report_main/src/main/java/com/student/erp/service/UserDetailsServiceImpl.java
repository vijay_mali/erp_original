package com.student.erp.service;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.student.erp.entities.ERPUserDetails;
import com.student.erp.entities.User;
import com.student.erp.master.entities.SchoolCode;
import com.student.erp.master.service.SchoolCodeService;
import com.student.erp.repositories.users.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private SchoolCodeService schoolCodeService;
    
    @Autowired
    private HttpServletRequest request;
    
    @Autowired
    DataSource ds;
    
   
    @Transactional(readOnly = true)
    public ERPUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
    	
    	String[] parts = username.split("\\|");
    	String email = parts[0]; 
    	String scode = parts[1];
    	
    	System.out.println(scode+"USERNAME: "+username);
    	
    	SchoolCode schoolCode=schoolCodeService.findbySchoolCode(scode);
    	System.out.println("GOT DB_ "+schoolCode.getDb_name());
    	
    	
    	try {
			System.out.println("GOT DB_** "+ds.getConnection().getCatalog());
			
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	User user =  null;
		try {
			user = userRepository.findByName(email,scode);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	Set<GrantedAuthority> grantedAuthorities = new HashSet();
      //  for (Role role : user.getRoles()){
            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
       // }
           
            ERPUserDetails erpUsers = new ERPUserDetails();
            erpUsers.setUser(user);
            erpUsers.setAuthorities(grantedAuthorities);
        return erpUsers;
    }
    
}