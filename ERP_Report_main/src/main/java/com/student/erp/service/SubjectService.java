package com.student.erp.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.student.erp.entities.Subject;

public interface SubjectService {

	
	//public Page<Subject> findAll(Pageable page);
	
	public List<String> searchByBatch(String batch);
	
}

