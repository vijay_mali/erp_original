package com.student.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.student.erp.entities.Student_Details;
import com.student.erp.entities.Temp_Attendance;
import com.student.erp.repositories.users.StudentDetailsRepo;
import com.student.erp.repositories.users.TempAttendanceRepo;

@Service("studentDetailsService")
public class StudentDetailsServiceImpl implements StudentDetailsService{

	
	@Autowired
	private StudentDetailsRepo studentDetailsRepo;
	
	
	@Autowired
	private TempAttendanceRepo tempAttendanceRepo;
	
	
	public Page<Student_Details> findAll(Pageable page) {
		return studentDetailsRepo.findAll(page);
	}
	
	
	public List<Student_Details> findAllList() {
		return studentDetailsRepo.findAll();
	}


	public String getAttendence(String admno, String session) {
		
		return studentDetailsRepo.getAttendence(admno, session);
		
	}


	public List getBatchList() {
		return studentDetailsRepo.getBatchList();
	}


	public List<Student_Details> searchByBatch(String batch) {
		return studentDetailsRepo.searchByBatch(batch);
	}


	public Student_Details findOne(long id) {
		
		return studentDetailsRepo.findOne(id);
	}


	public Student_Details saveStudent(Student_Details student) {
		
		return studentDetailsRepo.save(student);
	}
	
	public Long countByBatch(String batch)
	{
		return studentDetailsRepo.countByBatch(batch);
	}


	public Temp_Attendance getManualAttendance(String admno) {
		return tempAttendanceRepo.findByAdmno(admno);
	}


	public Student_Details findByAdmNo(String admno) {
		return studentDetailsRepo.findByAdmno(admno);
	}


	public List<Temp_Attendance> getManualAttendanceWokringDays(String batch,String exam, String admno){
		return tempAttendanceRepo.getManualAttendanceWokringDays(batch, exam,admno);
	}


	public int saveTempAttendance(String admno, String attendance) {
		return tempAttendanceRepo.updateData(admno, attendance);
	}


	public Temp_Attendance getManualAttendanceByExam(String admno, String exam) {
		return tempAttendanceRepo.findByAdmnoAndExam(admno, exam);
	}


	public List<Temp_Attendance> getManualAttendanceWokringDaysByAdmno(String batch, String admno) {
		return tempAttendanceRepo.getManualAttendanceWokringDaysByAdmno(batch,admno);
	}


	@Override
	public List<Student_Details> searchByBatchAndAdmno(String batch, String admno) {
		return studentDetailsRepo.searchByBatchAndAdmno(batch,admno);
	}
	

}
