package com.student.erp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.student.erp.entities.Attendence_Period;

public interface AttendencePeriodService {

	
	public Page<Attendence_Period> findAll(Pageable page);


}
