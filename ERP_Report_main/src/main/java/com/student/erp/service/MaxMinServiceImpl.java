package com.student.erp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.student.erp.entities.Max_Min_Marks;
import com.student.erp.repositories.users.Max_Min_Repo;

@Service("maxMinService")
public class MaxMinServiceImpl implements MaxMinService{

	
	@Autowired 
	private Max_Min_Repo maxMinRepo;
	
	/*public Page<Max_Min_Marks> findAll(Pageable page) {
		return maxMinRepo.findAll(page);
	}*/
	
	
	public String getMaxMarksByExam(String exam_name,String batch){
		return maxMinRepo.getMaxMarksByExam(exam_name, batch);
	}
	

}
