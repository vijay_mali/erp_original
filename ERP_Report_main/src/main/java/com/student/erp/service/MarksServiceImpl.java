package com.student.erp.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.student.erp.entities.BatchSubject;
import com.student.erp.entities.Marks;
import com.student.erp.repositories.users.BatchSubjectRepo;
import com.student.erp.repositories.users.MarksRepo;

@Service("marksService")
public class MarksServiceImpl implements MarksService{

	@Autowired
	private MarksRepo marksRepo;
	
	@Autowired
	private BatchSubjectRepo batchSubjectRepo;
	
	
	
	public Page<Marks> findAll(Pageable page) {
		return marksRepo.findAll(page);
	}
	
	public List<String> searchByBatch(String batch,List<String> examList)
	{
		return marksRepo.searchByBatch(batch,examList);
	}
	
	
	public List<String> findListByBatchExam(String batch,String exam)
	{
		return marksRepo.findListByBatchExam(batch, exam);
		
	}

	public List<String> searchAnnulaExamg2ByBatch(String batch) {
		return marksRepo.searchAnnulaExamg2ByBatch(batch);
	}

	public List<String> searchSkillsByBatch(String batch) {
		return marksRepo.searchSkillsByBatch(batch);
	}
	
	public List<Map<String, Object>> getSubjectMarksByBatch(String batch,String admno, List<String> examList)
	{
		return marksRepo.getSubjectMarksByBatch(batch,admno,examList);
	}
	
	public List<Map<String, Object>> getFullSubjectDetails(String admno,String batch,String subject){
		return marksRepo.getFullSubjectDetails(admno, batch);
	}
	
	
	
	public BatchSubject getSubjectByBatch(String batch){
		return batchSubjectRepo.findListByBatchExam(batch);
	}
	
	

	public List<String> listExam()
	{
		return marksRepo.listExam();
	}

	public List<Map<String, Object>> getStudentMarksManualAll() {
		return  marksRepo.getStudentMarksManualAll();
	}
	
	public List<Map<String, Object>> getStudentMarksManualByBatch(String batch)
	{
		return  marksRepo.getStudentMarksManualByBatch(batch);
	}

	public List<Marks> findSkillsSubjectMark(String batch, String admno,String exam,String subject) {
		return marksRepo.findSkillsSubjectMark(batch, admno,exam,subject);
	}

	public List<Map<String, Object>> getStudentRankInClass(String batch, String exam) {
		return marksRepo.getStudentRankInClass(batch,exam);
	}

	public List<Object> getAllStudentsTotalMarks(String batch, String exam) {
		return marksRepo.getAllStudentsTotalMarks(batch,exam);
	}

	public List<String> findListByBatch(String batch) {
		return marksRepo.findListByBatch(batch);
	}

	@Override
	public List<Map<String, Object>> getStudentRankInClassForFullExam(String batch,
			List<String> exam_list_without_max) {
		return marksRepo.getStudentRankInClassForFullExam(batch,exam_list_without_max);
	}
}
