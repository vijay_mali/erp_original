package com.student.erp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.student.erp.entities.Exam_Formula;

public interface ExamFormulaService {

	
	public Page<Exam_Formula> findAll(Pageable page);


}
