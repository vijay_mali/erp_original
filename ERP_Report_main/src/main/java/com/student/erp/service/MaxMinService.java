package com.student.erp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.student.erp.entities.Max_Min_Marks;

public interface MaxMinService {

	//public Page<Max_Min_Marks> findAll(Pageable page);
	public String getMaxMarksByExam(String exam_name,String batch);
	
}
