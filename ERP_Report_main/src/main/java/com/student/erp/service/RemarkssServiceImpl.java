package com.student.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.student.erp.entities.Marks;
import com.student.erp.entities.Remarks;
import com.student.erp.entities.Working_days;
import com.student.erp.repositories.users.MarksRepo;
import com.student.erp.repositories.users.RemarksRepo;
import com.student.erp.repositories.users.WorkingDaysRepo;

@Service("remarkssServiceImpl")
public class RemarkssServiceImpl implements RemarksService{

	
	@Autowired
	private RemarksRepo remarksRepo;
	
	
	public Remarks searchByStudent_id(String student_id) {
		return remarksRepo.findBystudent_id(student_id);
	}

	public Remarks update(String value, String student_id) {
		return remarksRepo.update(value, student_id);
	}

	public List<Remarks> searchByStudent(String student_id) {
		return remarksRepo.searchByStudent(student_id);
	}

	public int updateData(String admno, String label, String value) {
		
		return remarksRepo.updateData(admno,label,value);
	}

	public List<Remarks> findRemarksByStudentId(String student_id) {
		return remarksRepo.findRemarksByStudentId(student_id);
	}

	public List<Remarks> findResultByStudentId(String student_id, String examtype, String label) {
		return remarksRepo.findResultByStudentId(student_id, examtype, label);
	}

	public int updateRemarksData(String admno, String label, String value) {
		return remarksRepo.updateRemarksData(admno, label, value);
	}

	public int updateResultData(String admno, String label, String value, String exam) {
		return remarksRepo.updateResultData(admno, label, value, exam);
	}
	

}
