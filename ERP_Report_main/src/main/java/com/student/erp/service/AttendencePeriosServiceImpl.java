package com.student.erp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.student.erp.entities.Attendence_Period;
import com.student.erp.repositories.users.AtendencePeriodRepo;

@Service("attendencePeriodService")
public class AttendencePeriosServiceImpl implements AttendencePeriodService{

	
	@Autowired
	private AtendencePeriodRepo atendencePeriodRepo;
	
	
	public Page<Attendence_Period> findAll(Pageable page) {
		return atendencePeriodRepo.findAll(page);
	}
	
	

}
