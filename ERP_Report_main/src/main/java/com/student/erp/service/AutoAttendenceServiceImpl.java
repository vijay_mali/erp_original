package com.student.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.student.erp.entities.Attendence_Period;
import com.student.erp.entities.Auto_Attendence;
import com.student.erp.repositories.users.AtendencePeriodRepo;
import com.student.erp.repositories.users.AutoAttendenceRepo;

@Service("autoAttendenceService")
public class AutoAttendenceServiceImpl implements AutoAttendenceService{

	
	@Autowired
	private AutoAttendenceRepo autoAttendenceRepo;

	public Auto_Attendence save(Auto_Attendence autoAttendance) {
		return autoAttendenceRepo.save(autoAttendance);
	}

	public int update(String batch, String exam, String start_date, String end_date) {
		return autoAttendenceRepo.update(batch, exam,start_date,end_date);
	}

	public Auto_Attendence findByBatchExam(String batch, String exam) {
		
		return autoAttendenceRepo.findByBatchAndExam(batch, exam);
	}

	public Auto_Attendence findByBatch(String batch) {
		return autoAttendenceRepo.findByBatch(batch);
	}

	public List<Auto_Attendence> findByBatchDesc(String batch) {
		return autoAttendenceRepo.findByBatchDesc(batch);
	}
	
	
	

}
