package com.student.erp.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.student.erp.entities.Attendence_Period;
import com.student.erp.entities.Auto_Attendence;

public interface AutoAttendenceService {

	public Auto_Attendence save(Auto_Attendence autoAttendance);
	
	public int  update(String batch, String exam, String start_date, String end_date);
	
	public Auto_Attendence findByBatchExam(String batch, String exam);
	
	public Auto_Attendence findByBatch(String batch);
	
    public List<Auto_Attendence> findByBatchDesc(String batch);
}
