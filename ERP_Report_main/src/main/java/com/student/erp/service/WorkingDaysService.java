package com.student.erp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.student.erp.entities.Marks;
import com.student.erp.entities.Temp_Attendance;
import com.student.erp.entities.Working_days;

public interface WorkingDaysService {
	
//	public Page<Working_days> findAll(Pageable page);
	
	
	public Working_days searchByBatch(String batch);

	public Working_days update(int working_days, String batch);
	
	public int updateTempWorkingDays(String batch, String exam, String working_days);
	

}
