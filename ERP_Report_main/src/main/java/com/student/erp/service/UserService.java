package com.student.erp.service;

import java.sql.SQLException;
import java.util.List;

import com.student.erp.entities.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username) throws SQLException;
    
    List<User> findAll();
}