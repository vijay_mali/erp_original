package com.student.erp.service;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.student.erp.entities.User;
import com.student.erp.repositories.users.UserRepository;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    /*@Autowired
    private RoleRepository roleRepository;*/
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
       // user.setRoles(new HashSet(roleRepository.findAll()));
      //  userRepository.save(user);
    }

    public User findByUsername(String username) throws SQLException {
    	
    	        User user = userRepository.findByName(username,"1");
    	        if (user == null) {
    	            throw new UsernameNotFoundException(
    	              "Username not found for domain, username=");
    	        }
        return user;
    }

    
	@Override
	public List<User> findAll() {
//		return userRepository.findAll();
		return null;
	}

   
}