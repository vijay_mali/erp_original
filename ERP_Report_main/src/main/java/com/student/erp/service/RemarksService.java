package com.student.erp.service;

import java.util.List;

import com.student.erp.entities.Remarks;

public interface RemarksService {
	
//	public Page<Working_days> findAll(Pageable page);
	
	
	public Remarks searchByStudent_id(String student_id);

	public Remarks update(String value, String student_id);
	
	public List<Remarks> searchByStudent(String student_id);

	public int updateData(String admno, String label, String value);
	
	
	public List<Remarks> findRemarksByStudentId(String student_id);
	
	public List<Remarks> findResultByStudentId(String student_id,String examtype, String label);
	
	public int updateRemarksData(String admno, String label, String value);
	
	public int updateResultData(String admno, String label, String value,String exam);
	
}
