package com.student.erp.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.student.erp.entities.Attendence_Period;
import com.student.erp.entities.Student_Details;
import com.student.erp.entities.Temp_Attendance;

public interface StudentDetailsService {

	
	public Page<Student_Details> findAll(Pageable page);
	
	public List<Student_Details> findAllList();
	
	public String getAttendence(String admno, String session);
	
	public List getBatchList();
	
	public Student_Details findOne(long id);
	
	public Student_Details saveStudent(Student_Details student);
	
	
	public List<Student_Details> searchByBatch(String batch);
	
	public List<Student_Details> searchByBatchAndAdmno(String batch, String admno);
	
	public Long countByBatch(String batch);

	public Temp_Attendance getManualAttendance(String admno);
	
	public Temp_Attendance getManualAttendanceByExam(String admno,String exam);
	
	public Student_Details findByAdmNo(String admno);
	
	public List<Temp_Attendance> getManualAttendanceWokringDays(String batch,String exam, String admno);
	
	public List<Temp_Attendance> getManualAttendanceWokringDaysByAdmno(String batch, String admno);
	
	public int saveTempAttendance(String admno, String attendance);
	
	
}
