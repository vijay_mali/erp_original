package com.student.erp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.student.erp.entities.Marks;
import com.student.erp.entities.Temp_Attendance;
import com.student.erp.entities.Working_days;
import com.student.erp.repositories.users.MarksRepo;
import com.student.erp.repositories.users.TempAttendanceRepo;
import com.student.erp.repositories.users.WorkingDaysRepo;

@Service("workingDaysService")
public class WorkingDaysServiceImpl implements WorkingDaysService{

	@Autowired
	private WorkingDaysRepo workingDaysRepo;
	
	@Autowired
	private TempAttendanceRepo tempAttendanceRepo;
	
	public Working_days searchByBatch(String batch) {
		return workingDaysRepo.findByBatch(batch);
	}

	public Working_days  update(int working_days, String batch){
		return workingDaysRepo.update(working_days,batch);
	}
	
	public int updateTempWorkingDays(String batch, String exam, String working_days)
	{
		return tempAttendanceRepo.updateTempWorkingDays(batch, exam, working_days);
	}
	
	

}
