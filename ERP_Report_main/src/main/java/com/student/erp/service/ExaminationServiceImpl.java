package com.student.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.student.erp.entities.Examination;
import com.student.erp.repositories.users.ExaminationRepo;

@Service("examinationService")
public class ExaminationServiceImpl implements ExaminationService{

	
	@Autowired
	private ExaminationRepo examinationRepo;
	
	
	/*public Page<Examination> findAll(Pageable page) {
		return examinationRepo.findAll(page);
	}*/


/*	public List<Examination> findAllList() {
		//return examinationRepo.findAll();
		
	}
*/

	public List<Examination> findAllListByBbatch(String batch) {
		return examinationRepo.searchByBatch(batch);
	}


	public List<String> getExaminationType(String batch) {
		return examinationRepo.getExaminationType(batch);
	}


	@Override
	public List<String> getExaminationTypeWithDynamicExamList(String batch, List<String> dynamicExamListString) {
		return examinationRepo.getExaminationTypeWithDynamicExamList(batch,dynamicExamListString);
	}


	@Override
	public List<String> getExaminationTypeWithDynamicExamListWithoutmaxMarks(String batch,
			List<String> dynamicExamListString) {
		return examinationRepo.getExaminationTypeWithDynamicExamListWithoutmaxMarks(batch,dynamicExamListString);

	}
	
	

}
