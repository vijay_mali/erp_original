package com.student.erp.utils;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

public class PdfEvents extends PdfPageEventHelper{

	private String company="";
	private String subaddress="";
	private String affltn="";
	private String contact="";
	private String logoPath="";
	
	
	public PdfEvents(String Company, String subaddress, String affltn, String contact,String logoPath)
	{
		this.company= Company;
		this.subaddress= subaddress;
		this.affltn= affltn;
		this.contact= contact;
		this.logoPath=logoPath;
	}
	
	public void onStartPage(PdfWriter writer, Document document) {
       /* ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("Top Left"), 30, 800, 0);
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("Top Right"), 550, 800, 0);
        
        *
        */
		
		if(document.getPageNumber() == 1){
		
		Image image;
	    try {
	        image = Image.getInstance(this.logoPath);
	        image.setAlignment(Element.ALIGN_RIGHT);
	        image.setAbsolutePosition(120, 1060);
	        
	
	        image.scalePercent(47.5f, 47.5f);
	        writer.getDirectContent().addImage(image, true);
	    } catch (Exception e) {
	       System.out.println("Error Occured while preparing header.");
	       e.printStackTrace();
	    }
	    
	    Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
	    Font boldFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
	    Font boldFont2 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
	    Font boldFont3 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);

	    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(""), 30, 800, 0);
	    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(this.company, boldFont), 370, 1100, 0);
	    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(this.subaddress,boldFont1), 370, 1080, 0);
	    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(this.affltn,boldFont2), 370, 1065, 0);
	    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(this.contact,boldFont3), 370, 1050, 0);
		}
	 //   LineSeparator lines= new LineSeparator();
	    
	   // ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, lines, 300, 750, 0);
    }
}
