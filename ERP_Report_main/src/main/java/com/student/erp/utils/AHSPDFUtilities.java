package com.student.erp.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.student.erp.entities.Auto_Attendence;
import com.student.erp.entities.BatchSubject;
import com.student.erp.entities.Marks;
import com.student.erp.entities.Remarks;
import com.student.erp.entities.Student_Details;
import com.student.erp.entities.Temp_Attendance;
import com.student.erp.entities.Working_days;
import com.student.erp.service.HolidayService;
import com.student.erp.service.MarksService;
import com.student.erp.service.MaxMinService;
import com.student.erp.service.RemarksService;
import com.student.erp.service.StudentDetailsService;
import com.student.erp.service.WorkingDaysService;

public class AHSPDFUtilities {

	
/*	
	Document document;
	ByteArrayOutputStream out;
	PdfWriter writer;
	
	public void openDoc(String name, String subaddress, String affiliation,String contact,String logoPath) throws DocumentException
	{
		document = new Document(PageSize.A3, 36, 36, 154, 54);
		out = new ByteArrayOutputStream();
		writer =PdfWriter.getInstance(document, out);
		PdfEvents event = new PdfEvents(name, subaddress, affiliation,contact,logoPath);
		writer.setPageEvent(event);
		document.open();
	}
	
	public void  createHeader(RemarksService remarksService,HolidayService holidayService,MarksService marksService,MaxMinService maxMinService,WorkingDaysService workingDaysService,StudentDetailsService studentDetailsService,Student_Details student,List<String> subject_list,List<String> subject_skils_list,List<String> subject_annual_list,List<String> exam_list,Map<String, Map<String, Map<String,String>>> subjectMap,String attendence,String exams,List<String> examSequenceList,List<Auto_Attendence> autoAttendanceList, String imagePath, String name, String subaddress, String affiliation,String contact) throws DocumentException, MalformedURLException, IOException, ParseException
	{
		
		
		String attendenceCount="0";
		String attendencePercentage="%";
		int totalAttendence=0;
		int totalPresentDay=0,total_student_count=0;
		
		
		
		List<String> table_header=new ArrayList<String>();
	//	table_header.add("Subject");
		table_header.add("First Periodic");
		table_header.add("First Terminal");
		table_header.add("Second Periodic");
		table_header.add("Second Terminal");
		
		table_header.add("Total of Classwork");
		table_header.add("Final Exam");
		table_header.add("50% Of Total Classwork");
		table_header.add("50% Of Final Exam");
		table_header.add("Total");
		table_header.add("Grade");
		table_header.add("Remarks");
		
		
		
		List<String> additionalList=new ArrayList<String>();
		additionalList.add("Total");
		additionalList.add("Average");
		additionalList.add("Class Average");
		additionalList.add("No.of Working Days");
		additionalList.add("No.of Days Present");
		additionalList.add("No.of Pupils in Class");
		additionalList.add("Rank in class");
		
		List<String> table2Headers=new ArrayList<String>();
		table2Headers.add("First Periodic");
		table2Headers.add("First Terminal");
		table2Headers.add("Second Periodic");
		table2Headers.add("Second Terminal");
		table2Headers.add("Final Exam");
		
		double total_FirstPeriodic=0.0;
		int total_no_working_days=0,total_no_days_present=0,total_no_pupils=0,total_no_rank=0;
		
		Map<String, String> datMap=new HashMap<String, String>();
		if(examSequenceList.contains("First Periodic") || examSequenceList.contains("Final Exam"))
		{
			datMap.put("First_Periodic_Total", "0.0");
			if(!attendence.equalsIgnoreCase("manualattendance"))
			{
				String startDate=null, endDate=null;
				if(autoAttendanceList.size()>0)
				{
					for(Auto_Attendence  auto_attendence : autoAttendanceList)
					{
						if(auto_attendence.getExam().equalsIgnoreCase("First Periodic"))
						{
							datMap.put("First_Periodic_Start_Date", auto_attendence.getStart_date());
							datMap.put("First_Periodic_End_Date", auto_attendence.getEnd_date());
						}
					}
				}
			}
		}
		if(examSequenceList.contains("Second Periodic") || examSequenceList.contains("Final Exam"))
		{
			datMap.put("Second_Periodic_Total", "0.0");
			if(!attendence.equalsIgnoreCase("manualattendance"))
			{
				String startDate=null, endDate=null;
				if(autoAttendanceList.size()>0)
				{
					for(Auto_Attendence  auto_attendence : autoAttendanceList)
					{
						if(auto_attendence.getExam().equalsIgnoreCase("Second Periodic"))
						{
							datMap.put("Second_Periodic_Start_Date", auto_attendence.getStart_date());
							datMap.put("Second_Periodic_End_Date", auto_attendence.getEnd_date());
						}
					}
				}
			}
		}
		if(examSequenceList.contains("First Terminal") || examSequenceList.contains("Final Exam"))
		{
			datMap.put("First_Terminal_Total", "0.0");
			if(!attendence.equalsIgnoreCase("manualattendance"))
			{
				String startDate=null, endDate=null;
				if(autoAttendanceList.size()>0)
				{
					for(Auto_Attendence  auto_attendence : autoAttendanceList)
					{
						if(auto_attendence.getExam().equalsIgnoreCase("First Terminal"))
						{
							datMap.put("First_Terminal_Start_Date", auto_attendence.getStart_date());
							datMap.put("First_Terminal_End_Date", auto_attendence.getEnd_date());
						}
					}
				}
			}
		}
		if(examSequenceList.contains("Second Terminal") || examSequenceList.contains("Final Exam"))
		{
			datMap.put("Second_Terminal_Total", "0.0");
			if(!attendence.equalsIgnoreCase("manualattendance"))
			{
				String startDate=null, endDate=null;
				if(autoAttendanceList.size()>0)
				{
					for(Auto_Attendence  auto_attendence : autoAttendanceList)
					{
						if(auto_attendence.getExam().equalsIgnoreCase("Second Terminal"))
						{
							datMap.put("Second_Terminal_Start_Date", auto_attendence.getStart_date());
							datMap.put("Second_Terminal_End_Date", auto_attendence.getEnd_date());
						}
					}
				}
			}
		}
		
		if( examSequenceList.contains("Final Exam"))
		{
			datMap.put("Final_Exam_Total", "0.0");
		
			datMap.put("50%_Of_Final_Exam_Total", "0.0");
			datMap.put("Total_of_Classwork_Total", "0.0");
			datMap.put("50%_Of_Total_Classwork_Total", "0.0");
			datMap.put("Total_Total", "0.0");
			
			if(!attendence.equalsIgnoreCase("manualattendance"))
			{
				String startDate=null, endDate=null;
				if(autoAttendanceList.size()>0)
				{
					for(Auto_Attendence  auto_attendence : autoAttendanceList)
					{
						if(auto_attendence.getExam().equalsIgnoreCase("Final Exam"))
						{
							datMap.put("Final_Exam_Start_Date", auto_attendence.getStart_date());
							datMap.put("Final_Exam_End_Date", auto_attendence.getEnd_date());
						}
					}
				}
			}
		}
		int working_days=0;
		long student_count=0;
		Working_days workingDays=workingDaysService.searchByBatch(student.getBatch());
		
		if(workingDays!= null)
		{
			working_days=workingDays.getWorking_days();
		}
		
		
		
		student_count=studentDetailsService.countByBatch(student.getBatch());
		List<Object[]> fullSubjectList=marksService.getFullSubjectDetails(student.getAdmno(),student.getBatch(),"");
		BatchSubject batchSubject=marksService.getSubjectByBatch(student.getBatch());
		List<String> subjects = Arrays.asList(batchSubject.getSubject().split("\\s*,\\s*"));
		
		NumberFormat nf= NumberFormat.getInstance();
        nf.setMaximumFractionDigits(0);
        
		int total_subject_count=1;
		for(Object[] obj : fullSubjectList)
		{
			if(examSequenceList.contains("First Periodic") || examSequenceList.contains("Final Exam"))
			{
				datMap.put("First Periodic_"+obj[0], (obj[3] == null ? "" : nf.format(Double.valueOf(obj[3].toString()))));
				datMap.put("First_Periodic_Total", String.valueOf(Double.valueOf(datMap.get("First_Periodic_Total"))+(obj[3] == null ? 0.0 : Double.valueOf(obj[3].toString()))));
			}
			
			if(examSequenceList.contains("Second Periodic") || examSequenceList.contains("Final Exam"))
			{
				datMap.put("Second Periodic_"+obj[0], (obj[4] == null ? "" : nf.format(Double.valueOf(obj[4].toString()))));
				datMap.put("Second_Periodic_Total", String.valueOf(Double.valueOf(datMap.get("Second_Periodic_Total"))+(obj[4] == null ? 0.0 : Double.valueOf(obj[4].toString()))));
			}
			
			if(examSequenceList.contains("First Terminal") || examSequenceList.contains("Final Exam"))
			{
				datMap.put("First Terminal_"+obj[0], (obj[5] == null ? "" : nf.format(Double.valueOf(obj[5].toString()))));
			
				datMap.put("First_Terminal_Total", String.valueOf(Double.valueOf(datMap.get("First_Terminal_Total"))+(obj[5] == null ? 0.0 : Double.valueOf(obj[5].toString()))));
			}
			if(examSequenceList.contains("Second Terminal") || examSequenceList.contains("Final Exam"))
			{
				datMap.put("Second Terminal_"+obj[0], (obj[6] == null ? "" : nf.format(Double.valueOf(obj[6].toString()))));
				datMap.put("Second_Terminal_Total", String.valueOf(Double.valueOf(datMap.get("Second_Terminal_Total"))+(obj[6] == null ? 0.0 : Double.valueOf(obj[6].toString()))));
			}
			if(examSequenceList.contains("Final Exam"))
			{
				datMap.put("Final Exam_"+obj[0], (obj[7] == null ? "" : obj[7].toString()));
				datMap.put("Final_Exam_Total", String.valueOf(Double.valueOf(datMap.get("Final_Exam_Total"))+(obj[7] == null ? 0.0 : Double.valueOf(obj[7].toString()))));
			}
			
			if(examSequenceList.contains("Final Exam"))
			{
				datMap.put("50% Of Final Exam_"+obj[0], (obj[8] == null ? "" : obj[8].toString()));
				datMap.put("50%_Of_Final_Exam_Total", String.valueOf(Double.valueOf(datMap.get("50%_Of_Final_Exam_Total"))+(obj[8] == null ? 0.0 : Double.valueOf(obj[8].toString()))));
			}
			
			double total_classwork=0.0;
			if(examSequenceList.contains("Final Exam"))
			{
				
				total_classwork=(obj[3] == null ? 0.0 : Double.valueOf(obj[3].toString()))+(obj[4] == null ? 0.0 : Double.valueOf(obj[4].toString()))+(obj[5] == null ? 0.0 : Double.valueOf(obj[5].toString()))+(obj[6] == null ? 0.0 : Double.valueOf(obj[6].toString()));
				datMap.put("Total of Classwork_"+obj[0],String.valueOf(total_classwork));
				datMap.put("Total_of_Classwork_Total", String.valueOf(Double.valueOf(datMap.get("Total_of_Classwork_Total"))+total_classwork));
				
				datMap.put("50% Of Total Classwork_"+obj[0],String.valueOf(total_classwork/2));
				datMap.put("50%_Of_Total_Classwork_Total", String.valueOf(Double.valueOf(datMap.get("50%_Of_Total_Classwork_Total"))+(total_classwork/2)));
			}
			if(obj[7] != null)
			{
				String grade="AB";
				double totalmarks=Double.valueOf(obj[8].toString())+(total_classwork/2); 
				if(totalmarks<40)
				{
					grade="D";
				}
				else
					if(totalmarks>=40 && totalmarks<50)
					{
						grade="C";
					}
					else if(totalmarks>=50 && totalmarks<60)
					{
						grade="C+";
					}
					else if(totalmarks>=60 && totalmarks<70)
					{
						grade="B";
					}
					else if(totalmarks>=70 && totalmarks<80)
					{
						grade="B+";
					}else if(totalmarks>=80 && totalmarks<90)
					{
						grade="A";
					}
					else if(totalmarks>=90 && totalmarks<=100)
					{
						grade="A+";
					}
				
				datMap.put("Grade_"+obj[0], grade);
			}
			else
			{
				datMap.put("Grade_"+obj[0], (obj[9] == null ? "" : (obj[9].toString().equalsIgnoreCase("0")||obj[9].toString().equalsIgnoreCase("NA") ? "AB" : obj[9].toString())));
			}
			
			if(examSequenceList.contains("Final Exam"))
			{
				double grand_total=(total_classwork/2)+(obj[8] == null ? 0.0 : Double.valueOf(obj[8].toString()));
				datMap.put("Total_"+obj[0],String.valueOf(grand_total));
				datMap.put("Total_Total", String.valueOf(Double.valueOf(datMap.get("Total_Total"))+grand_total));
			}
			
			total_subject_count++;
			List<Remarks> reamrksEntity= remarksService.findRemarksByStudentId(student.getAdmno());
			for(Remarks rem: reamrksEntity)
			{
				datMap.put("Remarks_"+rem.getLabel(),rem.getValue());
			}
			
		}
		
		System.out.println("DAAMAP: "+datMap);
		System.out.println("DAAMAP: "+examSequenceList);
		
		PdfPCell cell = new PdfPCell();
		cell.setBorder(Rectangle.BOTTOM);
		cell.setBorderColor(new BaseColor(44, 67, 144));
		cell.setBorderWidth(1f);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.setWidthPercentage(90f);
		
		//document.add(table);
		
		
		*//** Student Details *//*
		
		 Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLDITALIC);
		    Font boldfont1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
		    Font boldFont2 = new Font(Font.FontFamily.TIMES_ROMAN,  10, Font.BOLD);
		    Font boldFont3 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);

		    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase(" NAME: "+student.getStudent_name(), boldFont3), document.getPageSize().getLeft()+50, 1030, 0);
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, new Phrase(" BATCH: "+student.getBatch(), boldFont3), document.getPageSize().getRight()-50, 1030, 0);
			    
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("FATHER/GUARDIAN NAME: "+student.getImmediate_contact(), boldFont3), document.getPageSize().getLeft()+50, 1000, 0);
			    if(student.getPhone().equalsIgnoreCase("NA"))
			    {
			    		ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, new Phrase("PHONE NO: ", boldFont3), document.getPageSize().getRight()-120,  1000, 0);
			    }
			    else
			    {
			    	ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, new Phrase("PHONE NO: "+student.getPhone(), boldFont3), document.getPageSize().getRight()-55,  1000, 0);
			    }
			    
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("ADDRESS: "+student.getAddress(), boldFont3), document.getPageSize().getLeft()+50,  970, 0);
			  
			    

			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("DATE OF BIRTH:  "+(student.getDob()==null || student.getDob().equalsIgnoreCase("null") ? "" : student.getDob()), boldFont3), document.getPageSize().getLeft()+50, 940, 0);
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("GR NO: "+student.getAdmno(), boldFont3), document.getPageSize().getLeft()+250, 940, 0);
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("ROLL NO: "+(student.getRoll_no()==null || student.getRoll_no().equalsIgnoreCase("null") ? "" : student.getRoll_no()), boldFont3), document.getPageSize().getLeft()+450, 940, 0);
			    
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("AIO NO: "+(student.getAio_no()==null || student.getAio_no().equalsIgnoreCase("null") ? "" : student.getAio_no()), boldFont3), document.getPageSize().getRight()-180, 940, 0);
			    
			    
			  
		    
		    PdfPTable tableempty = new PdfPTable(1);
			cell = new PdfPCell(new Phrase(""));
			 cell.setBorderWidth(0f);
			cell.setFixedHeight(120f);
			tableempty.addCell(cell);
			tableempty.setWidthPercentage(100f);
			document.add(tableempty);
		    
		*//** Table 1 **//* 
		PdfPTable table1 = new PdfPTable(exam_list.size()+2);
		
		
		cell = new PdfPCell(new Phrase("Subject",boldFont2));
		cell.setColspan(2);
		cell.setPadding(5f);
		cell.setPaddingLeft(10f);
		table1.addCell(cell);
		table1.setWidthPercentage(100f);
		
		
		table1.completeRow();
		Paragraph paragraphTable2 = new Paragraph(); 
        paragraphTable2.setSpacingAfter(10f);
        paragraphTable2.add(table1);
        
        
        document.add(paragraphTable2);
		
        document.newPage();
 
			
	}		
			
	
	
	
	
	
	
	
	
	*/
	
	
	
}
