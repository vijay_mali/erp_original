package com.student.erp.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.student.erp.entities.Auto_Attendence;
import com.student.erp.entities.BatchSubject;
import com.student.erp.entities.Marks;
import com.student.erp.entities.Remarks;
import com.student.erp.entities.Student_Details;
import com.student.erp.entities.Temp_Attendance;
import com.student.erp.entities.Working_days;
import com.student.erp.service.HolidayService;
import com.student.erp.service.MarksService;
import com.student.erp.service.MaxMinService;
import com.student.erp.service.RemarksService;
import com.student.erp.service.StudentDetailsService;
import com.student.erp.service.WorkingDaysService;

public class PDFUtilities {

	
	
	Document document;
	ByteArrayOutputStream out;
	PdfWriter writer;
	
	
	
	public void openDoc(String name, String subaddress, String affiliation,String contact,String logoPath) throws DocumentException
	{
		document = new Document(PageSize.A3, 36, 36, 154, 54);
		out = new ByteArrayOutputStream();
		writer =PdfWriter.getInstance(document, out);
		PdfEvents event = new PdfEvents(name, subaddress, affiliation,contact,logoPath);
		writer.setPageEvent(event);
		document.open();
	}
	
	public void  createHeader(RemarksService remarksService,HolidayService holidayService,MarksService marksService,MaxMinService maxMinService,WorkingDaysService workingDaysService,StudentDetailsService studentDetailsService,Student_Details student,List<String> subject_list,List<String> subject_skils_list,List<String> subject_annual_list,List<String> exam_list,Map<String, Map<String, Map<String,String>>> subjectMap,String attendence,String exams,List<String> examSequenceList,List<Auto_Attendence> autoAttendanceList, String imagePath, String name, String subaddress, String affiliation,String contact, List<String> exam_list2, List<String> exam_list_without_max, HashMap<String, Double> examClassAverage) throws DocumentException, MalformedURLException, IOException, ParseException
	{
		
		
		String attendenceCount="0";
		String attendencePercentage="%";
		int totalAttendence=0;
		int totalPresentDay=0,total_student_count=0;
		NumberFormat nf= NumberFormat.getInstance();
        nf.setMaximumFractionDigits(0);
		
		
		List<String> table_header=new ArrayList<String>();
	//	table_header.add("Subject");
//		table_header.add("First Periodic");
//		table_header.add("First Terminal");
//		table_header.add("Second Periodic");
//		table_header.add("Second Terminal");
		
		table_header.addAll(exam_list_without_max);
		
		table_header.add("Total of Classwork");
	//	table_header.add("Final Exam");
		table_header.add("50% Of Total Classwork");
		table_header.add("50% Of Final Exam");
		table_header.add("Total");
		table_header.add("Grade");
		table_header.add("Remarks");
		
		
		
		List<String> additionalList=new ArrayList<String>();
		additionalList.add("Total");
		additionalList.add("Average");
		additionalList.add("Class Average");
		additionalList.add("No.of Working Days");
		additionalList.add("No.of Days Present");
		additionalList.add("No.of Pupils in Class");
		additionalList.add("Rank in class");
		
		List<String> table2Headers=new ArrayList<String>();
//		table2Headers.add("First Periodic");
//		table2Headers.add("First Terminal");
//		table2Headers.add("Second Periodic");
//		table2Headers.add("Second Terminal");
//		table2Headers.add("Final Exam");
		table2Headers.addAll(exam_list_without_max);
		
		double total_FirstPeriodic=0.0;
		int total_no_working_days=0,total_no_days_present=0,total_no_pupils=0,total_no_rank=0;
		
		
		Map<String, String> datMap=new HashMap<String, String>();
		

		datMap.put("Final_Exam_Total", "0.0");
		datMap.put("50%_Of_Final_Exam_Total", "0.0");
		datMap.put("Total_of_Classwork_Total", "0.0");
		datMap.put("50%_Of_Total_Classwork_Total", "0.0");
		datMap.put("Total_Total", "0.0");
		
		
		int working_days=0;
		long student_count=0;
		Working_days workingDays=workingDaysService.searchByBatch(student.getBatch());
		
		if(workingDays!= null)
		{
			working_days=workingDays.getWorking_days();
		}
		
		
		student_count=studentDetailsService.countByBatch(student.getBatch());
		System.out.println("studentid"+student.getStudent_id()+"s\batch"+student.getBatch());
		List<Map<String, Object>> fullSubjectList=marksService.getFullSubjectDetails(student.getAdmno(),student.getBatch(),"");
		System.out.println(">>>>>  fullSubjectList: "+fullSubjectList+"student.getBatch()"+student.getBatch());
		//System.out.println("subjectMap"+subjectMap);
		
		
		BatchSubject batchSubject=marksService.getSubjectByBatch(student.getBatch());
		System.out.println("batchSubject"+batchSubject);
		List<String> subjects = Arrays.asList(batchSubject.getSubject().split("\\s*,\\s*"));
		
		System.out.println("SUBJECTS : "+subjects);
		
		int total_subject_count=0;
		int finalPresent=0;
		
		
		//logic for rank show for a exam
				Map<String,Boolean> rankMap= new HashMap<>();
				for(String exam : exam_list_without_max)
				{
					rankMap.put(exam, true);	
				}
				
				rankMap.put("Total", true);
		
		if(autoAttendanceList.size()>0)
			{
				for(Auto_Attendence  auto_attendence : autoAttendanceList)
				{
					for(String exam   : exam_list_without_max)
					{
						if(auto_attendence.getExam().equalsIgnoreCase(exam))
						{
							String examWithUnderScore=exam.replaceAll("\\s+","_");
							datMap.put(examWithUnderScore+"_Start_Date", auto_attendence.getStart_date());
							datMap.put(examWithUnderScore+"_End_Date", auto_attendence.getEnd_date());
						}
						
					}
					
				}
			}
			System.out.println("attendance map"+datMap);
			
		for(String subject : subjects)
		{
			double total_classwork=0.0;
			boolean finalExamPresent = false;
			double finalExam50=0.0;
		for(String exam : exam_list_without_max)
		{
			
				
				Map<String, Object> thatSubjectFullInfo=fullSubjectList.stream().filter(obj -> obj.get("subject").equals(subject)).findAny().orElse(null);
				System.out.println("thatSubjectFullInfo"+thatSubjectFullInfo);
				if(thatSubjectFullInfo!=null)
				{
					String examWithoutSpace=exam.replaceAll("\\s+","");
					String examWithUnderScore=exam.replaceAll("\\s+","_");
					System.out.println("examWithoutSpace"+examWithoutSpace+"  examWithUnderScore"+ examWithUnderScore);
					if(datMap.get(examWithUnderScore+"_Total")==null)
					{
						datMap.put(examWithUnderScore+"_Total", "0.0");
					}
					datMap.put(exam+"_"+thatSubjectFullInfo.get("subject"), (thatSubjectFullInfo.get(examWithoutSpace) == null ? "" : nf.format(Double.valueOf(thatSubjectFullInfo.get(examWithoutSpace).toString()))));
					
					datMap.put(examWithUnderScore+"_Total", String.valueOf(Double.valueOf(datMap.get(examWithUnderScore+"_Total"))+(thatSubjectFullInfo.get(examWithoutSpace)  == null ? 0.0 : Double.valueOf(thatSubjectFullInfo.get(examWithoutSpace).toString()))));
					System.out.println("examWithoutSpace"+examWithoutSpace+"  examWithUnderScore"+ examWithUnderScore);

					datMap.put("50% Of Final Exam_"+thatSubjectFullInfo.get("subject"), (thatSubjectFullInfo.get("FinalExam50") == null ? "" : thatSubjectFullInfo.get("FinalExam50").toString()));
					datMap.put("50%_Of_Final_Exam_Total", String.valueOf(Double.valueOf(datMap.get("50%_Of_Final_Exam_Total"))+(thatSubjectFullInfo.get("FinalExam50") == null ? 0.0 : Double.valueOf(thatSubjectFullInfo.get("FinalExam50").toString()))));
//					
					System.out.println("examWithoutSpace"+examWithoutSpace+"  examWithUnderScore"+ examWithUnderScore);

					double marksInOneSubject=thatSubjectFullInfo.get(examWithoutSpace) == null ? 0.0 : Double.valueOf((String) thatSubjectFullInfo.get(examWithoutSpace));
					System.out.println("marksInOneSubject"+marksInOneSubject);
					if(thatSubjectFullInfo.get(examWithoutSpace)!=null)
					{
					total_classwork+=Double.valueOf(thatSubjectFullInfo.get(examWithoutSpace).toString());
					}
					if(examWithoutSpace.equals("FinalExam"))
					{
						finalExamPresent=true;
					}
					if(thatSubjectFullInfo.get("FinalExam50")!=null)
					{
						finalExam50=Double.valueOf(thatSubjectFullInfo.get("FinalExam50").toString());
					}
					
			
				}
				else
				{
					rankMap.put(exam, false);
//					showRank=false;
				}
		}
		
		datMap.put("Total of Classwork_"+subject,String.valueOf(total_classwork));
		datMap.put("Total_of_Classwork_Total", String.valueOf(Double.valueOf(datMap.get("Total_of_Classwork_Total"))+total_classwork));
		
		datMap.put("50% Of Total Classwork_"+subject,String.valueOf(total_classwork/2));
		datMap.put("50%_Of_Total_Classwork_Total", String.valueOf(Double.valueOf(datMap.get("50%_Of_Total_Classwork_Total"))+(total_classwork/2)));
		
		
		if(finalExamPresent)
		{
			String grade="NA";
			double totalmarks=Double.valueOf(finalExam50)+(total_classwork/2); 
			if(totalmarks<40)
			{
				grade="D";
			}
			else
				if(totalmarks>=40 && totalmarks<50)
				{
					grade="C";
				}
				else if(totalmarks>=50 && totalmarks<60)
				{
					grade="C+";
				}
				else if(totalmarks>=60 && totalmarks<70)
				{
					grade="B";
				}
				else if(totalmarks>=70 && totalmarks<80)
				{
					grade="B+";
				}else if(totalmarks>=80 && totalmarks<90)
				{
					grade="A";
				}
				else if(totalmarks>=90 && totalmarks<=100)
				{
					grade="A+";
				}
				else if(totalmarks==0.0)
				{
					datMap.put("Grade_"+subject, "AB");
	
				}
			datMap.put("Grade_"+subject, grade);
		}
		else
		{
			datMap.put("Grade_"+subject, "AB");

			//datMap.put("Grade_"+subject, (obj.get("grade") == null ? "" : (obj.get("grade").toString().equalsIgnoreCase("0")||obj.get("grade").toString().equalsIgnoreCase("NA") ? "AB" : obj.get("grade").toString())));
		}
		
		
		double grand_total=(total_classwork/2)+finalExam50;
		datMap.put("Total_"+subject,String.valueOf(grand_total));
		datMap.put("Total_Total", String.valueOf(Double.valueOf(datMap.get("Total_Total"))+grand_total));
		if(grand_total<33)
		{
			rankMap.put("Total", false);
		}
		total_subject_count++;
		
		
		List<Remarks> reamrksEntity= remarksService.findRemarksByStudentId(student.getAdmno());
		for(Remarks rem: reamrksEntity)
		{
			System.out.println(rem.getLabel()+"--"+rem.getValue());
			datMap.put("Remarks_"+rem.getLabel(),rem.getValue());
		}
		
			
		}
		
		
//		if(examSequenceList.contains("First Periodic") || examSequenceList.contains("Final Exam"))
//		{
//			datMap.put("First_Periodic_Total", "0.0");
//			if(!attendence.equalsIgnoreCase("manualattendance"))
//			{
//				String startDate=null, endDate=null;
//				if(autoAttendanceList.size()>0)
//				{
//					for(Auto_Attendence  auto_attendence : autoAttendanceList)
//					{
//						if(auto_attendence.getExam().equalsIgnoreCase("First Periodic"))
//						{
//							datMap.put("First_Periodic_Start_Date", auto_attendence.getStart_date());
//							datMap.put("First_Periodic_End_Date", auto_attendence.getEnd_date());
//						}
//					}
//				}
//			}
//		}
//		if(examSequenceList.contains("Second Periodic") || examSequenceList.contains("Final Exam"))
//		{
//			datMap.put("Second_Periodic_Total", "0.0");
//			if(!attendence.equalsIgnoreCase("manualattendance"))
//			{
//				String startDate=null, endDate=null;
//				if(autoAttendanceList.size()>0)
//				{
//					for(Auto_Attendence  auto_attendence : autoAttendanceList)
//					{
//						if(auto_attendence.getExam().equalsIgnoreCase("Second Periodic"))
//						{
//							datMap.put("Second_Periodic_Start_Date", auto_attendence.getStart_date());
//							datMap.put("Second_Periodic_End_Date", auto_attendence.getEnd_date());
//						}
//					}
//				}
//			}
//		}
//		if(examSequenceList.contains("First Terminal") || examSequenceList.contains("Final Exam"))
//		{
//			datMap.put("First_Terminal_Total", "0.0");
//			if(!attendence.equalsIgnoreCase("manualattendance"))
//			{
//				String startDate=null, endDate=null;
//				if(autoAttendanceList.size()>0)
//				{
//					for(Auto_Attendence  auto_attendence : autoAttendanceList)
//					{
//						if(auto_attendence.getExam().equalsIgnoreCase("First Terminal"))
//						{
//							datMap.put("First_Terminal_Start_Date", auto_attendence.getStart_date());
//							datMap.put("First_Terminal_End_Date", auto_attendence.getEnd_date());
//						}
//					}
//				}
//			}
//		}
//		if(examSequenceList.contains("Second Terminal") || examSequenceList.contains("Final Exam"))
//		{
//			datMap.put("Second_Terminal_Total", "0.0");
//			if(!attendence.equalsIgnoreCase("manualattendance"))
//			{
//				String startDate=null, endDate=null;
//				if(autoAttendanceList.size()>0)
//				{
//					for(Auto_Attendence  auto_attendence : autoAttendanceList)
//					{
//						if(auto_attendence.getExam().equalsIgnoreCase("Second Terminal"))
//						{
//							datMap.put("Second_Terminal_Start_Date", auto_attendence.getStart_date());
//							datMap.put("Second_Terminal_End_Date", auto_attendence.getEnd_date());
//						}
//					}
//				}
//			}
//		}
//		
//		if( examSequenceList.contains("Final Exam"))
//		{
//			datMap.put("Final_Exam_Total", "0.0");
//		
//			datMap.put("50%_Of_Final_Exam_Total", "0.0");
//			datMap.put("Total_of_Classwork_Total", "0.0");
//			datMap.put("50%_Of_Total_Classwork_Total", "0.0");
//			datMap.put("Total_Total", "0.0");
//			
//			if(!attendence.equalsIgnoreCase("manualattendance"))
//			{
//				String startDate=null, endDate=null;
//				if(autoAttendanceList.size()>0)
//				{
//					for(Auto_Attendence  auto_attendence : autoAttendanceList)
//					{
//						if(auto_attendence.getExam().equalsIgnoreCase("Final Exam"))
//						{
//							datMap.put("Final_Exam_Start_Date", auto_attendence.getStart_date());
//							datMap.put("Final_Exam_End_Date", auto_attendence.getEnd_date());
//						}
//					}
//				}
//			}
//		}
//		
		
		
		
//		
//		int working_days=0;
//		long student_count=0;
//		Working_days workingDays=workingDaysService.searchByBatch(student.getBatch());
//		
//		if(workingDays!= null)
//		{
//			working_days=workingDays.getWorking_days();
//		}
//		
		
//		
//		student_count=studentDetailsService.countByBatch(student.getBatch());
//		List<Map<String, Object>> fullSubjectList=marksService.getFullSubjectDetails(student.getAdmno(),student.getBatch(),"");
//		System.out.println(">>>>> HITTING: "+student.getBatch());
//		BatchSubject batchSubject=marksService.getSubjectByBatch(student.getBatch());
//		System.out.println("batchSubject"+batchSubject);
//		
//		List<String> subjects = Arrays.asList(batchSubject.getSubject().split("\\s*,\\s*"));
//		
//		NumberFormat nf= NumberFormat.getInstance();
//        nf.setMaximumFractionDigits(0);
//        
//		int total_subject_count=1;
//		for(Map<String, Object> obj : fullSubjectList)
//		{
//			if(examSequenceList.contains("First Periodic") || examSequenceList.contains("Final Exam"))
//			{
//				datMap.put("First Periodic_"+obj.get("subject"), (obj.get("FirstPeriodic") == null ? "" : nf.format(Double.valueOf(obj.get("FirstPeriodic").toString()))));
//				datMap.put("First_Periodic_Total", String.valueOf(Double.valueOf(datMap.get("First_Periodic_Total"))+(obj.get("FirstPeriodic") == null ? 0.0 : Double.valueOf(obj.get("FirstPeriodic").toString()))));
//			}
//			
//			if(examSequenceList.contains("Second Periodic") || examSequenceList.contains("Final Exam"))
//			{
//				datMap.put("Second Periodic_"+obj.get("subject"), (obj.get("SecondPeriodic") == null ? "" : nf.format(Double.valueOf(obj.get("SecondPeriodic").toString()))));
//				datMap.put("Second_Periodic_Total", String.valueOf(Double.valueOf(datMap.get("Second_Periodic_Total"))+(obj.get("SecondPeriodic")== null ? 0.0 : Double.valueOf(obj.get("SecondPeriodic").toString()))));
//			}
//			
//			if(examSequenceList.contains("First Terminal") || examSequenceList.contains("Final Exam"))
//			{
//				datMap.put("First Terminal_"+obj.get("subject"), (obj.get("FirstTerminal") == null ? "" : nf.format(Double.valueOf(obj.get("FirstTerminal").toString()))));
//			
//				datMap.put("First_Terminal_Total", String.valueOf(Double.valueOf(datMap.get("First_Terminal_Total"))+(obj.get("FirstTerminal") == null ? 0.0 : Double.valueOf(obj.get("FirstTerminal").toString()))));
//			}
//			if(examSequenceList.contains("Second Terminal") || examSequenceList.contains("Final Exam"))
//			{
//				datMap.put("Second Terminal_"+obj.get("subject"), (obj.get("SecondTerminal")  == null ? "" : nf.format(Double.valueOf(obj.get("SecondTerminal").toString()))));
//				datMap.put("Second_Terminal_Total", String.valueOf(Double.valueOf(datMap.get("Second_Terminal_Total"))+(obj.get("SecondTerminal") == null ? 0.0 : Double.valueOf(obj.get("SecondTerminal").toString()))));
//			}
//			if(examSequenceList.contains("Final Exam"))
//			{
//				datMap.put("Final Exam_"+obj.get("subject"), (obj.get("FinalExam") == null ? "" : obj.get("FinalExam").toString()));
//				datMap.put("Final_Exam_Total", String.valueOf(Double.valueOf(datMap.get("Final_Exam_Total"))+(obj.get("FinalExam") == null ? 0.0 : Double.valueOf(obj.get("FinalExam").toString()))));
//			}
//			
//			if(examSequenceList.contains("Final Exam"))
//			{
//				datMap.put("50% Of Final Exam_"+obj.get("subject"), (obj.get("FinalExam50") == null ? "" :obj.get("FinalExam50").toString()));
//				datMap.put("50%_Of_Final_Exam_Total", String.valueOf(Double.valueOf(datMap.get("50%_Of_Final_Exam_Total"))+(obj.get("FinalExam50") == null ? 0.0 : Double.valueOf(obj.get("FinalExam50").toString()))));
//			}
//			
//			double total_classwork=0.0;
//			if(examSequenceList.contains("Final Exam"))
//			{
//				
//				total_classwork=(obj.get("FirstPeriodic") == null ? 0.0 : Double.valueOf(obj.get("FirstPeriodic").toString()))+(obj.get("SecondPeriodic") == null ? 0.0 : Double.valueOf(obj.get("SecondPeriodic").toString()))+(obj.get("FirstTerminal") == null ? 0.0 : Double.valueOf(obj.get("FirstTerminal").toString()))+(obj.get("SecondTerminal") == null ? 0.0 : Double.valueOf(obj.get("SecondTerminal").toString()));
//				datMap.put("Total of Classwork_"+obj.get("subject"),String.valueOf(total_classwork));
//				datMap.put("Total_of_Classwork_Total", String.valueOf(Double.valueOf(datMap.get("Total_of_Classwork_Total"))+total_classwork));
//				
//				datMap.put("50% Of Total Classwork_"+obj.get("subject"),String.valueOf(total_classwork/2));
//				datMap.put("50%_Of_Total_Classwork_Total", String.valueOf(Double.valueOf(datMap.get("50%_Of_Total_Classwork_Total"))+(total_classwork/2)));
//			}
//			if(obj.get("FinalExam") != null)
//			{
//				String grade="AB";
//				double totalmarks=Double.valueOf(obj.get("FinalExam50").toString())+(total_classwork/2); 
//				if(totalmarks<40)
//				{
//					grade="D";
//				}
//				else
//					if(totalmarks>=40 && totalmarks<50)
//					{
//						grade="C";
//					}
//					else if(totalmarks>=50 && totalmarks<60)
//					{
//						grade="C+";
//					}
//					else if(totalmarks>=60 && totalmarks<70)
//					{
//						grade="B";
//					}
//					else if(totalmarks>=70 && totalmarks<80)
//					{
//						grade="B+";
//					}else if(totalmarks>=80 && totalmarks<90)
//					{
//						grade="A";
//					}
//					else if(totalmarks>=90 && totalmarks<=100)
//					{
//						grade="A+";
//					}
//				
//				datMap.put("Grade_"+obj.get("subject"), grade);
//			}
//			else
//			{
//				datMap.put("Grade_"+obj.get("subject"), (obj.get("grade") == null ? "" : (obj.get("grade").toString().equalsIgnoreCase("0")||obj.get("grade").toString().equalsIgnoreCase("NA") ? "AB" : obj.get("grade").toString())));
//			}
//			
//			if(examSequenceList.contains("Final Exam"))
//			{
//				double grand_total=(total_classwork/2)+(obj.get("FinalExam50") == null ? 0.0 : Double.valueOf(obj.get("FinalExam50") .toString()));
//				datMap.put("Total_"+obj.get("subject"),String.valueOf(grand_total));
//				datMap.put("Total_Total", String.valueOf(Double.valueOf(datMap.get("Total_Total"))+grand_total));
//			}
//			
//			total_subject_count++;
//			List<Remarks> reamrksEntity= remarksService.findRemarksByStudentId(student.getAdmno());
//			for(Remarks rem: reamrksEntity)
//			{
//				datMap.put("Remarks_"+rem.getLabel(),rem.getValue());
//			}
//			
//		}
//		
		System.out.println("DAAMAP11: "+datMap);
		System.out.println("DAAMAP12: "+examSequenceList);
		
		PdfPCell cell = new PdfPCell();
		cell.setBorder(Rectangle.BOTTOM);
		cell.setBorderColor(new BaseColor(44, 67, 144));
		cell.setBorderWidth(1f);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.setWidthPercentage(90f);
		
		//document.add(table);
		
		
		/** Student Details */
		
		 Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLDITALIC);
		    Font boldfont1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
		    Font boldFont2 = new Font(Font.FontFamily.TIMES_ROMAN,  10, Font.BOLD);
		    Font boldFont3 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);

		    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase(" NAME: "+student.getStudent_name(), boldFont3), document.getPageSize().getLeft()+50, 1030, 0);
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, new Phrase(" BATCH: "+student.getBatch(), boldFont3), document.getPageSize().getRight()-50, 1030, 0);
			    
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("FATHER/GUARDIAN NAME: "+student.getImmediate_contact(), boldFont3), document.getPageSize().getLeft()+50, 1000, 0);
			   System.out.println("phone is"+student.getPhone());
			    if(student.getPhone().equalsIgnoreCase("NA"))
			    {
			    		ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, new Phrase("PHONE NO: ", boldFont3), document.getPageSize().getRight()-120,  1000, 0);
			    }
			    else
			    {
			    	ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, new Phrase("PHONE NO: "+student.getPhone(), boldFont3), document.getPageSize().getRight()-55,  1000, 0);
			    }
			    
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("ADDRESS: "+student.getAddress(), boldFont3), document.getPageSize().getLeft()+50,  970, 0);
			  
			    

			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("DATE OF BIRTH:  "+(student.getDob()==null || student.getDob().equalsIgnoreCase("null") ? "" : student.getDob()), boldFont3), document.getPageSize().getLeft()+50, 940, 0);
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("GR NO: "+student.getAdmno(), boldFont3), document.getPageSize().getLeft()+250, 940, 0);
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("ROLL NO: "+(student.getRoll_no()==null || student.getRoll_no().equalsIgnoreCase("null") ? "" : student.getRoll_no()), boldFont3), document.getPageSize().getLeft()+450, 940, 0);
			    
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("AIO NO: "+(student.getAio_no()==null || student.getAio_no().equalsIgnoreCase("null") ? "" : student.getAio_no()), boldFont3), document.getPageSize().getRight()-180, 940, 0);
			    
			    
			  
		    
		    PdfPTable tableempty = new PdfPTable(1);
			cell = new PdfPCell(new Phrase(""));
			 cell.setBorderWidth(0f);
			cell.setFixedHeight(120f);
			tableempty.addCell(cell);
			tableempty.setWidthPercentage(100f);
			document.add(tableempty);
		    
		/** Table 1 **/ 
		PdfPTable table1 = new PdfPTable(exam_list2.size()+2);
		
		
		cell = new PdfPCell(new Phrase("Subject",boldFont2));
		cell.setColspan(2);
		cell.setPadding(5f);
		cell.setPaddingLeft(10f);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setPaddingLeft(21f);

		table1.addCell(cell);
		table1.setWidthPercentage(100f);
		
		for(String headers : table_header)
		{
			System.out.println("header s"+headers);
			String max=maxMinService.getMaxMarksByExam(headers, student.getBatch());
			if(headers.equalsIgnoreCase("First Periodic")|| headers.equalsIgnoreCase("Second Periodic") || headers.equalsIgnoreCase("First Terminal")|| headers.equalsIgnoreCase("Second Terminal"))
			{
				headers=headers.replaceAll(" ",System.lineSeparator());
			}
			cell = new PdfPCell(new Phrase(headers+(max == null || max.equalsIgnoreCase("NULL")? "" : System.lineSeparator()+" "+nf.format(Double.valueOf(max))),boldFont2));
			//cell.setNoWrap(true);
			//cell.setPaddingLeft(10f);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table1.addCell(cell);
		}
		
		
		
		for(String obj : subjects)
		{
			cell = new PdfPCell(new Phrase(obj,boldfont1));
			cell.setColspan(2);
			cell.setPadding(10f);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			//cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setPaddingLeft(21f);

			table1.addCell(cell);
			for(String headers : table_header)
			{
				if(headers.equals("50% of Total Classwork"))
				{
					System.out.println(headers+"_"+obj.toString());
					System.out.println(datMap.containsKey(headers+"_"+obj.toString()) +"" +datMap.get(headers+"_"+obj.toString()));
					cell = new PdfPCell(new Phrase((datMap.containsKey(" "+headers+"_"+obj.toString()) ? datMap.get(headers+"_"+obj.toString())	: ""),boldfont1));

				}
				else
				{

				cell = new PdfPCell(new Phrase((datMap.containsKey(headers+"_"+obj.toString()) ? datMap.get(headers+"_"+obj.toString())	: ""),boldfont1));
				}
				//cell.setNoWrap(true);
				//cell.setPaddingLeft(15f);
				
				if(headers.equals("Grade"))
				{
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

				cell.setPaddingLeft(23f);	
				}
				else
				{
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);	
				}
			
				table1.addCell(cell);
			}
			
		}
		
		for(String addColumn : additionalList)
		{
			cell = new PdfPCell(new Phrase(addColumn,boldfont1));
			cell.setColspan(2);
			cell.setPadding(10f);
			
			cell.setPaddingLeft(21f);
			table1.addCell(cell);
			if(addColumn.equalsIgnoreCase("Total")){
				for(String headers : table_header)
				{
					//if(headers.equalsIgnoreCase("First Periodic")|| headers.equalsIgnoreCase("Final Exam"))
					//{
					if(exam_list_without_max.contains(headers) || headers.equals("Total"))
					{
						if(!headers.equals("Final Exam"))
						{
						cell = new PdfPCell(new Phrase((datMap.containsKey(headers.replaceAll(" ", "_")+"_Total") ? datMap.get(headers.replaceAll(" ", "_")+"_Total") : ""),boldfont1));
						//cell.setNoWrap(true);
//						cell.setPaddingLeft(15f);
						cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);						table1.addCell(cell);
						}
						else
						{
							cell = new PdfPCell(new Phrase("",boldfont1));
							table1.addCell(cell);
						}
						}
					else
					{
						cell = new PdfPCell(new Phrase("",boldfont1));
						cell.setPaddingLeft(15f);
						table1.addCell(cell);
					}
						/*}
					else
					{
						
							cell = new PdfPCell(new Phrase("",boldfont1));
							//cell.setNoWrap(true);
							cell.setPaddingLeft(15f);
							table1.addCell(cell);
						
					}*/
				}
			}
			else if(addColumn.equalsIgnoreCase("Average")){
				System.out.println(">>. HEADERS DONE - AVG");
				DecimalFormat df2 = new DecimalFormat(".##");
				for(String headers : table_header)
				{
//					if(headers.equalsIgnoreCase("First Periodic")|| headers.equalsIgnoreCase("Second Periodic") || headers.equalsIgnoreCase("First Terminal")|| headers.equalsIgnoreCase("Second Terminal") || headers.equalsIgnoreCase("Total"))
//					{
					if(exam_list_without_max.contains(headers) || headers.equals("Total"))
					{
						if(!headers.equals("Final Exam"))
						{
							cell = new PdfPCell(new Phrase((datMap.containsKey(headers.replaceAll(" ", "_")+"_Total") ? String.valueOf(df2.format(Double.valueOf(datMap.get(headers.replaceAll(" ", "_")+"_Total"))/(total_subject_count-1))) : ""),boldfont1));
							//	cell.setPaddingLeft(15f);
								cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
								cell.setHorizontalAlignment(Element.ALIGN_CENTER);
								table1.addCell(cell);		
						}
						else
						{
							cell = new PdfPCell(new Phrase("",boldfont1));
							table1.addCell(cell);
						}
					
					}
//					}
					else
					{
						cell = new PdfPCell(new Phrase("",boldfont1));
						cell.setPaddingLeft(15f);
						table1.addCell(cell);
					}
				}
			}
			else if(addColumn.equalsIgnoreCase("Class Average")){
				System.out.println(">>. HEADERS DONE- CLASS AVG");
				DecimalFormat df2 = new DecimalFormat(".##");
				for(String headers : table_header)
				{
//					if(headers.equalsIgnoreCase("First Periodic")|| headers.equalsIgnoreCase("Second Periodic") || headers.equalsIgnoreCase("First Terminal")|| headers.equalsIgnoreCase("Second Terminal") || headers.equalsIgnoreCase("Total"))
//					{
					if(exam_list_without_max.contains(headers) || headers.equals("Total"))
					{
						if(!headers.equals("Final Exam"))
						{
							double average=0;
							if(examClassAverage!=null)
							{
								 average=examClassAverage.get(headers);

							}
							
							cell = new PdfPCell(new Phrase(""+average,boldfont1));
					System.out.println("total_student"+student_count);
						//cell = new PdfPCell(new Phrase((datMap.containsKey(headers.replaceAll(" ", "_")+"_Total") ? String.valueOf(df2.format(Double.valueOf(datMap.get(headers.replaceAll(" ", "_")+"_Total"))/(student_count-1))) : ""),boldfont1));
					//	cell.setPaddingLeft(15f);
						cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						table1.addCell(cell);
						}
						else
						{
							cell = new PdfPCell(new Phrase("",boldfont1));
							table1.addCell(cell);
						}
					}
//					}
					else
					{
						cell = new PdfPCell(new Phrase("",boldfont1));
						table1.addCell(cell);
					}
				}
			}
			else if(addColumn.equalsIgnoreCase("No.of Working Days")){
				for(String headers : table_header)
				{
					
					if(exam_list_without_max.contains(headers) || headers.equals("Total"))
					{
						if(!headers.equals("Final Exam"))
						{
						if(attendence.equalsIgnoreCase("manualattendance"))
						{
							List<Temp_Attendance> temp_Attendance1= studentDetailsService.getManualAttendanceWokringDays(student.getBatch(),headers,student.getAdmno());
						
							int i=0;
							for(Temp_Attendance tmp : temp_Attendance1)
							{
								if(i==0)
								{
								
									working_days=Integer.parseInt(tmp.getWorking_days());
								}
								i++;
							}
						}
						else
						{
							
							if(datMap.containsKey(headers.replaceAll(" ", "_")+"_Start_Date") && datMap.get(headers.replaceAll(" ", "_")+"_Start_Date") != null && datMap.containsKey(headers.replaceAll(" ", "_")+"_End_Date") && datMap.get(headers.replaceAll(" ", "_")+"_End_Date") != null)
							{
								String startDate=datMap.get(headers.replaceAll(" ", "_")+"_Start_Date");
								String endDate=datMap.get(headers.replaceAll(" ", "_")+"_End_Date");
								System.out.println("start date"+startDate);
								System.out.println("end date "+endDate);
								SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
								Date startdate= sdf.parse(startDate);
								Date enddate= sdf.parse(endDate);
								
								working_days=(int)( (enddate.getTime() - startdate.getTime()) / (1000 * 60 * 60 * 24));
								long getHolidayCount=holidayService.getCountByDate(startDate, endDate);
								attendenceCount=studentDetailsService.getAttendence(student.getAdmno(), student.getSession());
								
								attendenceCount=String.valueOf(working_days-Long.valueOf(attendenceCount)-getHolidayCount);
								working_days=Integer.parseInt(String.valueOf(working_days-getHolidayCount));
							}
						}
						
						
						if(headers.equalsIgnoreCase("Total"))
						{
							cell = new PdfPCell(new Phrase(""+totalAttendence,boldfont1));
						}
						else
						{
							totalAttendence=totalAttendence+working_days;
							cell = new PdfPCell(new Phrase(""+working_days,boldfont1));
						}
						//cell.setPaddingLeft(15f);
						cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						table1.addCell(cell);
						
					  }
						else
						{
							cell = new PdfPCell(new Phrase("",boldfont1));
							cell.setPaddingLeft(15f);
							table1.addCell(cell);
						}
					}
					else
					{
						cell = new PdfPCell(new Phrase("",boldfont1));
						cell.setPaddingLeft(15f);
						table1.addCell(cell);
					}
				}
			}
			else if(addColumn.equalsIgnoreCase("No.of Days Present")){
				for(String headers : table_header)
				{
					
					
					if(exam_list_without_max.contains(headers) || headers.equals("Total"))
					{
						if(!headers.equals("Final Exam"))
						{
						if(attendence.equalsIgnoreCase("manualattendance"))
						{
							List<Temp_Attendance> temp_Attendance1= studentDetailsService.getManualAttendanceWokringDays(student.getBatch(),headers,student.getAdmno());
						
							
							int i=0;
							for(Temp_Attendance tmp : temp_Attendance1)
							{
								if(i==0)
								{
								
									attendenceCount=tmp.getTot_present();
								}
								i++;
							}
						}
						else
						{
							if(datMap.containsKey(headers.replaceAll(" ", "_")+"_Start_Date") && datMap.get(headers.replaceAll(" ", "_")+"_Start_Date") != null && datMap.containsKey(headers.replaceAll(" ", "_")+"_End_Date") && datMap.get(headers.replaceAll(" ", "_")+"_End_Date") != null)
							{
								String startDate=datMap.get(headers.replaceAll(" ", "_")+"_Start_Date");
								String endDate=datMap.get(headers.replaceAll(" ", "_")+"_End_Date");
								SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
								Date startdate= sdf.parse(startDate);
								Date enddate= sdf.parse(endDate);
								
								working_days=(int)( (enddate.getTime() - startdate.getTime()) / (1000 * 60 * 60 * 24));
								long getHolidayCount=holidayService.getCountByDate(startDate, endDate);
								attendenceCount=studentDetailsService.getAttendence(student.getAdmno(), student.getSession());
								
								attendenceCount=String.valueOf(working_days-Long.valueOf(attendenceCount)-getHolidayCount);
								working_days=Integer.parseInt(String.valueOf(working_days-getHolidayCount));
							}
						}
						
						if(headers.equalsIgnoreCase("Total"))
						{
							cell = new PdfPCell(new Phrase(""+totalPresentDay,boldfont1));
						}
						else
						{
							totalPresentDay=totalPresentDay+Integer.parseInt(attendenceCount);
							cell = new PdfPCell(new Phrase(""+attendenceCount,boldfont1));
						}
						
						//cell.setPaddingLeft(15f);
						cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						table1.addCell(cell);
					  }
					  else
						{
						  
							cell = new PdfPCell(new Phrase("",boldfont1));
							cell.setPaddingLeft(15f);
							table1.addCell(cell);
						}
					}
					else
					{
						
						cell = new PdfPCell(new Phrase("",boldfont1));
						cell.setPaddingLeft(15f);
						table1.addCell(cell);
					}
					
				}
			}
			else if(addColumn.equalsIgnoreCase("No.of Pupils in Class")){
				for(String headers : table_header)
				{
					if(exam_list_without_max.contains(headers) || headers.equals("Total"))
					{
						if(!headers.equals("Final Exam"))
						{
							if(headers.equalsIgnoreCase("Total"))
							{
								cell = new PdfPCell(new Phrase(""+total_student_count,boldfont1));
							}
							else
							{
								total_student_count=total_student_count+Integer.parseInt(""+student_count);
								cell = new PdfPCell(new Phrase(""+student_count,boldfont1));
							}
							//cell.setPaddingLeft(15f);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							table1.addCell(cell);
							
						}
						else
						{
							cell = new PdfPCell(new Phrase("",boldfont1));
							cell.setPaddingLeft(15f);
							table1.addCell(cell);
						}
					}
					else
					{
						cell = new PdfPCell(new Phrase("",boldfont1));
						cell.setPaddingLeft(15f);
						table1.addCell(cell);
					}
				}
			}
			else if(addColumn.equalsIgnoreCase("Rank in class")){
				System.out.println(">>. HEADERS DONE - RANK"+rankMap);
				for(String headers : table_header)
				{ 
//					if(headers.equalsIgnoreCase("First Periodic")|| headers.equalsIgnoreCase("Second Periodic") || headers.equalsIgnoreCase("First Terminal")|| headers.equalsIgnoreCase("Second Terminal") || headers.equalsIgnoreCase("Total"))
//					{
					if((exam_list_without_max.contains(headers) || headers.equals("Total")) && !headers.equals("Final Exam"))
					{
					if((rankMap.containsKey(headers) && rankMap.get(headers) == true)) {
						int rank=1, rankfound=0;
						List<Map<String, Object>> rankClass= new ArrayList<>();
						if(headers.equals("Total"))
						{
							rankClass=marksService.getStudentRankInClassForFullExam(student.getBatch(),exam_list_without_max);
						}
						else
						{
							 rankClass=marksService.getStudentRankInClass(student.getBatch(),headers);

						}
						System.out.println("RANK CLASS"+rankClass);
						for(Map<String, Object> obj : rankClass)
						{
							System.out.println(obj.get("admno").toString()+"--"+obj.get("admno").toString()+"--"+student.getAdmno());
							if(rankfound==0)
							{
								if(obj.get("admno").toString().equalsIgnoreCase(student.getAdmno()))
								{
									
									rankfound=rank;
									
								}
								else
								{
									rank++;
								}
							}
						}
						if(rankfound==0)
						{
							cell = new PdfPCell(new Phrase("",boldfont1));
						}
						else
						{
							cell = new PdfPCell(new Phrase(""+rankfound,boldfont1));
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);

						}
					}
					else
					{
						cell = new PdfPCell(new Phrase("-",boldfont1));
						cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);

					}
					}
					else
					{
						cell = new PdfPCell(new Phrase("",boldfont1));
						cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);

					}
						//cell.setPaddingLeft(15f);
						
					
						table1.addCell(cell);
//					}
//					else
//					{
//						cell = new PdfPCell(new Phrase("",boldFont1));
//						table1.addCell(cell);
//					}
				}
			}
			else { // here add for rest of the things
				for(String headers : table_header)
				{
					
						cell = new PdfPCell(new Phrase("",boldfont1));
						table1.addCell(cell);
				}
			}
		}
		
		
		/*for(String subjects : subject_list)
		{
			
			cell = new PdfPCell(new Phrase(subjects,boldfont1));
			cell.setColspan(2);
			cell.setPadding(10f);
			table1.addCell(cell);
			for(int i=0;i<exam_list.size(); i++)
			{
				
			}
			
		}*/
	//	System.out.println(student.getStudent_id()+"row_column_map"+row_column_map);
		
		/*for(String addColumn : additionalList)
		{
			cell = new PdfPCell(new Phrase(addColumn,boldfont1));
			cell.setColspan(2);
			cell.setPadding(10f);
			table1.addCell(cell);
			for(int i=0;i<exam_list.size(); i++)
			{
				cell = new PdfPCell(new Phrase("",boldFont));
				
				table1.addCell(cell);
			}
		}*/
		table1.completeRow();
		//table1.writeSelectedRows(0,-1, 30, 650, writer.getDirectContent());
		
		Paragraph paragraphTable2 = new Paragraph(); 
        paragraphTable2.setSpacingAfter(10f);
        paragraphTable2.add(table1);
        paragraphTable2.setAlignment(Element.ALIGN_MIDDLE);

		
        // ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, paragraphTable2,30, 590, 0);
        
        document.add(paragraphTable2);
		
        document.newPage();
        
//	}
		    
//		    System.out.println("SKILLS: "+subject_skils_list);
//		    System.out.println("ANNUAL: "+subject_annual_list);
		   
			
				
			
			//PdfPTable table3 = new PdfPTable(6);
		PdfPTable table3 = new PdfPTable(table2Headers.size()+1);

			
			cell = new PdfPCell(new Phrase("Subject",boldFont2));
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			//	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setPaddingLeft(35f);
				cell.setFixedHeight(30f);

			//cell.setColspan(1);
			//cell.setPadding(2f);
			table3.addCell(cell);
			table3.setWidthPercentage(100f);
			for(String addColumn : table2Headers)
			{
				cell = new PdfPCell(new Phrase(addColumn,boldFont2));
				cell.setPaddingLeft(25f);
				cell.setFixedHeight(30f);

				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table3.addCell(cell);
			}
			for(String skills : subject_annual_list)
			{
				cell = new PdfPCell(new Phrase(skills,boldfont1));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(30f);

//				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setPaddingLeft(35f);
				
				table3.addCell(cell);
				for(int i=0;i<table2Headers.size(); i++)
				{

					if(examSequenceList.contains(table2Headers.get(i)) || examSequenceList.contains("Final Exam")){
							List<Marks> skills_marks=marksService.findSkillsSubjectMark(student.getBatch(),student.getAdmno(),table2Headers.get(i)+" - G2",skills);
							
							if(skills_marks != null && skills_marks.size()>0)
								{
//									for(Marks marks :  skills_marks)
//									{
												cell = new PdfPCell(new Phrase(skills_marks.get(0).getGrade(),boldFont));
												//cell.setPaddingLeft(50f);
												cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
												//cell.setHorizontalAlignment(Element.ALIGN_CENTER);
												cell.setPaddingLeft(69f);
												
												table3.addCell(cell);
								//	}
								}
							else
							{
								
								cell = new PdfPCell(new Phrase("",boldFont));
								
								table3.addCell(cell);
								
							}	
							
					}
					else
					{
						cell = new PdfPCell(new Phrase("",boldFont));
						table3.addCell(cell);
					}
							
							
						}
														
					
			}
			
			
			document.add(table3);
			
			

			
			tableempty = new PdfPTable(1);
			cell = new PdfPCell(new Phrase(""));
			 cell.setBorderWidth(0f);
			cell.setFixedHeight(40f);
			tableempty.addCell(cell);
			tableempty.setWidthPercentage(100f);
			document.add(tableempty);
			
			
			 PdfPTable table2 = new PdfPTable(table2Headers.size()+1);
				
				
			    
				cell = new PdfPCell(new Phrase("Subject",boldFont2));
				//cell.setColspan(1);
				cell.setPaddingLeft(35f);
				cell.setFixedHeight(30f);

				table2.addCell(cell);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

				table2.setWidthPercentage(100f);
				for(String addColumn : table2Headers)
				{
					cell = new PdfPCell(new Phrase(addColumn,boldFont2));
					cell.setFixedHeight(30f);

					cell.setPaddingLeft(25f);
					table2.addCell(cell);
				}
				
		//		System.out.println(table2Headers+"SKILLS:"+subject_skils_list);
				for(String skills : subject_skils_list)
				{
					cell = new PdfPCell(new Phrase(skills,boldfont1));
					cell.setPaddingLeft(35f);
					cell.setFixedHeight(30f);

					table2.addCell(cell);
					
					for(int i=0;i<table2Headers.size(); i++)
					{
						if(examSequenceList.contains(table2Headers.get(i)) || examSequenceList.contains("Final Exam")){
							List<Marks> skills_marks=marksService.findSkillsSubjectMark(student.getBatch(),student.getAdmno(),table2Headers.get(i)+" - Skills",skills);
							
							if(skills_marks != null && skills_marks.size()>0)
								{
//									for(Marks marks :  skills_marks)
//									{
											//	System.out.println(i+"--"+skills+"--"+table2Headers.get(i));
												cell = new PdfPCell(new Phrase(skills_marks.get(0).getGrade(),boldFont));
												cell.setPaddingLeft(50f);
												//cell.setPaddingLeft(50f);
												cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
												table2.addCell(cell);
								//	}
								}
							else
							{
							//	System.out.println(i+"--"+skills+"-EMPTY-"+table2Headers.get(i));
								cell = new PdfPCell(new Phrase("",boldFont));
								
								table2.addCell(cell);
								
							}	
						}
						else
						{
							cell = new PdfPCell(new Phrase("",boldFont));
							table2.addCell(cell);
						}	
							
						/*if(!dataFound)
						{
							cell = new PdfPCell(new Phrase("",boldFont));
							
							table2.addCell(cell);
						}*/
						
					}
				}
				
				
				document.add(table2);
			
			
			document.add(tableempty);
			
			
			
			PdfPTable table4 = new PdfPTable(3);
			cell = new PdfPCell(new Phrase("S-Satisfactory",boldFont3));
			//cell.setColspan(1);
			cell.setPadding(5f);
			cell.setPaddingLeft(15f);
			cell.setPaddingBottom(5f);
			cell.setBorderWidthRight(0f);
			cell.setBorderColorRight(BaseColor.WHITE);
			table4.addCell(cell);
			
			cell = new PdfPCell(new Phrase("N-Needed improvement",boldFont3));
			//cell.setColspan(1);
			cell.setPadding(5f);
			cell.setPaddingBottom(5f);
			cell.setPaddingLeft(65f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.setBorderWidthLeft(0f);
			table4.addCell(cell);
			cell = new PdfPCell(new Phrase("U-Unsatisfactory",boldFont3));
			//cell.setColspan(1);
		//	cell.setPadding(5f);
			cell.setPaddingBottom(5f);
			cell.setPaddingLeft(130f);
			cell.setBorderColorLeft(BaseColor.WHITE);
			//cell.setBorderWidthRight(0f);
			cell.setBorderWidthLeft(0f);
			table4.addCell(cell);
			table4.setWidthPercentage(100f);
			
			document.add(table4);

			tableempty = new PdfPTable(1);
			cell = new PdfPCell(new Phrase(""));
			 cell.setBorderWidth(0f);
			cell.setFixedHeight(20f);
			tableempty.addCell(cell);
			tableempty.setWidthPercentage(100f);
			document.add(tableempty);
			
			document.add(tableempty);
			
			PdfPTable table5 = new PdfPTable(3);
			cell = new PdfPCell(new Phrase("Initials of Class Teacher",boldFont3));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setPaddingLeft(15f);
			cell.setBorderWidthRight(0f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderWidth(0f);
			table5.addCell(cell);
			cell = new PdfPCell(new Phrase("Initials of H.M / Principal",boldFont3));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setPaddingLeft(65f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.setBorderWidth(0f);
			table5.addCell(cell);
			cell = new PdfPCell(new Phrase("Initials of Parent",boldFont3));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setPaddingLeft(130f);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidth(0f);
			table5.addCell(cell);
			table5.setWidthPercentage(100f);
			
			document.add(table5);
			
			tableempty = new PdfPTable(1);
			cell = new PdfPCell(new Phrase(""));
			 cell.setBorderWidth(0f);
			cell.setFixedHeight(10f);
			tableempty.addCell(cell);
			tableempty.setWidthPercentage(100f);
			document.add(tableempty);
			document.add(tableempty);
			
			PdfPTable table6 = new PdfPTable(3);
			cell = new PdfPCell(new Phrase("Annual Report : "+student.getSession(),boldFont2));
			//cell.setColspan(1);
			cell.setPaddingLeft(5f);
			cell.setPaddingBottom(10f);
			//cell.setBorderWidthRight(0f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.BOTTOM);
			table6.addCell(cell);
			
			if(totalAttendence!=0)
			{
				cell = new PdfPCell(new Phrase(" Percentage of Attendance :  "+nf.format((totalPresentDay/totalAttendence)*100)+"%",boldFont2));

			}
			else
			{
				cell = new PdfPCell(new Phrase(" Percentage of Attendance :  "+"0%",boldFont2));
			}
			//cell = new PdfPCell(new Phrase(" Percentage of Attendance :  "+nf.format((totalPresentDay/totalAttendence)*100)+"%",boldFont2));
			//cell.setColspan(1);
			cell.setPadding(2f);
			
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			//cell.setBorderWidth(0f);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.LEFT);
			table6.addCell(cell);
			cell = new PdfPCell(new Phrase("Date: ",boldFont2));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setPaddingLeft(10f);
			cell.setBorderColorLeft(BaseColor.WHITE);
			//cell.setBorderWidth(0f);
			// cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.LEFT);
			
			table6.addCell(cell);
			
			String studentResult="", studentRemarks="";
			if(examSequenceList.contains("Final Exam"))
			{
				List<Remarks> resultList=remarksService.findResultByStudentId(student.getAdmno(), "Final Exam","Result");
				for(Remarks remarks : resultList)
				{
					if(remarks.getExamtype().equalsIgnoreCase("Final Exam") && remarks.getLabel().equalsIgnoreCase("Result"))
					{
						studentResult=remarks.getValue();
					}
					if(remarks.getExamtype().equalsIgnoreCase("Final Exam") && remarks.getLabel().equalsIgnoreCase("overall_remark"))
					{
						studentRemarks=remarks.getValue();
					}
				}
			}
			cell = new PdfPCell(new Phrase("Result : "+studentResult,boldFont2));
			//cell.setColspan(1);
			//cell.setPadding(2f);
			cell.setPaddingLeft(5f);
			cell.setPaddingBottom(10f);
			cell.setBorderWidthRight(0f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			cell = new PdfPCell(new Phrase("",boldfont1));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			cell = new PdfPCell(new Phrase(" ",boldfont1));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			

			cell = new PdfPCell(new Phrase("Remarks: "+studentRemarks,boldFont2));
			//cell.setColspan(1);
			//cell.setPadding(2f);
			cell.setPaddingLeft(5f);
			cell.setBorderWidthRight(0f);
			cell.setPaddingBottom(10f);
			
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			cell = new PdfPCell(new Phrase("School Re-opens on : ",boldFont2));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setPaddingLeft(10f);
			cell.setPaddingBottom(10f);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			
			cell = new PdfPCell(new Phrase("",boldfont1));
			//cell.setColspan(1);
			cell.setPadding(2f);
			//cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			
			

			cell = new PdfPCell(new Phrase("Class Teacher's Name : ",boldFont2));
			//cell.setColspan(1);
			//cell.setPadding(2f);
			cell.setPaddingBottom(10f);
			cell.setPaddingLeft(5f);
			
			cell.setBorderWidthRight(0f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.TOP);
			cell.disableBorderSide(Rectangle.BOTTOM);
			
			table6.addCell(cell);
			
			
			cell = new PdfPCell(new Phrase("",boldfont1));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			
			
			/*cell = new PdfPCell(new Phrase("Percentage of Attendance :  ",boldFont2));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setPaddingBottom(4f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.TOP);
			cell.disableBorderSide(Rectangle.LEFT);
			table6.addCell(cell);*/
			cell = new PdfPCell(new Phrase("PRESIDENT / PRINCIPAL ",boldFont2));
			//cell.setColspan(1);
			//cell.setPadding(2f);
			cell.setPaddingBottom(10f);
			
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.disableBorderSide(Rectangle.TOP);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.BOTTOM);
			table6.addCell(cell);
			
			
			
			
			cell = new PdfPCell(new Phrase("",boldfont1));
			//cell.setColspan(1);
			cell.setPadding(5f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			
			
			cell = new PdfPCell(new Phrase("",boldfont1));
			//cell.setColspan(1);
			cell.setPadding(5f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			
			
			cell = new PdfPCell(new Phrase("",boldfont1));
			//cell.setColspan(1);
			cell.setPadding(5f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.disableBorderSide(Rectangle.RIGHT);
			
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			

			
			
			
			
			
			System.out.println("DAAMAP: "+datMap);
			
			table6.setWidthPercentage(100f);
			
			document.add(table6);
			document.newPage();
			
	}		
			
			
	public ByteArrayInputStream closeDoc(){
		    document.close();
		    return new ByteArrayInputStream(out.toByteArray());
	
	}
	

	
	
	
	
	
	public void  createHeaderManual(RemarksService remarksService,HolidayService holidayService,MarksService marksService,MaxMinService maxMinService,WorkingDaysService workingDaysService,StudentDetailsService studentDetailsService,Student_Details student,List<String> subject_list,List<String> subject_skils_list,List<String> subject_annual_list,List<String> exam_list,Map<String, Map<String, Map<String,String>>> subjectMap,String attendence, String startDate, String endDate,String imagePath, String name, String subaddress, String affiliation,String contact, List<String> exam_list_without_max, HashMap<String, Double> examClassAverage) throws DocumentException, MalformedURLException, IOException, ParseException
	{
		
		
		int totalAttendence=0;
		int totalPresentDay=0,total_student_count=0,totalAttendenceCount=0;
		
		String attendenceCount="0";
		NumberFormat nf= NumberFormat.getInstance();
        nf.setMaximumFractionDigits(0);
		List<String> table_header=new ArrayList<String>();
		System.out.println("tabl header frm prpevioys"+exam_list_without_max);
		table_header.addAll(exam_list_without_max);
	//	table_header.add("Subject");
		//adding dynamic list
//		table_header.add("First Periodic");
//		table_header.add("First Terminal");
//		table_header.add("Second Periodic");
//		table_header.add("Second Terminal");
//		
		table_header.add("Total of Classwork");
		table_header.add("50% Of Total Classwork");
		table_header.add("50% Of Final Exam");
		table_header.add("Total");
		table_header.add("Grade");
		table_header.add("Remarks");
		
		
		
		List<String> additionalList=new ArrayList<String>();
		additionalList.add("Total");
		additionalList.add("Average");
		additionalList.add("Class Average");
		additionalList.add("No.of Working Days");
		additionalList.add("No.of Days Present");
		additionalList.add("No.of Pupils in Class");
		additionalList.add("Rank in class");
		
		List<String> table2Headers=new ArrayList<String>();
		System.out.println("exam_list_without_max"+exam_list_without_max);
//		table2Headers.add("marksInOneSubject0First Periodic");
//		table2Headers.add("First Terminal");
//		table2Headers.add("Second Periodic");
//		table2Headers.add("Second Terminal");
//		table2Headers.add("Final Exam");
		table2Headers.addAll(exam_list_without_max);
		
		
		double total_FirstPeriodic=0.0;
		
		Map<String, String> datMap=new HashMap<String, String>();
//		datMap.put("First_Periodic_Total", "0.0");
//		datMap.put("Second_Periodic_Total", "0.0");
//		datMap.put("First_Terminal_Total", "0.0");
//		datMap.put("Second_Terminal_Total", "0.0");
		
		
		datMap.put("Final_Exam_Total", "0.0");
		datMap.put("50%_Of_Final_Exam_Total", "0.0");
		datMap.put("Total_of_Classwork_Total", "0.0");
		datMap.put("50%_Of_Total_Classwork_Total", "0.0");
		datMap.put("Total_Total", "0.0");
		
		int working_days=0;
		long student_count=0;
		Working_days workingDays=workingDaysService.searchByBatch(student.getBatch());
		
		if(workingDays!= null)
		{
			working_days=workingDays.getWorking_days();
		}
		
		
		
		student_count=studentDetailsService.countByBatch(student.getBatch());
		System.out.println("studentid"+student.getStudent_id());
		List<Map<String, Object>> fullSubjectList=marksService.getFullSubjectDetails(student.getAdmno(),student.getBatch(),"");
		System.out.println(">>>>>  fullSubjectList: "+fullSubjectList);
		//System.out.println("subjectMap"+subjectMap);
		
		BatchSubject batchSubject=marksService.getSubjectByBatch(student.getBatch());
		List<String> subjects = Arrays.asList(batchSubject.getSubject().split("\\s*,\\s*"));
		
		System.out.println("SUBJECTS : "+subjects);
		
		int total_subject_count=0;
		int finalPresent=0;
		
		//logic for rank show for a exam
		Map<String,Boolean> rankMap= new HashMap<>();
		for(String exam : exam_list_without_max)
		{
			if(!exam.equals("Final Exam"))
			{
				rankMap.put(exam, true);	

			}
		}
		rankMap.put("Total", true);
		for(String subject : subjects)
		{
			double total_classwork=0.0;
			boolean finalExamPresent = false;
			double finalExam50=0.0;
		for(String exam : exam_list_without_max)
		{
			
				//ye yo ho gya exams ka
				Map<String, Object> thatSubjectFullInfo=fullSubjectList.stream().filter(obj -> obj.get("subject").equals(subject)).findAny().orElse(null);
				System.out.println("thatSubjectFullInfo"+thatSubjectFullInfo);
				if(thatSubjectFullInfo!=null)
				{
					String examWithoutSpace=exam.replaceAll("\\s+","");
					String examWithUnderScore=exam.replaceAll("\\s+","_");
					System.out.println("examWithoutSpace"+examWithoutSpace+"  examWithUnderScore"+ examWithUnderScore);
					if(datMap.get(examWithUnderScore+"_Total")==null)
					{
						datMap.put(examWithUnderScore+"_Total", "0.0");
					}
//					if(thatSubjectFullInfo.get(examWithoutSpace) == null)
//					{
//						rankMap.put(exam, false);
//					}
//					else
//					{
//						if(Double.valueOf(thatSubjectFullInfo.get(examWithoutSpace).toString())<33)
//						{
//							rankMap.put(exam, false);
//						}
//					}
					datMap.put(exam+"_"+thatSubjectFullInfo.get("subject"), (thatSubjectFullInfo.get(examWithoutSpace) == null ? "" : nf.format(Double.valueOf(thatSubjectFullInfo.get(examWithoutSpace).toString()))));
					
					datMap.put(examWithUnderScore+"_Total", String.valueOf(Double.valueOf(datMap.get(examWithUnderScore+"_Total"))+(thatSubjectFullInfo.get(examWithoutSpace)  == null ? 0.0 : Double.valueOf(thatSubjectFullInfo.get(examWithoutSpace).toString()))));
					
					datMap.put("50% Of Final Exam_"+thatSubjectFullInfo.get("subject"), (thatSubjectFullInfo.get("FinalExam50") == null ? "" : thatSubjectFullInfo.get("FinalExam50").toString()));
					datMap.put("50%_Of_Final_Exam_Total", String.valueOf(Double.valueOf(datMap.get("50%_Of_Final_Exam_Total"))+(thatSubjectFullInfo.get("FinalExam50") == null ? 0.0 : Double.valueOf(thatSubjectFullInfo.get("FinalExam50").toString()))));
//					
					double marksInOneSubject=thatSubjectFullInfo.get(examWithoutSpace) == null ? 0.0 : Double.valueOf((String) thatSubjectFullInfo.get(examWithoutSpace));
					System.out.println("marksInOneSubject"+marksInOneSubject);
					if(thatSubjectFullInfo.get(examWithoutSpace)!=null)
					{
					total_classwork+=Double.valueOf(thatSubjectFullInfo.get(examWithoutSpace).toString());
					}
					if(examWithoutSpace.equals("FinalExam"))
					{
						finalExamPresent=true;
					}
					if(thatSubjectFullInfo.get("FinalExam50")!=null)
					{
						finalExam50=Double.valueOf(thatSubjectFullInfo.get("FinalExam50").toString());
					}
					
			
				}
				else
				{
					if(!exam.equals("Final Exam"))
					{
						rankMap.put(exam, false);	

					}
				}
		}
		//now outside loop, 
		datMap.put("Total of Classwork_"+subject,String.valueOf(total_classwork));
		datMap.put("Total_of_Classwork_Total", String.valueOf(Double.valueOf(datMap.get("Total_of_Classwork_Total"))+total_classwork));
		
		datMap.put("50% Of Total Classwork_"+subject,String.valueOf(total_classwork/2));
		datMap.put("50%_Of_Total_Classwork_Total", String.valueOf(Double.valueOf(datMap.get("50%_Of_Total_Classwork_Total"))+(total_classwork/2)));
		
		
		if(finalExamPresent)
		{
			String grade="NA";
			double totalmarks=Double.valueOf(finalExam50)+(total_classwork/2); 
			if(totalmarks<40)
			{
				grade="D";
			}
			else
				if(totalmarks>=40 && totalmarks<50)
				{
					grade="C";
				}
				else if(totalmarks>=50 && totalmarks<60)
				{
					grade="C+";
				}
				else if(totalmarks>=60 && totalmarks<70)
				{
					grade="B";
				}
				else if(totalmarks>=70 && totalmarks<80)
				{
					grade="B+";
				}else if(totalmarks>=80 && totalmarks<90)
				{
					grade="A";
				}
				else if(totalmarks>=90 && totalmarks<=100)
				{
					grade="A+";
				}
				else if(totalmarks==0.0)
				{
					datMap.put("Grade_"+subject, "AB");
	
				}
			datMap.put("Grade_"+subject, grade);
		}
		else
		{
			datMap.put("Grade_"+subject, "AB");

			//datMap.put("Grade_"+subject, (obj.get("grade") == null ? "" : (obj.get("grade").toString().equalsIgnoreCase("0")||obj.get("grade").toString().equalsIgnoreCase("NA") ? "AB" : obj.get("grade").toString())));
		}
		
		
		double grand_total=(total_classwork/2)+finalExam50;
		datMap.put("Total_"+subject,String.valueOf(grand_total));
		if(grand_total<33)
		{
			rankMap.put("Total", false);
		}
		datMap.put("Total_Total", String.valueOf(Double.valueOf(datMap.get("Total_Total"))+grand_total));
		total_subject_count++;
		
		
		List<Remarks> reamrksEntity= remarksService.findRemarksByStudentId(student.getAdmno());
		for(Remarks rem: reamrksEntity)
		{
			System.out.println(rem.getLabel()+"--"+rem.getValue());
			datMap.put("Remarks_"+rem.getLabel(),rem.getValue());
		}
		
			
		}
		
//		for(Map<String, Object> obj : fullSubjectList)
//		{
//			
//			datMap.put("First Periodic_"+obj.get("subject"), (obj.get("FirstPeriodic") == null ? "" : nf.format(Double.valueOf(obj.get("FirstPeriodic").toString()))));
//			datMap.put("First_Periodic_Total", String.valueOf(Double.valueOf(datMap.get("First_Periodic_Total"))+(obj.get("FirstPeriodic")  == null ? 0.0 : Double.valueOf(obj.get("FirstPeriodic").toString()))));
//			
//			
//			datMap.put("Second Periodic_"+obj.get("subject"), (obj.get("SecondPeriodic") == null ? "" : nf.format(Double.valueOf(obj.get("SecondPeriodic").toString()))));
//			datMap.put("Second_Periodic_Total", String.valueOf(Double.valueOf(datMap.get("Second_Periodic_Total"))+(obj.get("SecondPeriodic") == null ? 0.0 : Double.valueOf(obj.get("SecondPeriodic").toString()))));
//			
//			datMap.put("First Terminal_"+obj.get("subject"), (obj.get("FirstTerminal") == null ? "" : nf.format(Double.valueOf(obj.get("FirstTerminal").toString()))));
//			datMap.put("First_Terminal_Total", String.valueOf(Double.valueOf(datMap.get("First_Terminal_Total"))+(obj.get("FirstTerminal") == null ? 0.0 : Double.valueOf(obj.get("FirstTerminal").toString()))));
//			
//			datMap.put("Second Terminal_"+obj.get("subject"), (obj.get("SecondTerminal") == null ? "" : nf.format(Double.valueOf(obj.get("SecondTerminal").toString()))));
//			datMap.put("Second_Terminal_Total", String.valueOf(Double.valueOf(datMap.get("Second_Terminal_Total"))+(obj.get("SecondTerminal") == null ? 0.0 : Double.valueOf(obj.get("SecondTerminal").toString()))));
//			
//			if(obj.get("FinalExam") != null)
//			{
//				finalPresent=1;
//			}
//			datMap.put("Final Exam_"+obj.get("subject"), (obj.get("FinalExam") == null ? "" : obj.get("FinalExam").toString()));
//			datMap.put("Final_Exam_Total", String.valueOf(Double.valueOf(datMap.get("Final_Exam_Total"))+(obj.get("FinalExam") == null ? 0.0 : Double.valueOf(obj.get("FinalExam").toString()))));
//			
//			
//			datMap.put("50% Of Final Exam_"+obj.get("subject"), (obj.get("FinalExam50") == null ? "" : obj.get("FinalExam50").toString()));
//			datMap.put("50%_Of_Final_Exam_Total", String.valueOf(Double.valueOf(datMap.get("50%_Of_Final_Exam_Total"))+(obj.get("FinalExam50") == null ? 0.0 : Double.valueOf(obj.get("FinalExam50").toString()))));
//			
//			
//			double total_classwork=0.0;
//			total_classwork=(obj.get("FirstPeriodic") == null ? 0.0 : Double.valueOf(obj.get("FirstPeriodic").toString()))+(obj.get("SecondPeriodic") == null ? 0.0 : Double.valueOf(obj.get("SecondPeriodic").toString()))+(obj.get("FirstTerminal")  == null ? 0.0 : Double.valueOf(obj.get("FirstTerminal").toString()))+(obj.get("SecondTerminal") == null ? 0.0 : Double.valueOf(obj.get("SecondTerminal").toString()));
//			datMap.put("Total of Classwork_"+obj.get("subject"),String.valueOf(total_classwork));
//			datMap.put("Total_of_Classwork_Total", String.valueOf(Double.valueOf(datMap.get("Total_of_Classwork_Total"))+total_classwork));
//			
//			datMap.put("50% Of Total Classwork_"+obj.get("subject"),String.valueOf(total_classwork/2));
//			datMap.put("50%_Of_Total_Classwork_Total", String.valueOf(Double.valueOf(datMap.get("50%_Of_Total_Classwork_Total"))+(total_classwork/2)));
//			
//			if(obj.get("FinalExam") != null)
//			{
//				String grade="NA";
//				double totalmarks=Double.valueOf(obj.get("FinalExam50").toString())+(total_classwork/2); 
//				if(totalmarks<40)
//				{
//					grade="D";
//				}
//				else
//					if(totalmarks>=40 && totalmarks<50)
//					{
//						grade="C";
//					}
//					else if(totalmarks>=50 && totalmarks<60)
//					{
//						grade="C+";
//					}
//					else if(totalmarks>=60 && totalmarks<70)
//					{
//						grade="B";
//					}
//					else if(totalmarks>=70 && totalmarks<80)
//					{
//						grade="B+";
//					}else if(totalmarks>=80 && totalmarks<90)
//					{
//						grade="A";
//					}
//					else if(totalmarks>=90 && totalmarks<=100)
//					{
//						grade="A+";
//					}
//				datMap.put("Grade_"+obj.get("subject"), grade);
//			}
//			else
//			{
//				datMap.put("Grade_"+obj.get("subject"), (obj.get("grade") == null ? "" : (obj.get("grade").toString().equalsIgnoreCase("0")||obj.get("grade").toString().equalsIgnoreCase("NA") ? "AB" : obj.get("grade").toString())));
//			}
//			
//			
//			double grand_total=(total_classwork/2)+(obj.get("FinalExam50") == null ? 0.0 : Double.valueOf(obj.get("FinalExam50").toString()));
//			datMap.put("Total_"+obj.get("subject"),String.valueOf(grand_total));
//			datMap.put("Total_Total", String.valueOf(Double.valueOf(datMap.get("Total_Total"))+grand_total));
//			total_subject_count++;
//			
//			
//			List<Remarks> reamrksEntity= remarksService.findRemarksByStudentId(student.getAdmno());
//			for(Remarks rem: reamrksEntity)
//			{
//				System.out.println(rem.getLabel()+"--"+rem.getValue());
//				datMap.put("Remarks_"+rem.getLabel(),rem.getValue());
//			}
//			
//		}
//		
		System.out.println("DAAMAP1111: "+datMap);
		
		PdfPCell cell = new PdfPCell();
		cell.setBorder(Rectangle.BOTTOM);
		cell.setBorderColor(new BaseColor(44, 67, 144));
		cell.setBorderWidth(1f);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.setHorizontalAlignment(Element.ALIGN_CENTER);
		

		table.setWidthPercentage(90f);
		
		//document.add(table);
		
		
		/** Student Details */
		
		    Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
		    Font boldFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
		    Font boldFont2 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
		    Font boldFont3 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
		    Font boldfont1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
		    Font boldfont4 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);

		    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase(" NAME: "+student.getStudent_name(), boldFont3), document.getPageSize().getLeft()+50, 1030, 0);
		    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, new Phrase(" BATCH: "+student.getBatch(), boldFont3), document.getPageSize().getRight()-50, 1030, 0);
		    
		    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("FATHER/GUARDIAN NAME: "+student.getImmediate_contact(), boldFont3), document.getPageSize().getLeft()+50, 1000, 0);
		    if(student.getPhone().equalsIgnoreCase("NA"))
		    {
		    		ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, new Phrase("PHONE NO: ", boldFont3), document.getPageSize().getRight()-120,  1000, 0);
		    }
		    else
		    {
		    	ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, new Phrase("PHONE NO: "+student.getPhone(), boldFont3), document.getPageSize().getRight()-55,  1000, 0);
		    }
		    
		    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("ADDRESS: "+student.getAddress(), boldFont3), document.getPageSize().getLeft()+50,  970, 0);
		  
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("DATE OF BIRTH:  "+(student.getDob()==null || student.getDob().equalsIgnoreCase("null") ? "" : student.getDob()), boldFont3), document.getPageSize().getLeft()+50, 940, 0);
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("GR NO: "+student.getAdmno(), boldFont3), document.getPageSize().getLeft()+250, 940, 0);
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("ROLL NO: "+(student.getRoll_no()==null || student.getRoll_no().equalsIgnoreCase("null") ? "" : student.getRoll_no()), boldFont3), document.getPageSize().getLeft()+450, 940, 0);
			    
			    ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("AIO NO: "+(student.getAio_no()==null || student.getAio_no().equalsIgnoreCase("null") ? "" : student.getAio_no()), boldFont3), document.getPageSize().getRight()-180, 940, 0);
			    
			  
		    
		    PdfPTable tableempty = new PdfPTable(1);
			cell = new PdfPCell(new Phrase(""));
			 cell.setBorderWidth(0f);
			cell.setFixedHeight(120f);
			tableempty.addCell(cell);
			tableempty.setWidthPercentage(100f);
			document.add(tableempty);
		    
		/** Table 1 **/ 
		PdfPTable table1 = new PdfPTable(exam_list.size()+2);
		cell = new PdfPCell(new Phrase("Subject",boldFont1));
		cell.setColspan(2);
		cell.setUseAscender(true);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setPaddingLeft(21f);
		table1.addCell(cell);
		table1.setWidthPercentage(100f);
//		 table1.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
//		 table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
//		
		for(String headers : table_header)
		{
			String max=maxMinService.getMaxMarksByExam(headers, student.getBatch());
//			if(headers.equalsIgnoreCase("First Periodic")|| headers.equalsIgnoreCase("Second Periodic") || headers.equalsIgnoreCase("First Terminal")|| headers.equalsIgnoreCase("Second Terminal"))
//			{
//				headers=headers.replaceAll(" ",System.lineSeparator());
//			}
			cell = new PdfPCell(new Phrase(headers+(max == null || max.equalsIgnoreCase("NULL")? "" : System.lineSeparator()+" "+nf.format(Double.valueOf(max))),boldFont2));
			//cell.setNoWrap(true);
			//cell.setPaddingLeft(10f);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table1.addCell(cell);
		}
		
		System.out.println(">>. HEADERS DONE"+table_header);
		
		for(String obj : subjects)
		{
			cell = new PdfPCell(new Phrase(obj,boldFont1));
			cell.setColspan(2);
			cell.setPadding(10f);
//			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setPaddingLeft(21f);

			table1.addCell(cell);
			for(String headers : table_header)
			{
				if(headers.equals("50% of Total Classwork"))
				{
					System.out.println(headers+"_"+obj.toString());
					System.out.println(datMap.containsKey(headers+"_"+obj.toString()) +"" +datMap.get(headers+"_"+obj.toString()));
					cell = new PdfPCell(new Phrase((datMap.containsKey(" "+headers+"_"+obj.toString()) ? datMap.get(headers+"_"+obj.toString())	: ""),boldFont1));

				}
				else
				{

				cell = new PdfPCell(new Phrase((datMap.containsKey(headers+"_"+obj.toString()) ? datMap.get(headers+"_"+obj.toString())	: ""),boldFont1));
				}
				//cell.setNoWrap(true);
				//cell.setPaddingLeft(15f);
				
				if(headers.equals("Grade"))
				{
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

				cell.setPaddingLeft(23f);	
				}
				else
				{
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);	
				}
			
				table1.addCell(cell);
			}
			
		}
		System.out.println(">>. HEADERS1 DONE"+subjects);
		System.out.println("additional headers list "+additionalList);
		for(String addColumn : additionalList)
		{
			cell = new PdfPCell(new Phrase(addColumn,boldFont1));
			cell.setColspan(2);
			cell.setPadding(10f);
//			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setPaddingLeft(21f);

			table1.addCell(cell);
			if(addColumn.equalsIgnoreCase("Total")){
				System.out.println(">>. HEADERS DONE2 TOTAL");
				for(String headers : table_header)
				{
					cell = new PdfPCell(new Phrase((datMap.containsKey(headers.replaceAll(" ", "_")+"_Total") ? datMap.get(headers.replaceAll(" ", "_")+"_Total") : ""),boldFont1));
					//cell.setNoWrap(true);
					//cell.setPaddingLeft(15f);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table1.addCell(cell);
				}
			}
			else if(addColumn.equalsIgnoreCase("Average")){
				System.out.println(">>. HEADERS DONE - AVG");
				DecimalFormat df2 = new DecimalFormat(".##");
				for(String headers : table_header)
				{
//					if(headers.equalsIgnoreCase("First Periodic")|| headers.equalsIgnoreCase("Second Periodic") || headers.equalsIgnoreCase("First Terminal")|| headers.equalsIgnoreCase("Second Terminal") || headers.equalsIgnoreCase("Total"))
//					{
					if(exam_list_without_max.contains(headers) || headers.equals("Total"))
					{
						if(!headers.equals("Final Exam"))
						{
							cell = new PdfPCell(new Phrase((datMap.containsKey(headers.replaceAll(" ", "_")+"_Total") ? String.valueOf(df2.format(Double.valueOf(datMap.get(headers.replaceAll(" ", "_")+"_Total"))/(total_subject_count-1))) : ""),boldFont1));
							//	cell.setPaddingLeft(15f);
								cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
								cell.setHorizontalAlignment(Element.ALIGN_CENTER);
								table1.addCell(cell);		
						}
						else
						{
							cell = new PdfPCell(new Phrase("",boldFont1));
							table1.addCell(cell);
						}
					
					}
//					}
					else
					{
						cell = new PdfPCell(new Phrase("",boldFont1));
						cell.setPaddingLeft(15f);
						table1.addCell(cell);
					}
				}
			}
			else if(addColumn.equalsIgnoreCase("Class Average")){
				System.out.println(">>. HEADERS DONE- CLASS AVG");
				DecimalFormat df2 = new DecimalFormat(".##");
				for(String headers : table_header)
				{
//					if(headers.equalsIgnoreCase("First Periodic")|| headers.equalsIgnoreCase("Second Periodic") || headers.equalsIgnoreCase("First Terminal")|| headers.equalsIgnoreCase("Second Terminal") || headers.equalsIgnoreCase("Total"))
//					{
					if(exam_list_without_max.contains(headers) || headers.equals("Total"))
					{
						if(!headers.equals("Final Exam"))
						{
					System.out.println("total_student"+student_count);
					double average=0;
					if(examClassAverage!=null)
					{
						 average=examClassAverage.get(headers);

					}
					
					cell = new PdfPCell(new Phrase(""+average,boldFont1));

//						cell = new PdfPCell(new Phrase((datMap.containsKey(headers.replaceAll(" ", "_")+"_Total") ? String.valueOf(df2.format(Double.valueOf(datMap.get(headers.replaceAll(" ", "_")+"_Total"))/(student_count-1))) : ""),boldFont1));
					//	cell.setPaddingLeft(15f);
						cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						table1.addCell(cell);
						}
						else
						{
							cell = new PdfPCell(new Phrase("",boldFont1));
							table1.addCell(cell);
						}
					}
//					}
					else
					{
						cell = new PdfPCell(new Phrase("",boldFont1));
						table1.addCell(cell);
					}
				}
			}
			else if(addColumn.equalsIgnoreCase("No.of Working Days")){
				System.out.println(">>. HEADERS DONE- WDAYS");
				for(String headers : table_header)
				{
					
					
						if(exam_list_without_max.contains(headers) || headers.equals("Total"))
						{
							if(!headers.equals("Final Exam"))
							{
						if(attendence.equalsIgnoreCase("manualattendance"))
						{
							List<Temp_Attendance> temp_Attendance1= studentDetailsService.getManualAttendanceWokringDays(student.getBatch(),headers,student.getAdmno());
						
							int i=0;
							for(Temp_Attendance tmp : temp_Attendance1)
							{
								if(i==0)
								{
									//attendenceCount=tmp.getTot_present();
									
									working_days=Integer.parseInt(tmp.getWorking_days());
									//totalAttendence=totalAttendence+working_days;
								}
								i++;
							}
						}
						if(headers.equalsIgnoreCase("Total"))
						{
							cell = new PdfPCell(new Phrase(""+totalAttendence,boldfont1));
						}
						else
						{
							totalAttendence=totalAttendence+working_days;
							cell = new PdfPCell(new Phrase(""+working_days,boldfont1));
						}
							}
							else
							{
								cell = new PdfPCell(new Phrase("",boldFont1));
							}
						//cell = new PdfPCell(new Phrase(""+working_days,boldFont1));
						//cell.setPaddingLeft(15f);
						cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						table1.addCell(cell);
					}
					else
					{
						cell = new PdfPCell(new Phrase("",boldFont1));
						
						table1.addCell(cell);
					}
				}
			}
			else if(addColumn.equalsIgnoreCase("No.of Days Present")){
				System.out.println(">>. HEADERS DONE - NDAYS PRESENT");
				for(String headers : table_header)
				{
					
					if(exam_list_without_max.contains(headers) || headers.equals("Total"))
					{
						if(!headers.equals("Final Exam"))
						{
						if(attendence.equalsIgnoreCase("manualattendance"))
						{
							List<Temp_Attendance> temp_Attendance1= studentDetailsService.getManualAttendanceWokringDays(student.getBatch(),headers,student.getAdmno());
						
							
							int i=0;
							for(Temp_Attendance tmp : temp_Attendance1)
							{
								System.out.println("MANULA: "+tmp.getTot_present());
								if(i==0)
								{
								
									attendenceCount=tmp.getTot_present();
									totalAttendenceCount=totalAttendenceCount+Integer.parseInt(attendenceCount);
								}
								i++;
							}
						}
						}
						else
						{
							cell = new PdfPCell(new Phrase("",boldFont1));

						}
						if(headers.equalsIgnoreCase("Total"))
						{
							cell = new PdfPCell(new Phrase(""+totalPresentDay,boldfont1));
						}
						else
						{
							totalPresentDay=totalPresentDay+Integer.parseInt(attendenceCount);
							if(!headers.equals("Final Exam"))
							{
								cell = new PdfPCell(new Phrase(""+attendenceCount,boldfont1));

							}
							else
							{
								cell = new PdfPCell(new Phrase("",boldfont1));

							}
						}
					//	cell = new PdfPCell(new Phrase(""+attendenceCount,boldFont1));
						//cell.setPaddingLeft(15f);
						cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						table1.addCell(cell);
					}
					else
					{
						cell = new PdfPCell(new Phrase("",boldFont1));
						table1.addCell(cell);
					}
				}
			}
			else if(addColumn.equalsIgnoreCase("No.of Pupils in Class")){
				System.out.println(">>. HEADERS DONE - PUPILS");
				for(String headers : table_header)
				{
					if(exam_list_without_max.contains(headers) || headers.equals("Total"))
					{
						if(!headers.equals("Final Exam"))
						{
						cell = new PdfPCell(new Phrase(""+student_count,boldFont1));
					//	cell.setPaddingLeft(15f);
						cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						table1.addCell(cell);
						}
						else
						{
							cell = new PdfPCell(new Phrase("",boldFont1));
							table1.addCell(cell);
						}
					}
					else
					{
						cell = new PdfPCell(new Phrase("",boldFont1));
						table1.addCell(cell);
					}
				}
			}
			else if(addColumn.equalsIgnoreCase("Rank in class")){
				System.out.println(">>. HEADERS DONE - RANK"+rankMap);
				for(String headers : table_header)
				{ 
//					if(headers.equalsIgnoreCase("First Periodic")|| headers.equalsIgnoreCase("Second Periodic") || headers.equalsIgnoreCase("First Terminal")|| headers.equalsIgnoreCase("Second Terminal") || headers.equalsIgnoreCase("Total"))
//					{
					if((exam_list_without_max.contains(headers) || headers.equals("Total")) && !headers.equals("Final Exam"))
					{
					if((rankMap.containsKey(headers) && rankMap.get(headers) == true)) {
						int rank=1, rankfound=0;
						System.out.println("header inside"+headers);
						List<Map<String, Object>> rankClass= new ArrayList<>();
						if(headers.equals("Total"))
						{
							rankClass=marksService.getStudentRankInClassForFullExam(student.getBatch(),exam_list_without_max);
						}
						else
						{
							 rankClass=marksService.getStudentRankInClass(student.getBatch(),headers);

						}
						System.out.println("RANK CLASS"+rankClass);
						for(Map<String, Object> obj : rankClass)
						{
							System.out.println(obj.get("admno").toString()+"--"+obj.get("admno").toString()+"--"+student.getAdmno());
							if(rankfound==0)
							{
								if(obj.get("admno").toString().equalsIgnoreCase(student.getAdmno()))
								{
									
									rankfound=rank;
									
								}
								else
								{
									rank++;
								}
							}
						}
						if(rankfound==0)
						{
							cell = new PdfPCell(new Phrase("",boldFont1));
						}
						else
						{
							cell = new PdfPCell(new Phrase(""+rankfound,boldFont1));
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);

						}
					}
					else
					{
						cell = new PdfPCell(new Phrase("-",boldFont1));
						cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);

					}
					}
					else
					{
						cell = new PdfPCell(new Phrase("",boldFont1));
						cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);

					}
						//cell.setPaddingLeft(15f);
						
					
						table1.addCell(cell);
//					}
//					else
//					{
//						cell = new PdfPCell(new Phrase("",boldFont1));
//						table1.addCell(cell);
//					}
				}
			}
			else { // here add for rest of the things
				for(String headers : table_header)
				{
					
						cell = new PdfPCell(new Phrase("",boldFont1));
						table1.addCell(cell);
				}
			}
		}
		
		
		/*for(String subjects : subject_list)
		{
			
			cell = new PdfPCell(new Phrase(subjects,boldFont1));
			cell.setColspan(2);
			cell.setPadding(10f);
			table1.addCell(cell);
			for(int i=0;i<exam_list.size(); i++)
			{
				
			}
			
		}*/
	//	System.out.println(student.getStudent_id()+"row_column_map"+row_column_map);
		
		/*for(String addColumn : additionalList)
		{
			cell = new PdfPCell(new Phrase(addColumn,boldFont1));
			cell.setColspan(2);
			cell.setPadding(10f);
			table1.addCell(cell);
			for(int i=0;i<exam_list.size(); i++)
			{
				cell = new PdfPCell(new Phrase("",boldFont));
				
				table1.addCell(cell);
			}
		}*/
		table1.completeRow();
		//table1.writeSelectedRows(0,-1, 30, 650, writer.getDirectContent());
		
		Paragraph paragraphTable2 = new Paragraph(); 
        paragraphTable2.setSpacingAfter(10f);
        paragraphTable2.add(table1);
        paragraphTable2.setAlignment(Element.ALIGN_CENTER);
		
        // ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, paragraphTable2,30, 590, 0);
        
        document.add(paragraphTable2);
		
        document.newPage();
        System.out.println(">>. first page done");
//	}
		    
//		    System.out.println("SKILLS: "+subject_skils_list);
//		    System.out.println("ANNUAL: "+subject_annual_list);
		   
			
				
			
			PdfPTable table3 = new PdfPTable(table2Headers.size()+1);
			
			
			cell = new PdfPCell(new Phrase("Subject",boldFont1));
			//cell.setColspan(1);
			//cell.setPadding(2f);
			cell.setFixedHeight(30f);

			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		//	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setPaddingLeft(35f);
			table3.addCell(cell);
			
			table3.setWidthPercentage(100f);
			for(String addColumn : table2Headers)
			{
				cell = new PdfPCell(new Phrase(addColumn,boldFont1));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(30f);

				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table3.addCell(cell);
			}
			for(String skills : subject_annual_list)
			{
				cell = new PdfPCell(new Phrase(skills,boldFont1));
				//cell.setPadding(5f);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(30f);

//				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setPaddingLeft(35f);

				table3.addCell(cell);
				for(int i=0;i<table2Headers.size(); i++)
				{
					List<Marks> skills_marks=marksService.findSkillsSubjectMark(student.getBatch(),student.getAdmno(),table2Headers.get(i)+" - G2",skills);
					
						if(skills_marks != null && skills_marks.size()>0)
							{
//								for(Marks marks :  skills_marks)
//								{
											cell = new PdfPCell(new Phrase(skills_marks.get(0).getGrade(),boldFont));
//											cell.setPaddingLeft(50f);
											cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
											cell.setFixedHeight(30f);

											//cell.setHorizontalAlignment(Element.ALIGN_CENTER);
											cell.setPaddingLeft(69f);

											table3.addCell(cell);
								//}
							}
						else
						{
							
							cell = new PdfPCell(new Phrase("",boldFont));
							
							table3.addCell(cell);
							
						}				
					}
			}
			
			
			document.add(table3);
			
			

			
			tableempty = new PdfPTable(1);
			cell = new PdfPCell(new Phrase(""));
			 cell.setBorderWidth(0f);
			cell.setFixedHeight(40f);
			tableempty.addCell(cell);
			tableempty.setWidthPercentage(100f);
			document.add(tableempty);
			
			
			 PdfPTable table2 = new PdfPTable(table2Headers.size()+1);
				
				
			    
				cell = new PdfPCell(new Phrase("Subject",boldFont1));
				//cell.setColspan(1);
//				cell.setPadding(2f);
				cell.setFixedHeight(30f);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setPaddingLeft(35f);
				table2.addCell(cell);
				table2.setWidthPercentage(100f);
				for(String addColumn : table2Headers)
				{
					cell = new PdfPCell(new Phrase(addColumn,boldFont1));
					cell.setFixedHeight(30f);

					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table2.addCell(cell);
				}
				
				System.out.println(table2Headers+"SKILLS:"+subject_skils_list);
				for(String skills : subject_skils_list)
				{
					cell = new PdfPCell(new Phrase(skills,boldFont1));
//					cell.setPadding(5f);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(30f);

//					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setPaddingLeft(35f);
					table2.addCell(cell);
					for(int i=0;i<table2Headers.size(); i++)
					{
						List<Marks> skills_marks=marksService.findSkillsSubjectMark(student.getBatch(),student.getAdmno(),table2Headers.get(i)+" - Skills",skills);
						
						if(skills_marks != null && skills_marks.size()>0)
							{
//								for(Marks marks :  skills_marks)
//								{
											System.out.println(i+"--"+skills+"--"+table2Headers.get(i));
											cell = new PdfPCell(new Phrase(skills_marks.get(0).getGrade(),boldFont));
//											cell.setPaddingLeft(50f);
											cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
											cell.setFixedHeight(30f);

										//	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
											cell.setPaddingLeft(69f);

											table2.addCell(cell);
								//}
							}
						else
						{
							System.out.println(i+"--"+skills+"-EMPTY-"+table2Headers.get(i));
							cell = new PdfPCell(new Phrase("",boldFont));
							
							table2.addCell(cell);
							
						}
						
						/*if(!dataFound)
						{
							cell = new PdfPCell(new Phrase("",boldFont));
							
							table2.addCell(cell);
						}*/
						
					}
				}
				
				
				document.add(table2);
			
			
			document.add(tableempty);
			
			System.out.println(">>. Last Table Started");
			
			PdfPTable table4 = new PdfPTable(3);
			cell = new PdfPCell(new Phrase("S-Satisfactory",boldFont3));
			//cell.setColspan(1);
			cell.setPadding(5f);
			cell.setPaddingLeft(15f);
			cell.setPaddingBottom(5f);
			cell.setBorderWidthRight(0f);
			cell.setBorderColorRight(BaseColor.WHITE);
			table4.addCell(cell);
			
			cell = new PdfPCell(new Phrase("N-Needed improvement",boldFont3));
			//cell.setColspan(1);
			cell.setPadding(5f);
			cell.setPaddingBottom(5f);
			cell.setPaddingLeft(65f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.setBorderWidthLeft(0f);
			table4.addCell(cell);
			cell = new PdfPCell(new Phrase("U-Unsatisfactory",boldFont3));
			//cell.setColspan(1);
		//	cell.setPadding(5f);
			cell.setPaddingBottom(5f);
			cell.setPaddingLeft(130f);
			cell.setBorderColorLeft(BaseColor.WHITE);
			//cell.setBorderWidthRight(0f);
			cell.setBorderWidthLeft(0f);
			table4.addCell(cell);
			table4.setWidthPercentage(100f);
			
			document.add(table4);

			tableempty = new PdfPTable(1);
			cell = new PdfPCell(new Phrase(""));
			 cell.setBorderWidth(0f);
			cell.setFixedHeight(20f);
			tableempty.addCell(cell);
			tableempty.setWidthPercentage(100f);
			document.add(tableempty);
			
			document.add(tableempty);
			
			PdfPTable table5 = new PdfPTable(3);
			cell = new PdfPCell(new Phrase("Initials of Class Teacher",boldFont3));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setPaddingLeft(15f);
			cell.setBorderWidthRight(0f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderWidth(0f);
			table5.addCell(cell);
			cell = new PdfPCell(new Phrase("Initials of H.M / Principal",boldFont3));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setPaddingLeft(65f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.setBorderWidth(0f);
			table5.addCell(cell);
			cell = new PdfPCell(new Phrase("Initials of Parent",boldFont3));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setPaddingLeft(130f);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidth(0f);
			table5.addCell(cell);
			table5.setWidthPercentage(100f);
			
			document.add(table5);
			
			tableempty = new PdfPTable(1);
			cell = new PdfPCell(new Phrase(""));
			 cell.setBorderWidth(0f);
			cell.setFixedHeight(10f);
			tableempty.addCell(cell);
			tableempty.setWidthPercentage(100f);
			document.add(tableempty);
			document.add(tableempty);
			
			PdfPTable table6 = new PdfPTable(3);
			cell = new PdfPCell(new Phrase("Annual Report : "+student.getSession(),boldFont2));
			//cell.setColspan(1);
			cell.setPaddingLeft(5f);
			cell.setPaddingBottom(10f);
			//cell.setBorderWidthRight(0f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.BOTTOM);
			table6.addCell(cell);
			if(totalAttendence!=0)
			{
				cell = new PdfPCell(new Phrase(" Percentage of Attendance :  "+nf.format((totalPresentDay/totalAttendence)*100)+"%",boldFont2));

			}
			else
			{
				cell = new PdfPCell(new Phrase(" Percentage of Attendance :  "+"0%",boldFont2));
			}
			//cell = new PdfPCell(new Phrase(" Percentage of Attendance :  "+nf.format((totalPresentDay/totalAttendence)*100)+"%",boldFont2));
			//cell.setColspan(1);
			cell.setPadding(2f);
			
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			//cell.setBorderWidth(0f);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.LEFT);
			table6.addCell(cell);
			cell = new PdfPCell(new Phrase("Date: ",boldFont2));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setPaddingLeft(10f);
			cell.setBorderColorLeft(BaseColor.WHITE);
			//cell.setBorderWidth(0f);
			// cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.LEFT);
			
			table6.addCell(cell);


			System.out.println(">>. TABLE DONE - SETTING REMARKS");
			
			String studentResult="";
			
				List<Remarks> resultList=remarksService.findResultByStudentId(student.getAdmno(), "Final Exam","Result");
				for(Remarks remarks : resultList)
				{
					if(remarks.getExamtype().equalsIgnoreCase("Final Exam") && remarks.getLabel().equalsIgnoreCase("Result"))
					{
						studentResult=remarks.getValue();
					}
				}
			cell = new PdfPCell(new Phrase("Result : "+studentResult,boldFont2));
			//cell.setColspan(1);
			//cell.setPadding(2f);
			cell.setPaddingLeft(5f);
			cell.setPaddingBottom(10f);
			cell.setBorderWidthRight(0f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			cell = new PdfPCell(new Phrase("",boldfont1));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			cell = new PdfPCell(new Phrase(" ",boldfont1));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			
			System.out.println(">>. SETTING OVERALL REMARKS");
			String remarks="";
			if(finalPresent == 1){
				List<Remarks> reamrksEntity= remarksService.findRemarksByStudentId(student.getAdmno());
				for(Remarks rem: reamrksEntity)
				{
					if(rem.getLabel().equalsIgnoreCase("overall_remark"))
					{
						remarks=rem.getValue();
					}
					
				}
			}
			cell = new PdfPCell(new Phrase("Remarks: "+remarks,boldFont2));
			//cell.setColspan(1);
			//cell.setPadding(2f);
			cell.setPaddingLeft(5f);
			cell.setBorderWidthRight(0f);
			cell.setPaddingBottom(10f);
			
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			cell = new PdfPCell(new Phrase("School Re-opens on : ",boldFont2));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setPaddingLeft(10f);
			cell.setPaddingBottom(10f);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			
			cell = new PdfPCell(new Phrase("",boldfont1));
			//cell.setColspan(1);
			cell.setPadding(2f);
			//cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			
			cell.disableBorderSide(Rectangle.BOTTOM);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			
			

			cell = new PdfPCell(new Phrase("Class Teacher's Name : ",boldFont2));
			//cell.setColspan(1);
			//cell.setPadding(2f);
			cell.setPaddingBottom(10f);
			cell.setPaddingLeft(5f);
			
			cell.setBorderWidthRight(0f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.TOP);
			cell.disableBorderSide(Rectangle.BOTTOM);
			
			table6.addCell(cell);
			
			
			cell = new PdfPCell(new Phrase("",boldfont1));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			
			
			/*cell = new PdfPCell(new Phrase("Percentage of Attendance :  ",boldFont2));
			//cell.setColspan(1);
			cell.setPadding(2f);
			cell.setPaddingBottom(4f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.TOP);
			cell.disableBorderSide(Rectangle.LEFT);
			table6.addCell(cell);*/
			cell = new PdfPCell(new Phrase("PRESIDENT / PRINCIPAL ",boldFont2));
			//cell.setColspan(1);
			//cell.setPadding(2f);
			cell.setPaddingBottom(10f);
			
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.disableBorderSide(Rectangle.TOP);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.BOTTOM);
			table6.addCell(cell);
			
			
			
			
			cell = new PdfPCell(new Phrase("",boldfont1));
			//cell.setColspan(1);
			cell.setPadding(5f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			
			
			cell = new PdfPCell(new Phrase("",boldfont1));
			//cell.setColspan(1);
			cell.setPadding(5f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.disableBorderSide(Rectangle.RIGHT);
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			
			
			cell = new PdfPCell(new Phrase("",boldfont1));
			//cell.setColspan(1);
			cell.setPadding(5f);
			cell.setBorderColorRight(BaseColor.WHITE);
			cell.setBorderColorLeft(BaseColor.WHITE);
			cell.setBorderWidthRight(0f);
			cell.disableBorderSide(Rectangle.RIGHT);
			
			cell.disableBorderSide(Rectangle.LEFT);
			cell.disableBorderSide(Rectangle.TOP);
			table6.addCell(cell);
			
			
			table6.setWidthPercentage(100f);
			
			document.add(table6);
			document.newPage();
			
	}

	public HashMap<String, Double> getClassAverageOfStudent(RemarksService remarksService, HolidayService holidayService,
			MarksService marksService, MaxMinService maxMinService, WorkingDaysService workingDaysService,
			StudentDetailsService studentDetailsService, Student_Details student, List<String> subject_list,
			List<String> exam_skills_list, List<String> exam_annual_list, List<String> exam_list,
			Map<String, Map<String, Map<String, String>>> subjectMap, String attendence, String string, String string2,
			String string3, String string4, String string5, String string6, String string7,
			List<String> exam_list_without_max) {
		
		int totalAttendence=0;
		int totalPresentDay=0,total_student_count=0,totalAttendenceCount=0;
		int total_subject_count=0;
		String attendenceCount="0";
		NumberFormat nf= NumberFormat.getInstance();
        nf.setMaximumFractionDigits(0);
		List<String> table_header=new ArrayList<String>();
		System.out.println("tabl header frm prpevioys"+exam_list_without_max);
		table_header.addAll(exam_list_without_max);
	//	table_header.add("Subject");
		//adding dynamic list
//		table_header.add("First Periodic");
//		table_header.add("First Terminal");
//		table_header.add("Second Periodic");
//		table_header.add("Second Terminal");
//		
		table_header.add("Total of Classwork");
		table_header.add("50% Of Total Classwork");
		table_header.add("50% Of Final Exam");
		table_header.add("Total");
		table_header.add("Grade");
		table_header.add("Remarks");
		
		
		
		List<String> additionalList=new ArrayList<String>();
		additionalList.add("Total");
		additionalList.add("Average");
		additionalList.add("Class Average");
		additionalList.add("No.of Working Days");
		additionalList.add("No.of Days Present");
		additionalList.add("No.of Pupils in Class");
		additionalList.add("Rank in class");
		
		
		
		List<Map<String, Object>> fullSubjectList=marksService.getFullSubjectDetails(student.getAdmno(),student.getBatch(),"");
		System.out.println(">>>>>  fullSubjectList: "+fullSubjectList);
		//System.out.println("subjectMap"+subjectMap);
		Map<String, String> datMap=new HashMap<String, String>();
		datMap.put("Final_Exam_Total", "0.0");
		datMap.put("50%_Of_Final_Exam_Total", "0.0");
		datMap.put("Total_of_Classwork_Total", "0.0");
		datMap.put("50%_Of_Total_Classwork_Total", "0.0");
		datMap.put("Total_Total", "0.0");
		
		BatchSubject batchSubject=marksService.getSubjectByBatch(student.getBatch());
		List<String> subjects = Arrays.asList(batchSubject.getSubject().split("\\s*,\\s*"));
		for(String subject : subjects)
		{
			double total_classwork=0.0;
			boolean finalExamPresent = false;
			double finalExam50=0.0;
		for(String exam : exam_list_without_max)
		{
			//ye yo ho gya exams ka
			Map<String, Object> thatSubjectFullInfo=fullSubjectList.stream().filter(obj -> obj.get("subject").equals(subject)).findAny().orElse(null);
			System.out.println("thatSubjectFullInfo"+thatSubjectFullInfo);
			if(thatSubjectFullInfo!=null)
			{
				String examWithoutSpace=exam.replaceAll("\\s+","");
				String examWithUnderScore=exam.replaceAll("\\s+","_");
				System.out.println("examWithoutSpace"+examWithoutSpace+"  examWithUnderScore"+ examWithUnderScore);
				if(datMap.get(examWithUnderScore+"_Total")==null)
				{
					datMap.put(examWithUnderScore+"_Total", "0.0");
				}
				datMap.put(exam+"_"+thatSubjectFullInfo.get("subject"), (thatSubjectFullInfo.get(examWithoutSpace) == null ? "" : nf.format(Double.valueOf(thatSubjectFullInfo.get(examWithoutSpace).toString()))));
				
				datMap.put(examWithUnderScore+"_Total", String.valueOf(Double.valueOf(datMap.get(examWithUnderScore+"_Total"))+(thatSubjectFullInfo.get(examWithoutSpace)  == null ? 0.0 : Double.valueOf(thatSubjectFullInfo.get(examWithoutSpace).toString()))));
				
				datMap.put("50% Of Final Exam_"+thatSubjectFullInfo.get("subject"), (thatSubjectFullInfo.get("FinalExam50") == null ? "" : thatSubjectFullInfo.get("FinalExam50").toString()));
				datMap.put("50%_Of_Final_Exam_Total", String.valueOf(Double.valueOf(datMap.get("50%_Of_Final_Exam_Total"))+(thatSubjectFullInfo.get("FinalExam50") == null ? 0.0 : Double.valueOf(thatSubjectFullInfo.get("FinalExam50").toString()))));
//				
				double marksInOneSubject=thatSubjectFullInfo.get(examWithoutSpace) == null ? 0.0 : Double.valueOf((String) thatSubjectFullInfo.get(examWithoutSpace));
				System.out.println("marksInOneSubject"+marksInOneSubject);
				if(thatSubjectFullInfo.get(examWithoutSpace)!=null)
				{
				total_classwork+=Double.valueOf(thatSubjectFullInfo.get(examWithoutSpace).toString());
				}
				if(examWithoutSpace.equals("FinalExam"))
				{
					finalExamPresent=true;
				}
				if(thatSubjectFullInfo.get("FinalExam50")!=null)
				{
					finalExam50=Double.valueOf(thatSubjectFullInfo.get("FinalExam50").toString());
				}
				
		
			}
			else
			{
				
//				showRank=false;
			}
		}
		
		datMap.put("Total of Classwork_"+subject,String.valueOf(total_classwork));
		datMap.put("Total_of_Classwork_Total", String.valueOf(Double.valueOf(datMap.get("Total_of_Classwork_Total"))+total_classwork));
		
		datMap.put("50% Of Total Classwork_"+subject,String.valueOf(total_classwork/2));
		datMap.put("50%_Of_Total_Classwork_Total", String.valueOf(Double.valueOf(datMap.get("50%_Of_Total_Classwork_Total"))+(total_classwork/2)));
		
		
		double grand_total=(total_classwork/2)+finalExam50;
		datMap.put("Total_"+subject,String.valueOf(grand_total));
		datMap.put("Total_Total", String.valueOf(Double.valueOf(datMap.get("Total_Total"))+grand_total));
		total_subject_count++;
		
		
		}
		HashMap<String, Double> examAverage= new HashMap<>();
		for(String addColumn : additionalList)
		{
			if(addColumn.equalsIgnoreCase("Average")){
				DecimalFormat df2 = new DecimalFormat(".##");
				for(String headers : table_header)
				{
//					if(headers.equalsIgnoreCase("First Periodic")|| headers.equalsIgnoreCase("Second Periodic") || headers.equalsIgnoreCase("First Terminal")|| headers.equalsIgnoreCase("Second Terminal") || headers.equalsIgnoreCase("Total"))
//					{
					if(exam_list_without_max.contains(headers) || headers.equals("Total"))
					{
						if(!headers.equals("Final Exam"))
						{
							if(datMap.containsKey(headers.replaceAll(" ", "_")+"_Total"))
							{
								examAverage.put(headers,Double.valueOf(df2.format(Double.valueOf(datMap.get(headers.replaceAll(" ", "_")+"_Total"))/subjects.size())));
							}
							else
							{
								examAverage.put(headers,0.0);

							}
							

						}
					
					
					}
//					
					
				}
			}
		}
		// TODO Auto-generated method stub
		return examAverage;
	}		
			
	
	
	
	
	
	
	
	
	
	
	
	
}
