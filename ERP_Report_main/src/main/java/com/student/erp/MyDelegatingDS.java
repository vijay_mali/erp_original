package com.student.erp;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DelegatingDataSource;


public class MyDelegatingDS extends DelegatingDataSource {
	  private final String catalogName;

	  public MyDelegatingDS(final String catalogName, final DataSource dataSource) {
	    super(dataSource);
	    this.catalogName = catalogName;
	  }

	  @Override
	  public Connection getConnection() throws SQLException {
	    final Connection cnx = super.getConnection();
	    cnx.setCatalog(this.catalogName);
	    return cnx;
	  }

	 
}
