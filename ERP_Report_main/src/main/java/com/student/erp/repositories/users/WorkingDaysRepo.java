package com.student.erp.repositories.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.student.erp.MasterDBConfigProperties;
import com.student.erp.entities.Working_days;

@Repository("workingDaysRepo")
public class WorkingDaysRepo{

	
	@Autowired
	MasterDBConfigProperties masterDbProperties;
	
	
	@Autowired
	private HttpServletRequest request;
	
	 @Value("${app.db.url}")
	 private String DB_CONNECTION_STRING;

	
	//@Query("Update Working_days set working_days=?1  where batch=?2 ")
	public Working_days update(int working_days, String batch)
	{
		return null;
	}

//	@Query("Select u from  Working_days u where batch=?1 ")
	public Working_days findByBatch(String batch)
	{
		String sql="Select * from  Working_days u where batch='"+batch+"'";
		 List<Working_days> working_days=getJdbcTemplate().query(sql, 
					new RowMapper<Working_days>(){
	            public Working_days mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Working_days st = new Working_days();
	            	st.setId(rs.getInt("id"));
	            	st.setBatch(rs.getString("batch"));
	            	st.setWorking_days(Integer.parseInt(rs.getString("working_days")));
	            	
	            	return st;
	            	    
	            }
			});
		  
		 if(working_days.size()> 0)
		  return working_days.get(0);
		 else
			 return null;
	}
	
	private JdbcTemplate getJdbcTemplate()
	{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	       dataSource.setDriverClassName(masterDbProperties.getDriverClassName());
	       dataSource.setUrl(DB_CONNECTION_STRING + request.getSession().getAttribute("USER_DB"));
	       dataSource.setUsername(masterDbProperties.getUsername());
	       dataSource.setPassword(masterDbProperties.getPassword());
	       
	       JdbcTemplate jdbcTemplates= new JdbcTemplate();
	       jdbcTemplates.setDataSource(dataSource);
	       return jdbcTemplates;
	}
	
	
}
