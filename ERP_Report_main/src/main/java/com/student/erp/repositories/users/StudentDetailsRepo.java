package com.student.erp.repositories.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.student.erp.MasterDBConfigProperties;
import com.student.erp.entities.Student_Details;


@Repository("studentDetailsRepo")
public class StudentDetailsRepo{
	
	@Autowired
	MasterDBConfigProperties masterDbProperties;
	
	
	@Autowired
	private HttpServletRequest request;
	
	 @Value("${app.db.url}")
	 private String DB_CONNECTION_STRING;
	
	/*@Query(value="select ROUND((select count(case forenoon when 'true' then 1 else null end) from attendence_period WHERE admno=?1 AND session=?2)-(select count(case afternoon when 'false' then 1 else null end)/2 from attendence_period WHERE admno=?1 AND session=?2))", nativeQuery=true)
	public String getAttendence(String admno, String session);
	
	
	@Query("SELECT distinct u.batch FROM Student_Details u Order by u.batch asc")
	public List getBatchList();
	
	@Query("SELECT u FROM Student_Details u where u.batch=?1 Order by u.batch asc")
	public List<Student_Details> searchByBatch(String batch);
	
	@Query("SELECT count(u) FROM Student_Details u where u.batch=?1")
	public Long countByBatch(String batch);
	
	public Student_Details findByAdmno(String admno);
	 
	
	@Query("SELECT u FROM Student_Details u where u.id=?1")
	public Student_Details findOne(long id);
	*/
	
	 public String getAttendence(String admno, String session)
	 {
		 String sql="select ROUND((select count(case forenoon when 'true' then 1 else null end) from attendence_period WHERE admno='"+admno+"' AND session='"+session+"')-(select count(case afternoon when 'false' then 1 else null end)/2 from attendence_period WHERE admno='"+admno+"' AND session='"+session+"'))";
		 String result=getJdbcTemplate().queryForObject(sql, new Object[]{},String.class);
		 return result;
	 }
	 
	 public Student_Details findOne(long id)
	 {
		 String sql="SELECT * FROM Student_Details u where u.student_id="+id+" Order by u.batch asc";
		  List<Student_Details> student_details=getJdbcTemplate().query(sql, 
					new RowMapper<Student_Details>(){
	            public Student_Details mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Student_Details st = new Student_Details();
	            	st.setStudent_id(rs.getInt("student_id"));
	            	st.setStudent_name(rs.getString("student_name"));
	            	st.setAdmno(rs.getString("admno"));
	            	
	            	return st;
	            	    
	            }
			});
		  
		  return student_details.get(0);
	 }
	 public long countByBatch(String batch)
	 {
		 String sql="SELECT count(*) FROM Student_Details u where u.batch='"+batch+"'";
		 long result=getJdbcTemplate().queryForObject(sql, new Object[]{},Long.class);
		 return result;
	 }
	 
	 public List<Student_Details> findAll()
	 {
		 String sql="SELECT * FROM Student_Details u Order by u.batch asc";
		  List<Student_Details> student_details=getJdbcTemplate().query(sql, 
					new RowMapper<Student_Details>(){
	            public Student_Details mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Student_Details st = new Student_Details();
	            	st.setStudent_id(rs.getInt("student_id"));
	            	st.setStudent_name(rs.getString("student_name"));
	            	st.setAdmno(rs.getString("admno"));
	            	
	            	return st;
	            	    
	            }
			});
		  
		  return student_details;
	 }
	 
	 
	 public Page<Student_Details> findAll(Pageable page)
	 {
		 String sql="SELECT * FROM Student_Details u";
		 String countSql="SELECT count(1) FROM Student_Details u";
		 long total =getJdbcTemplate().queryForObject(
				 	countSql,Long.class
	                );
		  List<Student_Details> student_details=getJdbcTemplate().query(sql, 
					new RowMapper<Student_Details>(){
	            public Student_Details mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Student_Details st = new Student_Details();
	            	st.setStudent_id(rs.getInt("student_id"));
	            	st.setStudent_name(rs.getString("student_name"));
	            	st.setAdmno(rs.getString("admno"));
	            	
	            	return st;
	            	    
	            }
			});
		  
		  return new PageImpl<>(student_details, page, total);
	 }
	 
	 
	 public Student_Details findByAdmno(String admno)
	 {
		 String sql="SELECT * FROM Student_Details u where u.admno='"+admno+"' Order by u.batch asc";
		  List<Student_Details> student_details=getJdbcTemplate().query(sql, 
					new RowMapper<Student_Details>(){
	            public Student_Details mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Student_Details st = new Student_Details();
	            	st.setStudent_id(rs.getInt("student_id"));
	            	st.setStudent_name(rs.getString("student_name"));
	            	st.setAdmno(rs.getString("admno"));
	            	st.setBatch(rs.getString("batch"));
	            	st.setPhone(rs.getString("phone"));
	            	st.setSession(rs.getString("session"));
	            	st.setImmediate_contact(rs.getString("immediate_contact"));
	            	st.setAddress(rs.getString("address"));
	            	st.setDob(rs.getString("dob"));
	            	st.setAio_no(rs.getString("aio_no"));
	            	
	            	st.setRoll_no(rs.getString("roll_no"));
	            	return st;
	            	    
	            }
			});
		  
		  return student_details.get(0);
	 }
	 
	
	  public List<Student_Details> searchByBatch(String batch)
		{
		  String sql="SELECT * FROM Student_Details u where u.batch='"+batch+"' Order by u.batch asc";
		  List<Student_Details> student_details=getJdbcTemplate().query(sql, 
					new RowMapper<Student_Details>(){
	            public Student_Details mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Student_Details st = new Student_Details();
	            	st.setStudent_id(rs.getInt("student_id"));
	            	st.setStudent_name(rs.getString("student_name"));
	            	st.setAdmno(rs.getString("admno"));
	            	st.setBatch(rs.getString("batch"));
	            	st.setPhone(rs.getString("phone"));
	            	st.setAddress(rs.getString("address"));
	            	st.setDob(rs.getString("dob"));
	            	st.setRoll_no(rs.getString("roll_no"));
	            	st.setAio_no(rs.getString("aio_no"));
	            	st.setImmediate_contact(rs.getString("immediate_contact"));
	            	st.setSession(rs.getString("session"));
	            	return st;
	            	    
	            }
			});
		  
		  return student_details;
		}
	
	  
	  public List<Student_Details> searchByBatchAndAdmno(String batch, String admno)
	  {
		  String sql="SELECT * FROM Student_Details u where u.batch='"+batch+"' and u.admno='"+admno+"' Order by u.batch asc";
		  List<Student_Details> student_details=getJdbcTemplate().query(sql, 
					new RowMapper<Student_Details>(){
	            public Student_Details mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Student_Details st = new Student_Details();
	            	st.setStudent_id(rs.getInt("student_id"));
	            	st.setStudent_name(rs.getString("student_name"));
	            	st.setAdmno(rs.getString("admno"));
	            	st.setBatch(rs.getString("batch"));
	            	st.setPhone(rs.getString("phone"));
	            	st.setAddress(rs.getString("address"));
	            	st.setDob(rs.getString("dob"));
	            	st.setRoll_no(rs.getString("roll_no"));
	            	st.setAio_no(rs.getString("aio_no"));
	            	st.setImmediate_contact(rs.getString("immediate_contact"));
	            	st.setSession(rs.getString("session"));
	            	return st;
	            	    
	            }
			});
		  
		  return student_details;
	  }
	public List getBatchList()
	{
		List<String> batches=getJdbcTemplate().query("select distinct batch from Student_Details Order by batch asc", 
				new RowMapper<String>(){
            public String mapRow(ResultSet rs, int rowNum) 
                                         throws SQLException {
                    return rs.getString(1);
            }
		});
				return batches;
	}
	
	
	public Student_Details save(Student_Details st)
	 {
		return null;
	 }
	
	private JdbcTemplate getJdbcTemplate()
	{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	       dataSource.setDriverClassName(masterDbProperties.getDriverClassName());
	       dataSource.setUrl(DB_CONNECTION_STRING + request.getSession().getAttribute("USER_DB"));
	       dataSource.setUsername(masterDbProperties.getUsername());
	       dataSource.setPassword(masterDbProperties.getPassword());
	       
	       JdbcTemplate jdbcTemplates= new JdbcTemplate();
	       jdbcTemplates.setDataSource(dataSource);
	       return jdbcTemplates;
	}
}
