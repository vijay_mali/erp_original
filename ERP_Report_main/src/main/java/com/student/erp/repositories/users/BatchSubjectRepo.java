package com.student.erp.repositories.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.student.erp.MasterDBConfigProperties;
import com.student.erp.entities.BatchSubject;

@Repository("batchSubjectRepo")
public class BatchSubjectRepo{

	@Autowired
	MasterDBConfigProperties masterDbProperties;
	
	
	@Autowired
	private HttpServletRequest request;
	
	 @Value("${app.db.url}")
	 private String DB_CONNECTION_STRING;
	
//	@Query("SELECT distinct u FROM BatchSubject u where  u.batch=?1")
	public BatchSubject findListByBatchExam(String batch)
	{
	
		String sql="SELECT distinct subject  FROM batch_subject_order u where  u.batch='"+batch+"'";
		//System.out.println("QUERY>> "+sql);
		 List<BatchSubject> batchSubject=getJdbcTemplate().query(sql, 
					new RowMapper<BatchSubject>(){
	            public BatchSubject mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	BatchSubject st = new BatchSubject();
	            	st.setSubject(rs.getString("subject"));
	            
	            	return st;
	            	    
	            }
			});
		  
		 if(batchSubject.size()>0)
			 return batchSubject.get(0);
		 else
			 return null;
	}

	
	private JdbcTemplate getJdbcTemplate()
	{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	       dataSource.setDriverClassName(masterDbProperties.getDriverClassName());
	       dataSource.setUrl(DB_CONNECTION_STRING + request.getSession().getAttribute("USER_DB"));
	       dataSource.setUsername(masterDbProperties.getUsername());
	       dataSource.setPassword(masterDbProperties.getPassword());
	       
	       JdbcTemplate jdbcTemplates= new JdbcTemplate();
	       jdbcTemplates.setDataSource(dataSource);
	       return jdbcTemplates;
	}
	
}



