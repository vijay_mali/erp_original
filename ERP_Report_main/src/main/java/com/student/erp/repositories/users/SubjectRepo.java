package com.student.erp.repositories.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.student.erp.MasterDBConfigProperties;

@Repository("subjectRepo")
public class SubjectRepo{

	@Autowired
	MasterDBConfigProperties masterDbProperties;
	
	
	@Autowired
	private HttpServletRequest request;
	
	 @Value("${app.db.url}")
	 private String DB_CONNECTION_STRING;
	 
//	@Query("SELECT distinct u.subject_name FROM Subject u where u.batch=?1 Order by u.batch asc")
	public List<String> searchByBatch(String batch)
	{
		String sql="SELECT distinct u.subject_name FROM Subject u where u.batch='"+batch+"' Order by u.batch asc";
		List<String> batches=getJdbcTemplate().query(sql, new Object[]{},
				new RowMapper<String>(){
            public String mapRow(ResultSet rs, int rowNum) 
                                         throws SQLException {
                    return rs.getString(1);
            }
		});
				return batches;
	}
	
	private JdbcTemplate getJdbcTemplate()
	{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	       dataSource.setDriverClassName(masterDbProperties.getDriverClassName());
	       dataSource.setUrl(DB_CONNECTION_STRING + request.getSession().getAttribute("USER_DB"));
	       dataSource.setUsername(masterDbProperties.getUsername());
	       dataSource.setPassword(masterDbProperties.getPassword());
	       
	       JdbcTemplate jdbcTemplates= new JdbcTemplate();
	       jdbcTemplates.setDataSource(dataSource);
	       return jdbcTemplates;
	}
}
