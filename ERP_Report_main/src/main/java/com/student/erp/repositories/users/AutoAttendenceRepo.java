package com.student.erp.repositories.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.student.erp.MasterDBConfigProperties;
import com.student.erp.entities.Auto_Attendence;
import com.student.erp.entities.Student_Details;


@Repository("autoAttendenceRepo")
public class AutoAttendenceRepo{

	@Autowired
	MasterDBConfigProperties masterDbProperties;
	
	
	@Autowired
	private HttpServletRequest request;
	
	 @Value("${app.db.url}")
	 private String DB_CONNECTION_STRING;
	 
	@Transactional
	@Modifying
	//@Query("Update Auto_Attendence set start_date=?3, end_date=?4  where batch=?1 and exam=?2")
	public int update(String batch, String exam, String start_date, String end_date)
	{
		String sql="Update auto_attendence set start_date='"+start_date+"', end_date='"+end_date+"'  where batch='"+batch+"' and exam_name='"+exam+"'";
		return getJdbcTemplate().update(sql);
	}
	
//	@Query("Select u from  Auto_Attendence u WHERE u.batch=?1 and u.exam=?2")
	public Auto_Attendence findByBatchAndExam(String batch, String exam)
	{
		
		String sql="Select * from  auto_attendence u WHERE u.batch='"+batch+"'  and u.exam_name='"+exam+"'";
		String countSql="Select count(*) from  auto_attendence u WHERE u.batch='"+batch+"'  and u.exam_name='"+exam+"'";
		int total= getJdbcTemplate().queryForObject(countSql, Integer.class);
		System.out.println(total+" - COUNT AUTO ATTENDENCE: >> "+countSql);
		if(total == 0)
		{
			return null;
		}
		  List<Auto_Attendence> student_details=getJdbcTemplate().query(sql, 
					new RowMapper<Auto_Attendence>(){
	            public Auto_Attendence mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Auto_Attendence st = new Auto_Attendence();
	            	st.setBatch(rs.getString("batch"));
	            	st.setExam(rs.getString("exam"));
	            	st.setStart_date(rs.getString("start_date"));
	            	st.setEnd_date(rs.getString("end_date"));
	            	
	            	return st;
	            	    
	            }
			});
		  
		  return student_details.get(0);
	}
	
	//@Query("Select u from  Auto_Attendence u WHERE u.batch=?1")
	public Auto_Attendence findByBatch(String batch)
	{
		String sql="Select * from  auto_attendence u WHERE u.batch='"+batch+"'";
		  List<Auto_Attendence> student_details=getJdbcTemplate().query(sql, 
					new RowMapper<Auto_Attendence>(){
	            public Auto_Attendence mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Auto_Attendence st = new Auto_Attendence();
	            	st.setBatch(rs.getString("batch"));
	            	st.setExam(rs.getString("exam_name"));
	            	st.setStart_date(rs.getString("start_date"));
	            	st.setEnd_date(rs.getString("end_date"));
	            	
	            	return st;
	            	    
	            }
			});
		  
		  return student_details.get(0);
	}
	
//	@Query("Select u from  Auto_Attendence u WHERE u.batch=?1 order by u.id desc")
	public List<Auto_Attendence> findByBatchDesc(String batch)
	{
		String sql="Select * from  auto_attendence u WHERE u.batch='"+batch+"' order by u.id desc";
		System.out.println(">>>. QUERY: "+sql);
		  List<Auto_Attendence> student_details=getJdbcTemplate().query(sql, 
					new RowMapper<Auto_Attendence>(){
	            public Auto_Attendence mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Auto_Attendence st = new Auto_Attendence();
	            	st.setBatch(rs.getString("batch"));
	            	st.setExam(rs.getString("exam_name"));
	            	st.setStart_date(rs.getString("start_date"));
	            	st.setEnd_date(rs.getString("end_date"));
	            	
	            	return st;
	            	    
	            }
			});
		  
		  return student_details;
	}
	

	public Auto_Attendence save(Auto_Attendence autoAttendance) {
		String sql="insert into auto_attendence(batch,start_date,end_date, exam_name) values(?,?,?,?)";
		getJdbcTemplate().update(sql,new Object[]{autoAttendance.getBatch(),autoAttendance.getStart_date(),autoAttendance.getEnd_date(),autoAttendance.getExam()});
		return null;
	}

	private JdbcTemplate getJdbcTemplate()
	{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	       dataSource.setDriverClassName(masterDbProperties.getDriverClassName());
	       dataSource.setUrl(DB_CONNECTION_STRING + request.getSession().getAttribute("USER_DB"));
	       dataSource.setUsername(masterDbProperties.getUsername());
	       dataSource.setPassword(masterDbProperties.getPassword());
	       
	       JdbcTemplate jdbcTemplates= new JdbcTemplate();
	       jdbcTemplates.setDataSource(dataSource);
	       return jdbcTemplates;
	}

}
