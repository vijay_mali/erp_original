package com.student.erp.repositories.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.student.erp.MasterDBConfigProperties;
import com.student.erp.MyDelegatingDS;
import com.student.erp.entities.User;


@Repository
public class UserRepository {
	
	
//	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	MasterDBConfigProperties masterDbProperties;
	
	
	@Autowired
	private HttpServletRequest request;
	
	 @Value("${app.db.url}")
	 private String DB_CONNECTION_STRING;
	
	//@Query("Select u from User u where u.name=?1 and u.code=?2")
	@Transactional
    public User findByName(String username,String code) throws SQLException{
		System.out.println("REPO: "+request.getSession().getAttribute("USER_DB"));
	/*	jdbcTemplate.getDataSource().getConnection().setCatalog(request.getSession().getAttribute("USER_DB").toString());
		jdbcTemplate.query("select * from users", 
                new UserRowMapper());*/
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	       dataSource.setDriverClassName(masterDbProperties.getDriverClassName());
	       dataSource.setUrl(DB_CONNECTION_STRING + request.getSession().getAttribute("USER_DB"));
	       dataSource.setUsername(masterDbProperties.getUsername());
	       dataSource.setPassword(masterDbProperties.getPassword());
	       
	       JdbcTemplate jdbcTemplates= new JdbcTemplate();
	       jdbcTemplates.setDataSource(dataSource);
	       System.out.println("JDBC DONE");
	//	jdbcTemplate=  new JdbcTemplate(new MyDelegatingDS(request.getSession().getAttribute("USER_DB"), dataSource))
		List<User> users=jdbcTemplates.query("select * from user where username='"+username+"'", 
                new UserRowMapper());
		System.out.println("userlist"+users);
		if(users!=null && !users.isEmpty())
			
		{
			return users.get(0);

		}
		return null;
	}

	class UserRowMapper implements RowMapper<User>
	{
	    @Override
	    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
	        User user = new User();
	        user.setId(rs.getInt("user_id"));
	        user.setName(rs.getString("name"));
	        user.setPassword(rs.getString("password"));
	        user.setActive(1);
	        return user;
	    }
	}
}