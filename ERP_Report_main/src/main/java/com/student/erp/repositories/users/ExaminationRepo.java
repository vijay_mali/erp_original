package com.student.erp.repositories.users;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.student.erp.MasterDBConfigProperties;
import com.student.erp.entities.Examination;
import com.student.erp.entities.Student_Details;

@Repository("examinationRepo")
public class ExaminationRepo{

	@Autowired
	MasterDBConfigProperties masterDbProperties;
	
	
	@Autowired
	private HttpServletRequest request;
	
	 @Value("${app.db.url}")
	 private String DB_CONNECTION_STRING;
	 
	 public Page<Examination> findAll(Pageable page) 
	 {
		 
		 return null; 
	 }
//	@Query("SELECT distinct u.exam_name FROM Examination u where u.batch=?1 Order by u.batch asc")
	public List<Examination> searchByBatch(String batch)
	{
		String sql="SELECT distinct u.exam_name FROM Examination u where u.batch='"+batch+"'";
		  List<Examination> student_details=getJdbcTemplate().query(sql, new Object[]{},
					new RowMapper<Examination>(){
	            public Examination mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Examination st = new Examination();
	            	
	            	st.setExam_name(rs.getString("exam_name"));
	            	
	            	return st;
	            	    
	            }
			});
		  
		  return student_details;
	}
	
	
	//@Query(value="SELECT * FROM (SELECT distinct concat_ws('\n',a.exam_name,' '+ROUND(b.min)) exam_name FROM examination a, max_min_marks b where a.batch=?1 and a.exam_name=b.exam_name and a.batch=b.batch and a.exam_name in ('First Periodic','Second Periodic','First Terminal','Second Terminal') UNION SELECT distinct concat_ws('\n',a.exam_name,ROUND(b.max)) exam_name  FROM examination a,  max_min_marks b where a.batch=?1 and a.exam_name=b.exam_name  and a.batch=b.batch  and a.exam_name in ('First Periodic','Second Periodic','First Terminal','Second Terminal')) c order by c.exam_name",nativeQuery=true)
	//@Query(value="SELECT * FROM (SELECT distinct concat_ws('\n',a.exam_name,ROUND(b.max)) exam_name  FROM examination a,  max_min_marks b where a.batch=?1 and a.exam_name=b.exam_name  and a.batch=b.batch  and a.exam_name in ('First Periodic','Second Periodic','First Terminal','Second Terminal','Final Exam')) c order by c.exam_name",nativeQuery=true)
	public List<String> getExaminationType(String batch)
	{
		String sql="SELECT * FROM (SELECT distinct concat_ws('\n',a.exam_name,ROUND(b.max)) exam_name  FROM examination a,  max_min_marks b where a.batch='"+batch+"' and a.exam_name=b.exam_name  and a.batch=b.batch  and a.exam_name in ('First Periodic','Second Periodic','First Terminal','Second Terminal','Final Exam')) c order by c.exam_name";
		  List<String> student_details=getJdbcTemplate().query(sql, new Object[]{},
					new RowMapper<String>(){
	            public String mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	
	            	
	            	return rs.getString(1);
	            	    
	            }
			});
		  
		  return student_details;
	}
	
	
	
	/*@Query(value="SELECT * FROM (SELECT distinct a.exam_name exam_name FROM examination a, max_min_marks b where a.batch=?1 and a.exam_name=b.exam_name and a.batch=b.batch and a.exam_name not in ('skills','annual exam G2') UNION SELECT distinct concat_ws('\n',a.exam_name,b.max) exam_name  FROM examination a,  max_min_marks b where a.batch=?1 and a.exam_name=b.exam_name  and a.batch=b.batch  and a.exam_name not in ('skills','annual exam G2')) c order by c.exam_name",nativeQuery=true)
	public List<String> getExaminationTypeAnnualG2(String batch);*/
	
	private JdbcTemplate getJdbcTemplate()
	{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	       dataSource.setDriverClassName(masterDbProperties.getDriverClassName());
	       dataSource.setUrl(DB_CONNECTION_STRING + request.getSession().getAttribute("USER_DB"));
	       dataSource.setUsername(masterDbProperties.getUsername());
	       dataSource.setPassword(masterDbProperties.getPassword());
	       
	       JdbcTemplate jdbcTemplates= new JdbcTemplate();
	       jdbcTemplates.setDataSource(dataSource);
	       return jdbcTemplates;
	}
	
	
	public List<String> getExaminationTypeWithDynamicExamList(String batch, List<String> dynamicExamListString) {
		//System.out.println("dynamicExamListString"+dynamicExamListString);
		String dynamicExamStirng="";
		 for(String str :  dynamicExamListString)
		 {
			 dynamicExamStirng+="'"+str+"'";
			 dynamicExamStirng+=",";
		 }
		 System.out.println("dynamicExamStirng"+dynamicExamStirng);
		String sql="SELECT * FROM (SELECT distinct concat_ws('\n',a.exam_name,ROUND(b.max)) exam_name  FROM examination a,  max_min_marks b where a.batch='"+batch+"' and a.exam_name=b.exam_name  and a.batch=b.batch  and a.exam_name in ("+dynamicExamStirng.substring(0, dynamicExamStirng.length()-1)+")) c ";
		//String sql="SELECT * FROM (SELECT distinct concat_ws('\n',a.exam_name,ROUND(b.max)) exam_name  FROM examination a,  max_min_marks b where a.batch='"+batch+"' and a.exam_name=b.exam_name  and a.batch=b.batch  and a.exam_name in ('First Periodic','Second Periodic','First Terminal','Second Terminal','Final Exam','Second Periodic - G2')) c order by c.exam_name";

		List<String> student_details=getJdbcTemplate().query(sql, new Object[]{},
					new RowMapper<String>(){
	            public String mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	
	            	
	            	return rs.getString(1);
	            	    
	            }
			});
		  
		  return student_details;
	}
	
	public List<String> getExaminationTypeWithDynamicExamListWithoutmaxMarks(String batch, List<String> dynamicExamListString) {
		//System.out.println("dynamicExamListString"+dynamicExamListString);
		String dynamicExamStirng="";
		 for(String str :  dynamicExamListString)
		 {
			 dynamicExamStirng+="'"+str+"'";
			 dynamicExamStirng+=",";
		 }
			String sql="SELECT * FROM (SELECT distinct a.exam_name   FROM examination a,  max_min_marks b where a.batch='"+batch+"' and a.exam_name=b.exam_name  and a.batch=b.batch  and a.exam_name in ("+dynamicExamStirng.substring(0, dynamicExamStirng.length()-1)+")) c ";

		//String sql="SELECT * FROM (SELECT distinct concat_ws('\n',a.exam_name,ROUND(b.max)) exam_name  FROM examination a,  max_min_marks b where a.batch='"+batch+"' and a.exam_name=b.exam_name  and a.batch=b.batch  and a.exam_name in ('First Periodic','Second Periodic','First Terminal','Second Terminal','Final Exam','Second Periodic - G2')) c order by c.exam_name";

		List<String> student_details=getJdbcTemplate().query(sql, new Object[]{},
					new RowMapper<String>(){
	            public String mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	
	            	
	            	return rs.getString(1);
	            	    
	            }
			});
		  
		  return student_details;
	}
}


