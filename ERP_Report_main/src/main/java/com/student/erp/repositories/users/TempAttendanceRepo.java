package com.student.erp.repositories.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.student.erp.MasterDBConfigProperties;
import com.student.erp.entities.Temp_Attendance;


@Repository("tempAttendanceRepo")
public class TempAttendanceRepo{

	@Autowired
	MasterDBConfigProperties masterDbProperties;
	
	
	@Autowired
	private HttpServletRequest request;
	
	 @Value("${app.db.url}")
	 private String DB_CONNECTION_STRING;
	
	public Temp_Attendance findByAdmno(String admno)
	{
		String sql="Select distinct * from temp_attendence u  where u.admno='"+admno+"'";
		  List<Temp_Attendance> student_details=getJdbcTemplate().query(sql, 
					new RowMapper<Temp_Attendance>(){
	            public Temp_Attendance mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Temp_Attendance st = new Temp_Attendance();
	            	
	            	st.setBatch(rs.getString("batch"));
	            	st.setAdmno(rs.getString("admno"));
	            	st.setTot_present(rs.getString("tot_present"));
	            	st.setWorking_days(rs.getString("working_days"));
	            	
	            	return st;
	            	    
	            }
			});
		  
		  return student_details.get(0);
	}
	
	public Temp_Attendance findByAdmnoAndExam(String admno,String exam)
	{
		String sql="Select distinct * from temp_attendence u  where u.exam_name='"+exam+"'  and u.admno='"+admno+"'";
		  List<Temp_Attendance> student_details=getJdbcTemplate().query(sql, 
					new RowMapper<Temp_Attendance>(){
	            public Temp_Attendance mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Temp_Attendance st = new Temp_Attendance();
	            	
	            	st.setBatch(rs.getString("batch"));
	            	st.setAdmno(rs.getString("admno"));
	            	st.setTot_present(rs.getString("tot_present"));
	            	st.setWorking_days(rs.getString("working_days"));
	            	
	            	return st;
	            	    
	            }
			});
		  
		  return student_details.get(0);
	}
	
//	@Query("Select distinct u from Temp_Attendance u  where u.exam=?2 and u.batch=?1 and u.admno=?3")
	public List<Temp_Attendance> getManualAttendanceWokringDays(String batch,String exam,String admno)
	{
		String sql="Select distinct * from temp_attendence u  where u.exam_name='"+exam+"' and u.batch='"+batch+"' and u.admno='"+admno+"'";
		System.out.println("SELECT QUERY: "+sql);
		  List<Temp_Attendance> student_details=getJdbcTemplate().query(sql, 
					new RowMapper<Temp_Attendance>(){
	            public Temp_Attendance mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Temp_Attendance st = new Temp_Attendance();
	            	
	            	st.setBatch(rs.getString("batch"));
	            	st.setAdmno(rs.getString("admno"));
	            	st.setTot_present(rs.getString("tot_present"));
	            	st.setWorking_days(rs.getString("working_days"));
	            	System.out.println("SELECT QUERY: "+rs.getString("tot_present"));
	            	return st;
	            	    
	            }
			});
		  
		  return student_details;
	}
	
//	@Query("Select distinct u from Temp_Attendance u  where  u.batch=?1 and u.admno=?2")
	public List<Temp_Attendance> getManualAttendanceWokringDaysByAdmno(String batch,String admno)
	{
		String sql="Select distinct * from temp_attendence u  where  u.batch='"+batch+"' and u.admno='"+admno+"'";
		  List<Temp_Attendance> student_details=getJdbcTemplate().query(sql, 
					new RowMapper<Temp_Attendance>(){
	            public Temp_Attendance mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Temp_Attendance st = new Temp_Attendance();
	            	
	            	st.setBatch(rs.getString("batch"));
	            	st.setAdmno(rs.getString("admno"));
	            	st.setTot_present(rs.getString("tot_present"));
	            	st.setWorking_days(rs.getString("working_days"));
	            	
	            	return st;
	            	    
	            }
			});
		  
		  return student_details;
	}
	
	@Transactional
	@Modifying
//	@Query("Update Temp_Attendance set working_days=?3 where exam=?2 and batch=?1")
	public int updateTempWorkingDays(String batch, String exam, String working_days)
	{
		String sql="Update temp_attendence set working_days='"+working_days+"' where exam_name='"+exam+"' and batch='"+batch+"'";
		return getJdbcTemplate().update(sql);
	}
	
	@Transactional
	@Modifying
	//@Query("Update Temp_Attendance set tot_present=?2 where admno=?1")
	public int updateData(String admno, String attendance)
	{
		String sql="Update temp_attendence set tot_present='"+attendance+"' where admno='"+admno+"'";
		System.out.println("UPDATE QUERY: "+sql);
		return getJdbcTemplate().update(sql);
	}
	
	private JdbcTemplate getJdbcTemplate()
	{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	       dataSource.setDriverClassName(masterDbProperties.getDriverClassName());
	       dataSource.setUrl(DB_CONNECTION_STRING + request.getSession().getAttribute("USER_DB"));
	       dataSource.setUsername(masterDbProperties.getUsername());
	       dataSource.setPassword(masterDbProperties.getPassword());
	       
	       JdbcTemplate jdbcTemplates= new JdbcTemplate();
	       jdbcTemplates.setDataSource(dataSource);
	       return jdbcTemplates;
	}
}
