package com.student.erp.repositories.users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.student.erp.entities.Exam_Formula;

@Repository("exam_formula_repo")
public interface ExamFormulaRepo extends JpaRepository<Exam_Formula, Long>{

}
