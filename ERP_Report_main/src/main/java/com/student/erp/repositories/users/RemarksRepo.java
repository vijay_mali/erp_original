package com.student.erp.repositories.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.student.erp.MasterDBConfigProperties;
import com.student.erp.entities.Remarks;

@Repository("remarksRepo")
public class RemarksRepo{

	@Autowired
	MasterDBConfigProperties masterDbProperties;
	
	
	@Autowired
	private HttpServletRequest request;
	
	 @Value("${app.db.url}")
	 private String DB_CONNECTION_STRING;
	
//	@Query("Update Remarks set value=?1  where student_id=?2 ")
	public Remarks update(String value, String student_id)
	{
		return null;
	}

//	@Query("Select u from  Remarks u where student_id=?1 ")
	public Remarks findBystudent_id(String student_id)
	{
		String sql="SELECT * FROM remarks u where u.student_id='"+student_id+"'";
		  List<Remarks> remarks=getJdbcTemplate().query(sql, 
					new RowMapper<Remarks>(){
	            public Remarks mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Remarks st = new Remarks();
	            	st.setLabel(rs.getString("label"));
	            	st.setValue(rs.getString("value"));
	            	
	            	
	            	return st;
	            	    
	            }
			});
		  
		  return remarks.get(0);
	}
	
	
//	@Query("Select u from  Remarks u where student_id=?1 and u.label in ('overall_remark','Result') ")
	public List<Remarks> searchByStudent(String student_id)
	{
		String sql="SELECT * FROM remarks u where u.student_id='"+student_id+"'  and u.label in ('overall_remark','Result') ";
		  List<Remarks> remarks=getJdbcTemplate().query(sql, 
					new RowMapper<Remarks>(){
	            public Remarks mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Remarks st = new Remarks();
	            	st.setLabel(rs.getString("label"));
	            	st.setValue(rs.getString("value"));
	            	
	            	
	            	return st;
	            	    
	            }
			});
		  
		  return remarks;
	}
	
	
//	@Query("Select u from  Remarks u where student_id=?1 ")
	public List<Remarks> findRemarksByStudentId(String student_id)
	{
		String sql="SELECT * FROM remarks u where u.student_id='"+student_id+"'";
		  List<Remarks> remarks=getJdbcTemplate().query(sql, 
					new RowMapper<Remarks>(){
	            public Remarks mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Remarks st = new Remarks();
	            	st.setLabel(rs.getString("label"));
	            	st.setValue(rs.getString("value"));
	            	
	            	
	            	return st;
	            	    
	            }
			});
		  
		  return remarks;
	}
	
	
//	@Query("Select u from  Remarks u where student_id=?1 and examtype=?2 and label=?3")
	public List<Remarks> findResultByStudentId(String student_id,String examType, String label)
	{
		String sql="SELECT * FROM remarks u where u.student_id='"+student_id+"' and examtype='"+examType+"' and label='"+label+"'";
		  List<Remarks> remarks=getJdbcTemplate().query(sql, 
					new RowMapper<Remarks>(){
	            public Remarks mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Remarks st = new Remarks();
	            	st.setLabel(rs.getString("label"));
	            	st.setValue(rs.getString("value"));
	            	st.setExamtype(rs.getString("examtype"));
	            	
	            	
	            	return st;
	            	    
	            }
			});
		  
		  return remarks;
	}
	
	@Transactional
	@Modifying
//	@Query("Update Remarks set value=?3, label=?2  where student_id=?1 ")
	public int updateData(String student_id, String label,String value)
	{
		return 0;
	}
	
	@Transactional
	@Modifying
//	@Query("Update Remarks set value=?3 where student_id=?1 and  label=?2 ")
	public int updateRemarksData(String student_id, String label,String value)
	{
		String sql="Update remarks set value='"+value+"' where student_id='"+student_id+"' and  label='"+label+"'";
		System.out.println("UPDATE QUERY: "+sql);
		return getJdbcTemplate().update(sql);
	}
	
	

	@Transactional
	@Modifying
	//@Query("Update Remarks set value=?3  where student_id=?1 and examtype=?4 and label=?2 ")
	public int updateResultData(String student_id, String label,String value,String exam)
	{
		String sql="Update remarks set value='"+value+"' where student_id='"+student_id+"' and examtype='"+exam+"' and  label='"+label+"'";
		System.out.println("UPDATE QUERY: "+sql);
		return getJdbcTemplate().update(sql);
	}
	
	
	private JdbcTemplate getJdbcTemplate()
	{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	       dataSource.setDriverClassName(masterDbProperties.getDriverClassName());
	       dataSource.setUrl(DB_CONNECTION_STRING + request.getSession().getAttribute("USER_DB"));
	       dataSource.setUsername(masterDbProperties.getUsername());
	       dataSource.setPassword(masterDbProperties.getPassword());
	       
	       JdbcTemplate jdbcTemplates= new JdbcTemplate();
	       jdbcTemplates.setDataSource(dataSource);
	       return jdbcTemplates;
	}
}
