package com.student.erp.repositories.users;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.student.erp.MasterDBConfigProperties;
import com.student.erp.entities.Max_Min_Marks;

@Repository("maxMinRepo")
public class Max_Min_Repo{
	@Autowired
	MasterDBConfigProperties masterDbProperties;
	
	
	@Autowired
	private HttpServletRequest request;
	
	 @Value("${app.db.url}")
	 private String DB_CONNECTION_STRING;
	
	//@Query("SELECT distinct u.max FROM Max_Min_Marks u where u.exam_name=?1 and u.batch=?2")
	public String getMaxMarksByExam(String exam_name,String batch)
	{
		String sql="SELECT distinct u.max FROM Max_Min_Marks u where u.exam_name='"+exam_name+"' and u.batch='"+batch+"'";
		String countSql="SELECT distinct count(u.max) FROM Max_Min_Marks u where u.exam_name='"+exam_name+"' and u.batch='"+batch+"'";
		long total =getJdbcTemplate().queryForObject(
			 	countSql,Long.class
                );
		if(total ==0)
		{
			return null;
		}
		String result=getJdbcTemplate().queryForObject(sql, new Object[]{},String.class);
		return result;
	}
	
	private JdbcTemplate getJdbcTemplate()
	{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	       dataSource.setDriverClassName(masterDbProperties.getDriverClassName());
	       dataSource.setUrl(DB_CONNECTION_STRING + request.getSession().getAttribute("USER_DB"));
	       dataSource.setUsername(masterDbProperties.getUsername());
	       dataSource.setPassword(masterDbProperties.getPassword());
	       
	       JdbcTemplate jdbcTemplates= new JdbcTemplate();
	       jdbcTemplates.setDataSource(dataSource);
	       return jdbcTemplates;
	}
}
