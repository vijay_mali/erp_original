package com.student.erp.repositories.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.student.erp.MasterDBConfigProperties;
import com.student.erp.entities.Marks;

@Repository("marksRepo")
public class MarksRepo{

	@Autowired
	MasterDBConfigProperties masterDbProperties;
	
	
	@Autowired
	private HttpServletRequest request;
	
	 @Value("${app.db.url}")
	 private String DB_CONNECTION_STRING;
	 
	//@Query("SELECT distinct u.subject FROM Marks u where u.batch=?1 and u.exam  in ('First periodic','Second Periodic','First Terminal','Second Terminal','Final Exam') Order by u.subject asc")
//	@Query("SELECT distinct u.subject FROM Marks u where u.batch=?1 and u.exam  in ?2 Order by u.subject asc")
	public List<String> searchByBatch(String batch, List<String> examList )
	{
		/*MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("examLists", examList);*/
		String examlists="";
		 for(String str :  examList)
		 {
			 examlists+="\""+str+"\"";
			 examlists+=",";
		 }
		 
		String sql="SELECT distinct u.subject FROM Marks u where u.batch='"+batch+"' and u.exam  in ("+examlists.substring(0, examlists.length()-1)+") Order by u.subject asc";
		List<String> exams=getJdbcTemplate().query(sql, new Object[]{},
				new RowMapper<String>(){
            public String mapRow(ResultSet rs, int rowNum) 
                                         throws SQLException {
                    return rs.getString(1);
            }
		});
				return exams;
	}
	
//	@Query("SELECT distinct u.subject FROM Marks u where u.batch=?1 and u.exam  in ('First Periodic - skills','Second Periodic - skills','Second Terminal - skills','First Terminal - skills','Final Exam - skills') Order by u.subject asc")
	public List<String> searchSkillsByBatch(String batch)
	{
		String sql="SELECT distinct u.subject FROM Marks u where u.batch='"+batch+"' and u.exam  in ('First Periodic - skills','Second Periodic - skills','Second Terminal - skills','First Terminal - skills','Final Exam - skills') Order by u.subject asc";
		List<String> exams=getJdbcTemplate().query(sql, new Object[]{},
				new RowMapper<String>(){
            public String mapRow(ResultSet rs, int rowNum) 
                                         throws SQLException {
                    return rs.getString(1);
            }
		});
				return exams;
	}
	
//	@Query("SELECT distinct u.subject FROM Marks u where u.batch=?1 and u.exam  in ('First Periodic - G2','First Terminal - G2','Second Periodic - G2','Second Terminal - G2','Final Exam - G2') Order by u.subject asc")
	public List<String> searchAnnulaExamg2ByBatch(String batch)
	{
		String sql="SELECT distinct u.subject FROM Marks u where u.batch='"+batch+"' and u.exam  in ('First Periodic - G2','First Terminal - G2','Second Periodic - G2','Second Terminal - G2','Final Exam - G2') Order by u.subject asc";
		List<String> exams=getJdbcTemplate().query(sql, new Object[]{},
				new RowMapper<String>(){
            public String mapRow(ResultSet rs, int rowNum) 
                                         throws SQLException {
                    return rs.getString(1);
            }
		});
				return exams;
	}
	
	
//	@Query("SELECT a.student_id,u.subject,u.exam, u.marks,u.grade FROM Marks u, Student_Details a where u.batch=?1 and u.exam  in ?3 and a.admno=u.admnno and a.batch=u.batch and a.admno=?2 Order by u.subject asc")
	public List<Map<String, Object>> getSubjectMarksByBatch(String batch,String admno, List<String> examList)
	{
		//System.out.println(">>>>>>>>>>"+examList);
		 String examlists="";
		 for(String str :  examList)
		 {
			 examlists+="'"+str+"'";
			 examlists+=",";
		 }
		 System.out.println("examList"+examlists);
		String sql="SELECT a.student_id,u.subject,u.exam, u.marks,u.grade FROM Marks u, Student_Details a where u.batch='"+batch+"' and u.exam  in ("+examlists.substring(0, examlists.length()-1)+") and a.admno=u.admno and a.batch=u.batch and a.admno='"+admno+"' Order by u.subject asc";
		List<Map<String, Object>> result= getJdbcTemplate().queryForList(sql, new Object[]{});
		return result;
	}
	
	
	
//	@Query(value="SELECT * FROM (SELECT a.admno, concat_ws('\n',a.exam_name,' '+ROUND(b.min)) exam_name,a.subject,a.marks,a.grade FROM marks a, max_min_marks b where a.batch=?1 and a.exam_name=b.exam_name and a.batch=b.batch and a.exam_name in ('First Periodic','Second Periodic','First Terminal','Second Terminal') UNION SELECT distinct concat_ws('\n',a.exam_name,ROUND(b.max)) exam_name  FROM marks a,  max_min_marks b where a.batch=?1 and a.exam_name=b.exam_name  and a.batch=b.batch  and a.exam_name in ('First Periodic','Second Periodic','First Terminal','Second Terminal')) c order by c.exam_name",nativeQuery=true)
	public List<Map<String, Object>> getExaminationDetails(String batch,String admno, String subject)
	{
		String sql="SELECT * FROM (SELECT a.admno, concat_ws('\n',a.exam_name,' '+ROUND(b.min)) exam_name,a.subject,a.marks,a.grade FROM marks a, max_min_marks b where a.batch=?1 and a.exam_name=b.exam_name and a.batch=b.batch and a.exam_name in ('First Periodic','Second Periodic','First Terminal','Second Terminal') UNION SELECT distinct concat_ws('\n',a.exam_name,ROUND(b.max)) exam_name  FROM marks a,  max_min_marks b where a.batch=?1 and a.exam_name=b.exam_name  and a.batch=b.batch  and a.exam_name in ('First Periodic','Second Periodic','First Terminal','Second Terminal')) c order by c.exam_name";
		List<Map<String, Object>> result=getJdbcTemplate().queryForList(sql, new Object[]{batch,admno,subject});
		return result;
	}
	
	
//	@Query(value="select a.subject,a.admno, a.batch,  group_concat(case when a.exam='First Periodic' then a.marks end) FirstPeriodic,group_concat(case when a.exam='Second Periodic' then a.marks end) SecondPeriodic,group_concat(case when exam='First Terminal' then marks end) FirstTerminal,group_concat(case when exam='Second Terminal' then marks end) SecondTerminal,group_concat(case when exam='Final Exam' then marks end) FinalExam, group_concat(case when exam='Final Exam' then marks end)/2 FinalExam50,a.grade from marks a where a.admno=?1 and a.batch=?2 and a.exam not in ('First Periodic - skills','Second Periodic - skills','Second Terminal - skills','First Terminal - skills','Final Exam - skills','First Periodic - G2','First Terminal - G2','Second Periodic - G2','Second Terminal - G2','Final Exam - G2') group by subject",nativeQuery=true)
	public List<Map<String, Object>> getFullSubjectDetails(String admno,String batch)
	{
		String sql="select a.subject,a.admno, a.batch,  group_concat(case when a.exam='First Periodic' then a.marks end) FirstPeriodic,group_concat(case when a.exam='Second Periodic' then a.marks end) SecondPeriodic,group_concat(case when exam='First Terminal' then marks end) FirstTerminal,group_concat(case when exam='Second Terminal' then marks end) SecondTerminal,group_concat(case when exam='Final Exam' then marks end) FinalExam, group_concat(case when exam='Final Exam' then marks end)/2 FinalExam50,a.grade from marks a where a.admno='"+admno+"' and a.batch='"+batch+"' and a.exam not in ('First Periodic - skills','Second Periodic - skills','Second Terminal - skills','First Terminal - skills','Final Exam - skills','First Periodic - G2','First Terminal - G2','Second Periodic - G2','Second Terminal - G2','Final Exam - G2') group by subject";
		List<Map<String, Object>> result=getJdbcTemplate().queryForList(sql, new Object[]{});
		return result;
	}
	
	
//	@Query("SELECT distinct u.exam FROM Marks u Order by u.exam asc")
	public List<String> listExam()
	{
		String sql="SELECT distinct u.exam FROM Marks u Order by u.exam asc";
		List<String> exams=getJdbcTemplate().query(sql, new Object[]{},
				new RowMapper<String>(){
            public String mapRow(ResultSet rs, int rowNum) 
                                         throws SQLException {
                    return rs.getString(1);
            }
		});
				return exams;
	}
	
	//@Query("SELECT distinct u.admnno FROM Marks u where  u.batch=?1 and u.exam=?2")
	public List<String> findListByBatchExam(String batch,String exam)
	{
		String sql="SELECT distinct u.admno FROM marks u where  u.batch='"+batch+"' and u.exam='"+exam+"'";
		List<String> batches=getJdbcTemplate().query(sql, new Object[]{},
				new RowMapper<String>(){
            public String mapRow(ResultSet rs, int rowNum) 
                                         throws SQLException {
                    return rs.getString(1);
            }
		});
				return batches;
	}

	
	//@Query("SELECT distinct u.admnno FROM Marks u where  u.batch=?1")
	public List<String> findListByBatch(String batch)
	{
		String sql="SELECT distinct u.admno FROM Marks u where  u.batch='"+batch+"'";
		List<String> batches=getJdbcTemplate().query(sql, new Object[]{},
				new RowMapper<String>(){
            public String mapRow(ResultSet rs, int rowNum) 
                                         throws SQLException {
                    return rs.getString(1);
            }
		});
				return batches;
	}
	
//	@Query(value="select distinct a.student_id, a.student_name,a.admno, c.tot_present, c.working_days from student_details a , temp_attendence c where a.admno=c.admno",nativeQuery=true)
	public List<Map<String, Object>> getStudentMarksManualAll()
	{
		String sql="select distinct a.student_id as id, a.student_name as name,a.admno, c.tot_present, c.working_days from student_details a , temp_attendence c where a.admno=c.admno";
		List<Map<String, Object>> result= getJdbcTemplate().queryForList(sql, new Object[]{});
		return result;
	}
	
	
	//@Query(value="select distinct a.student_id, a.student_name,a.admno, c.tot_present, c.working_days from student_details a , temp_attendence c where a.admno=c.admno and a.batch=?1",nativeQuery=true)
	public List<Map<String, Object>> getStudentMarksManualByBatch(String batch)
	{
		String sql="select distinct a.student_id as id, a.student_name as name,a.admno, c.tot_present, c.working_days from student_details a , temp_attendence c where a.admno=c.admno and a.batch='"+batch+"'";
		List<Map<String, Object>> result= getJdbcTemplate().queryForList(sql, new Object[]{});
		return result;
	}
	
	
	
	
	//@Query("SELECT u FROM Marks u where u.batch=?1 and u.admnno=?2 and u.exam=?3 and u.exam  in ('First Periodic - skills','Second Periodic - skills','Second Terminal - skills','First Terminal - skills','Final Exam - skills') Order by u.subject asc")
	//@Query("SELECT u FROM Marks u where u.batch=?1 and u.admnno=?2 and u.exam=?3 and u.subject=?4 Order by u.subject asc")
	public List<Marks> findSkillsSubjectMark(String batch,String admno,String exam, String subject)
	{
		String sql="SELECT * FROM Marks u where u.batch='"+batch+"' and u.admno='"+admno+"' and u.exam='"+exam+"' and u.subject='"+subject+"' Order by u.subject asc";
		List<Marks> marks=getJdbcTemplate().query(sql,new Object[]{},new BeanPropertyRowMapper<>(Marks.class));
		return marks;
	}
	
	
	
	
	//@Query("select sum(a.marks) as b,a.admnno  from Marks a  where a.batch=?1 and a.exam=?2 group by a.admnno order by b desc")
	public List<Map<String, Object>> getStudentRankInClass(String batch,String exam)
	{
		String sql="select sum(a.marks) as total,a.admno  from Marks a  where a.batch='"+batch+"' and a.exam='"+exam+"' group by a.admno order by a.batch desc";
		List<Map<String, Object>> result= getJdbcTemplate().queryForList(sql, new Object[]{});
		return result;
	}
	
	
	public List<Object> getAllStudentsTotalMarks(String batch, String exam)
	{
		String sql="select sum(a.b) from(select sum(marks) as b from marks where batch='"+batch+"' and exam='"+exam+"' group by admno) as a";
		System.out.println("QUERY>> "+sql);
		double result= getJdbcTemplate().queryForObject(sql, new Object[]{},Double.class);
		List<Object> list=  new ArrayList<>();
		list.add(result);
		return list;
	}
	
	
	 public Page<Marks> findAll(Pageable page)
	 {
		 String sql="SELECT * FROM Marks u";
		 String countSql="SELECT count(1) FROM Marks u";
		 long total =getJdbcTemplate().queryForObject(
				 	countSql,Long.class
	                );
		  List<Marks> student_details=getJdbcTemplate().query(sql, 
					new RowMapper<Marks>(){
	            public Marks mapRow(ResultSet rs, int rowNum) 
	                                         throws SQLException {
	            	Marks st = new Marks();
	            	st.setId(rs.getInt("id"));
	            	st.setExam(rs.getString("exam"));
	            	st.setAdmnno(rs.getString("admno"));
	            	
	            	return st;
	            	    
	            }
			});
		  
		  return new PageImpl<>(student_details, page, total);
	 }
	 

	private JdbcTemplate getJdbcTemplate()
	{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	       dataSource.setDriverClassName(masterDbProperties.getDriverClassName());
	       dataSource.setUrl(DB_CONNECTION_STRING + request.getSession().getAttribute("USER_DB"));
	       dataSource.setUsername(masterDbProperties.getUsername());
	       dataSource.setPassword(masterDbProperties.getPassword());
	       
	       JdbcTemplate jdbcTemplates= new JdbcTemplate();
	       jdbcTemplates.setDataSource(dataSource);
	       return jdbcTemplates;
	}

	public List<Map<String, Object>> getStudentRankInClassForFullExam(String batch,
			List<String> examList) {
		//System.out.println(">>>>>>>>>>"+examList);
		 String examlists="";
		 for(String str :  examList)
		 {
			 examlists+="'"+str+"'";
			 examlists+=",";
		 }
		 String sql="select sum(a.marks) as total,a.admno  from Marks a  where a.batch='"+batch+"' and a.exam  in ("+examlists.substring(0, examlists.length()-1)+") group by a.admno order by a.batch desc";
			List<Map<String, Object>> result= getJdbcTemplate().queryForList(sql, new Object[]{});
			return result;
		
	}
	

	
	
}



