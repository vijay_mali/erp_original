package com.student.erp.repositories.users;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.student.erp.MasterDBConfigProperties;

@Repository("holidayRepo")
public class HolidayRepo {
	
	@Autowired
	MasterDBConfigProperties masterDbProperties;
	
	
	@Autowired
	private HttpServletRequest request;
	
	 @Value("${app.db.url}")
	 private String DB_CONNECTION_STRING;
	 
//	@Query("Select count(u.id) from Holiday u where u.hdate between ?1 and ?2")
	public Long getCountByDate(String startDate, String endDate)
	{
		String sql="Select count(u.id) from holiday u where u.h_date between '"+startDate+"' and '"+endDate+"'";
		long count=getJdbcTemplate().queryForObject(sql, Long.class);
		return count;
	}

	
	private JdbcTemplate getJdbcTemplate()
	{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	       dataSource.setDriverClassName(masterDbProperties.getDriverClassName());
	       dataSource.setUrl(DB_CONNECTION_STRING + request.getSession().getAttribute("USER_DB"));
	       dataSource.setUsername(masterDbProperties.getUsername());
	       dataSource.setPassword(masterDbProperties.getPassword());
	       
	       JdbcTemplate jdbcTemplates= new JdbcTemplate();
	       jdbcTemplates.setDataSource(dataSource);
	       return jdbcTemplates;
	}
}

