package com.student.erp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//@ComponentScan(basePackages ={"com.student.erp.configuration.master","com.student.erp.config","com.student.erp.configuration.tenant"})
@ComponentScan(basePackages ={"com.student.erp"}/*, excludeFilters={
		  @ComponentScan.Filter(type=FilterType.ASSIGNABLE_TYPE, value=DataSourceConfig.class)}*/)
@SpringBootApplication
//@SpringBootApplication
@EnableJpaRepositories
@Configuration
public class ERPApplication extends SpringBootServletInitializer{

	
	 @Override
	    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	        return application.sources(ERPApplication.class);
	    }
	 
	public static void main(String[] args) {
		SpringApplication.run(ERPApplication.class, args);
	}

}
