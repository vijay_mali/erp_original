package com.student.erp.master.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.student.erp.master.entities.SchoolCode;

@Repository
public interface CodeRepository  extends JpaRepository<SchoolCode, Long>{
	
	@Query("SELECT u FROM SchoolCode u WHERE u.school_code=?1")
	public SchoolCode findbySchoolCode(String code);

}
