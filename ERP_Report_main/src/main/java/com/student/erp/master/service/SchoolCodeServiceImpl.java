package com.student.erp.master.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.student.erp.master.entities.SchoolCode;
import com.student.erp.master.repositories.CodeRepository;

@Service
public class SchoolCodeServiceImpl implements SchoolCodeService{

	@Autowired
	private CodeRepository codeRepo;
	
	@Override
	public SchoolCode findbySchoolCode(String code) {
		return codeRepo.findbySchoolCode(code);
	}

	
	
}
