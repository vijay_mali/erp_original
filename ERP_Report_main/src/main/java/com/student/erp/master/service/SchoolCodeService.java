package com.student.erp.master.service;

import com.student.erp.master.entities.SchoolCode;

public interface SchoolCodeService {

	public SchoolCode findbySchoolCode(String code);
	
}
