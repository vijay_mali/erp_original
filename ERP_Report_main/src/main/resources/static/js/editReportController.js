var app=angular.module('editReport', ['datatables']);

          
app.controller('editReportCtrl',['$scope', '$http','DTOptionsBuilder','$window', function ($scope, $http,DTOptionsBuilder,$window) {
	 /*$('#multiselect').multiselect({
		    buttonWidth : '160px',
		    includeSelectAllOption : true,
				nonSelectedText: 'Select an Option'
		  });*/
	 
	$scope.enableDate=function(){
		$( "#startdatepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
		$( "#enddatepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
	}
	$scope.dtOptions = DTOptionsBuilder.newOptions()
    .withDisplayLength(10)
    .withOption('bLengthChange', true)
    .withOption('processing', true)
     .withPaginationType('full_numbers');
	$scope.dataTableOpt = {
			   //custom datatable options
			  // or load data through ajax call also
			  "aLengthMenu": [[1,10, 50, 100,-1], [1,10, 50, 100,'All']],
			  };

	$scope.remarksAvbl='0';
	$scope.batchinfo=0;
	$scope.msg='';
	$scope.loading=0;
	$scope.screen='hide';
	
	$scope.reports={};
	$scope.attendance='manualattendance';
	$scope.fn_getReportList=function()
	{
		$scope.url='erp/api/getEdit?attendance='+$scope.attendance;
		if($scope.attendance && $scope.attendance=='autoattendance')
		{
			if($('#startdatepicker').val().length<1 || $('#startdatepicker').val().length<1)
			{
				//alert("Select date");
			}
			else
			{
				console.log($('#startdatepicker').val());
				console.log($('#startdatepicker').val());
				$scope.url='erp/api/getEditAuto?attendance='+$scope.attendance+'&startdate='+$('#startdatepicker').val()+'&enddate='+$('#enddatepicker').val();
			}
		}
		
		$http({
	        		          method  : 'GET',
	        		          url     : $scope.url
	        		         })
	        		          .success(function(data) {
	        		        	  $scope.batchinfo=1;
	        		            			console.log(data);
	        		            			$scope.reports=data;
	        		            			if(data[0].working_days == null || data[0].working_days == 'null')
	        		            				$scope.working_days=0;
	        		            			else
	        		            				$scope.working_days=data[0].working_days;
	        		            			$scope.loading=1;
	        		          });
		
	}
	
	$scope.editData=function(id,name,admno,attendence,remarks,result)
	{
	//	alert(id);
		console.log(remarks);
		$("#editModel").modal();
		$('#id').val(id);
		$('#rowname').val(name);
		$('#admno').val(admno);
		$('#attendence').val(attendence);
		if(remarks != null && remarks != "null")
		{
			
			$scope.remarksAvbl='1';
		}
		else
		{
			$scope.remarksAvbl='0';
		}
	
		//$('#remarks').val(remarks);
		$scope.stremarks=remarks;
		$scope.stresult=result;
		
	}

	$scope.batches={};
	$scope.exams={};
	$scope.fn_getBatchList=function()
	{
	$http({
	        		          method  : 'GET',
	        		          url     : 'erp/api/getBatch'
	        		         })
	        		          .success(function(data) {
	        		            			console.log(data);
	        		            			$scope.batches=data;
	        		          });
	}
	$scope.fn_getExamList=function()
	{
	$http({
	        		          method  : 'GET',
	        		          url     : 'erp/api/getExam'
	        		         })
	        		          .success(function(data) {
	        		            			console.log(data);
	        		            			$scope.exams=data;
	        		          });
	}
	
	$scope.searchData=function()
	{
	
		if($scope.examName == 'Final Exam')
		{
				$scope.screen='show';
		}
		else
			{
				$scope.screen='hide';
			}
		
		$scope.loading=0;
	
		$scope.url="";
		
		if($scope.attendance && $scope.attendance=='autoattendance')
		{
			console.log($scope.startdatepicker+"-- "+$('#startdatepicker').val());
			if(!$('#startdatepicker').val() || !$('#startdatepicker').val())
			{
				alert("Select date");
			}
			else
			{
				console.log($('#startdatepicker').val());
				console.log($('#startdatepicker').val());
				$scope.saveAutoAttendeceData();
			}
		}
		if(($scope.batchName == '0' || $scope.batchName == 'ALL')  && ($scope.examName == '0' || $scope.examName == 'ALL'))
		{
			$scope.batchinfo=0;
			$scope.fn_getReportList();
		}
		else if($scope.batchName == '0' || $scope.batchName == 'ALL')
		{
			$scope.batchinfo=0;
			$scope.fn_getReportList();
		}
		else if($scope.examName == '0' || $scope.examName == 'ALL')
			{
			$http({
			
	          method  : 'GET',
	          url     : 'erp/api/searchEdit?batch='+$scope.batchName+'&attendance='+$scope.attendance
	         })
	          .success(function(data) {
	        	  $scope.reports={};
	            			console.log(data);
	            			$scope.reports=data;
	            			$scope.loading=1;
	            			
	            			/*$http({
		        		          method  : 'GET',
		        		          url     : '/erp/api/getDays?batch='+$scope.batchName
		        		         })
		        		          .success(function(data) {
		        		        	  $scope.batchinfo=1;
		        		            			console.log(data);
		        		            			$scope.working_days=data;
		        		            			$scope.loading=1;
		        		            			
		        		          });*/
	          });
			}
		else{
			
			$scope.url='erp/api/searchEditByExamBranch?batch='+$scope.batchName+'&exam='+$scope.examName+'&attendance='+$scope.attendance;
			
			if($scope.attendance && $scope.attendance=='autoattendance')
			{
				if($('#startdatepicker').val().length<1 || $('#startdatepicker').val().length<1)
				{
					//alert("Select date");
				}
				else
				{
					console.log($('#startdatepicker').val());
					console.log($('#startdatepicker').val());
					$scope.url='erp/api/searchEditByExamBranch?batch='+$scope.batchName+'&exam='+$scope.examName+'&attendance='+$scope.attendance+'&startdate='+$('#startdatepicker').val()+'&enddate='+$('#enddatepicker').val();
					//$scope.url='/erp/api/getEditAuto?attendance='+$scope.attendance+'&startdate='+$('#startdatepicker').val()+'&enddate='+$('#enddatepicker').val();
				}
			}
			$http({
				
		          method  : 'GET',
		          url     : $scope.url
		         })
		          .success(function(data) {
		        	  $scope.reports={};
		            			console.log(data);
		            			$scope.reports=data;
		            			$scope.loading=1;
		            			$scope.working_days=0;
		            			var k=0;
		            			for(k=0; k<data.length; k++)
		            				{
		            					if(data[k].working_days != null)
		            					{
		            						$scope.working_days=data[k].working_days;
		            					}
		            				}
		            			/*if(data[0].working_days == null || data[0].working_days == 'null')
		            				$scope.working_days=0;
		            			else
		            				$scope.working_days=data[0].working_days;*/
		            			/*$http({
			        		          method  : 'GET',
			        		          url     : '/erp/api/getDays?batch='+$scope.batchName
			        		         })
			        		          .success(function(data) {
			        		        	  $scope.batchinfo=1;
			        		            			console.log(data);
			        		            			$scope.working_days=data;
			        		            			$scope.loading=1;
			        		            			
			        		          });*/
		          });
			
			
		}
		
		
		
	}
	
	
	
	
	
	$scope.searchPrintData=function()
	{
	
		if($scope.examName == 'Final Exam')
		{
				$scope.screen='show';
		}
		else
			{
				$scope.screen='hide';
			}
		
		$scope.loading=0;
	
		$scope.url="";
		
		
		if(($scope.batchName == '0' || $scope.batchName == 'ALL') )
		{
			$scope.batchinfo=0;
			$scope.fn_getReportList();
		}
		else{
			
			$scope.url='erp/api/searchPrintEditByExamBranch?batch='+$scope.batchName+'&attendance='+$scope.attendance;
			
			$http({
				
		          method  : 'GET',
		          url     : $scope.url
		         })
		          .success(function(data) {
		        	  $scope.reports={};
		            			console.log(data);
		            			$scope.reports=data;
		            			$scope.loading=1;
		            			if(data[0].working_days == null || data[0].working_days == 'null')
		            				$scope.working_days=0;
		            			else
		            				$scope.working_days=data[0].working_days;
		            			/*$http({
			        		          method  : 'GET',
			        		          url     : '/erp/api/getDays?batch='+$scope.batchName
			        		         })
			        		          .success(function(data) {
			        		        	  $scope.batchinfo=1;
			        		            			console.log(data);
			        		            			$scope.working_days=data;
			        		            			$scope.loading=1;
			        		            			
			        		          });*/
		          });
			
			
		}
		
		
		
	}
	
	
	
	$scope.saveAutoAttendeceData=function(){
		$http({
	          method  : 'GET',
	          url     : 'erp/api/saveAutoAttendence?batch='+$scope.batchName+'&exam='+$scope.examName+'&start_date='+$('#startdatepicker').val()+'&end_date='+$('#enddatepicker').val()
	          
	         })
	          .success(function(data) {
	        	  
	        	  $scope.msg=' Data saved successfully';
	            			
	          });
	}
	$scope.getWorking_Days=function()
	{

		$http({
	          method  : 'GET',
	          url     : 'erp/api/getDays?batch='+$scope.batchName+'&attendance='+$scope.attendance
	         })
	          .success(function(data) {
	        	  $scope.batchinfo=1;
	            			console.log(data);
	            			$scope.working_days=data;
	            			$scope.loading=1;
	            			
	          });
	}
	
	
	$scope.saveData=function()
	{
		
		$scope.editReportWrapper={
				'id' : $('#id').val(),
				'name' :$('#rowname').val(),
				'admno' :$('#admno').val(),
				'attendence' :$('#attendence').val(),
				'attendenceType' : $scope.attendance,
				'remarks' : $('#remarks').val(),
				'result' :	$scope.stresult,
				'examType' : $scope.examName
		};
		
		console.log($scope.editReportWrapper)
		$http({
	          method  : 'POST',
	          url     : 'erp/api/saveEdit',
	          data : $scope.editReportWrapper
	         })
	          .success(function(data) {
	            			console.log(data);
	            			$scope.fn_getReportList();
	            			$scope.msg='Success';
	          })
	          .error(function(){
	        	  $scope.msg='Error Occured';
	          });
	}
	
	$scope.fn_getBatchList();
	$scope.fn_getExamList();
	
	$scope.fn_getReportList();
	
	
	$scope.updateWorkingDays=function()
	{
		$http({
	          method  : 'GET',
	          url     : 'erp/api/updateWorkingDays?batch='+$scope.batchName+'&exam='+$scope.examName+'&workingdays='+$scope.working_days
	          
	         })
	          .success(function(data) {
	            			console.log(data);
	            			$scope.msg='Success';
	          })
	          .error(function(){
	        	  $scope.msg='Error Occured';
	          });
		
	}
	
	$scope.printAll=function()
	{
		if(!$scope.batchName)
		{
			alert("Select Valid Batch and Exam.");
			return false;
		}
		$scope.printUrl='erp/api/print?batch='+$scope.batchName+'&attendence='+$scope.attendance;
		
		/*if($scope.attendance && $scope.attendance=='autoattendance'){
			
			$scope.printUrl='/erp/api/print?batch='+$scope.batchName+'&attendence='+$scope.attendance;
		}*/
		
		$window.open($scope.printUrl, '_blank');
	}
	
	$scope.printStudent=function(admno)
	{
		if(!$scope.batchName)
		{
			alert("Select Valid Batch and Exam.");
			return false;
		}
		$scope.printStudentUrl='erp/api/printStudent?batch='+$scope.batchName+'&admno='+admno+'&attendence='+$scope.attendance;
		if($scope.attendance && $scope.attendance=='autoattendance'){
			/*if($('#startdatepicker').val().length<1 || $('#enddatepicker').val().length<1)
			{
				alert("Select Valid Date.");
				return false;
			}*/
			$scope.printStudentUrl='erp/api/printStudent?batch='+$scope.batchName+'&admno='+admno+'&attendence='+$scope.attendance;
		}
		
		$window.open($scope.printStudentUrl, '_blank');
	}
	
    $scope.initDataTable = function() {
         // $("#noCampaignData").hide();
          $("#reportTable").dataTable({});
    }

	
}]);


app.directive('repeatDone', function() {
    return function(scope, element, attrs) {
    console.log("DONE");
        if (scope.$last) { // all are rendered
            scope.$eval(attrs.repeatDone);
        }
    }
})